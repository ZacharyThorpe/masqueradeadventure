using UnityEngine;

[CreateAssetMenu(fileName = "Loot Table", menuName = "Data/Loot Table", order = 1)]
public class LootTableSO : ScriptableObject
{
    public LootTable lootTable = default;
}
