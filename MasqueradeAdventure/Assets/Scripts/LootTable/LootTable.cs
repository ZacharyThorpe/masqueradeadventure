using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class LootTable
{
    public List<LootPair> lootTable = new List<LootPair>();

    public Item GetRandomLoot()
    {
        List<Item> items = new List<Item>();

        for (int i = 0; i < lootTable.Count; i++)
        {
            for (int j = 0; j < lootTable[i].chance; j++)
            {
                switch (lootTable[i].item.itemType)
                {
                    case ItemType.TradeGood:
                        TradeGood tradeGood = new(lootTable[i].item);
                        items.Add(tradeGood);
                        break;
                    case ItemType.Weapon:
                        Weapon weapon = new(lootTable[i].item);
                        items.Add(weapon);
                        break;
                    case ItemType.Shield:
                        Shield shield = new(lootTable[i].item);
                        items.Add(shield);
                        break;
                    case ItemType.ItemAffix:
                        ItemAffixItem itemAffix = new(lootTable[i].item);
                        items.Add(itemAffix);
                        break;
                    case ItemType.Mask:
                        Mask mask = new(lootTable[i].item);
                        items.Add(mask);
                        break;
                    case ItemType.Bag:
                        Bag bag = new(lootTable[i].item);
                        items.Add(bag);
                        break;
                    case ItemType.Consumable:
                        Consumable consumable = new(lootTable[i].item);
                        items.Add(consumable);
                        break;
                    case ItemType.None:
                    default:
                        break;
                }
            }
        }

        return items[Random.Range(0, items.Count)];
    }
}

[Serializable]
public class LootPair
{
    [SerializeReference]
    public ItemDataSO item = default;
    public int chance = 1;
}
