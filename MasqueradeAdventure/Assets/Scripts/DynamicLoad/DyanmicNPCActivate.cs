using UnityEngine;

public class DyanmicNPCActivate : MonoBehaviour
{
    [SerializeField] private PlayerSpawner playerSpawner;

    private Entity playerEntity = default;

    private void Awake()
    {
        playerSpawner.NewSpawn += SnapToPosition;
    }

    private void SnapToPosition(Entity _entity)
    {
        playerEntity = _entity;
    }

    private void FixedUpdate()
    {
        if (playerEntity == null) return;

        transform.position = playerEntity.transform.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Entity>())
        {
            var entity = other.GetComponent<Entity>();
            entity.isActive = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Entity>())
        {
            var entity = other.GetComponent<Entity>();
            entity.isActive = false;
        }
    }
}
