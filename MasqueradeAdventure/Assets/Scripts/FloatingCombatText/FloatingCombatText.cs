using UnityEngine;
using TMPro;
using DG.Tweening;

public class FloatingCombatText : MonoBehaviour
{
    [SerializeField] private CanvasGroup canvas = default;
    [SerializeField] private TextMeshProUGUI combatText = default;

    private RectTransform rectTransform = default;

    private void Update()
    {
        canvas.transform.LookAt(Camera.main.transform.position);
    }

    public void SetColor(Color color)
    {
        combatText.faceColor = color * 255f;
    }

    public void AnimateCombatText(int amount)
    {
        combatText.text = amount.ToString();

        rectTransform = canvas.GetComponent<RectTransform>();
        rectTransform.DOAnchorPosY(1, 1).SetEase(Ease.OutExpo);

        Sequence seq = DOTween.Sequence();
        seq.Append(canvas.DOFade(1, 0.15f));
        seq.AppendInterval(0.5f);
        seq.Append(canvas.DOFade(0, 0.15f));
        seq.onComplete += Death;
    }
    private void Death()
    {
        DOTween.Kill(rectTransform);
        Destroy(gameObject);
    }
}
