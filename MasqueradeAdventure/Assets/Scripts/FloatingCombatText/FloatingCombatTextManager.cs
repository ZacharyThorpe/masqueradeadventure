using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class FloatingCombatTextManager : MonoBehaviour
{
    [SerializeField] private FloatingCombatText combatTextPrefab = default;
    [SerializeField] private FloatingCombatText blueCombatTextPrefab = default;
    [SerializeField] private float offsetAmount = 0.5f;

    private void Start()
    {
        combatTextPrefab.gameObject.SetActive(false);
        blueCombatTextPrefab.gameObject.SetActive(false);

        GlobalEventsManager.OnDamageTaken += FloatingCombatTextSpawn;
    }

    private void OnDestroy()
    {
        GlobalEventsManager.OnDamageTaken -= FloatingCombatTextSpawn;
    }

    private void FloatingCombatTextSpawn(Entity entity, int amount, bool isShield)
    {
        if (amount == 0) return;

        FloatingCombatText floatingTextClone = combatTextPrefab;

        if (isShield == true)
        {
            floatingTextClone = blueCombatTextPrefab;
        }

        var clone = Instantiate(floatingTextClone, floatingTextClone.transform.parent);
        clone.transform.position = entity.transform.position + RandomOffset();
        clone.AnimateCombatText(amount);
        clone.gameObject.SetActive(true);
    }

    private Vector3 RandomOffset()
    {
        return new Vector3(Random.Range(-offsetAmount, offsetAmount), Random.Range(-offsetAmount, offsetAmount), Random.Range(-offsetAmount, offsetAmount));
    }
}
