﻿using System;
using UnityEngine;

public class SeekerArrow : MonoBehaviour
{
    [SerializeField] private MeshRenderer mRenderer = default;
    [SerializeField] private SkinnedMeshRenderer smRenderer = default;

    Material instanceMaterial;

    void Start()
        {
        if(mRenderer != null)
        {
            instanceMaterial = new Material(mRenderer.material);
            mRenderer.material = instanceMaterial;
        }
        else if(smRenderer != null)
        {
            instanceMaterial = new Material(smRenderer.material);
            smRenderer.material = instanceMaterial;
        }
        }

    public void loseGraphic()
        {
        instanceMaterial.color = Color.white;
        }

    public void lockedGraphic()
        {
        instanceMaterial.color = Color.red;
        }

    public void seekGraphic()
        {
        instanceMaterial.color = Color.yellow;
        }
    }
