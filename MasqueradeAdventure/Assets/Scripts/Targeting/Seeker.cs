﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seeker : MonoBehaviour
{
    [SerializeField] private LayerMask seekerLayer = default;
    [SerializeField] private LayerMask losLayer = default;
    [SerializeField] private EntityMotor entityMotor = default;
    [SerializeField] private Entity entity = default;

    [SerializeField] private List<IInteractable> listOfInteractables = new List<IInteractable>();

    [SerializeField] private Collider[] colliders = new Collider[2];

    public IInteractable currentInteracble = default;

    void Start()
    {
        StartCoroutine(CheckTargets());
    }

    private IEnumerator CheckTargets()
    {
        while(true)
        {
            while (entity.isActive == false)
            {
                yield return new WaitForEndOfFrame();
            }

            ClearAllInteractableStatus();

            if (entity.entityMotor.isDieing || entity.entityMotor.isExtracting)
            {
                yield return new WaitForEndOfFrame();
                continue;
            }

            float shortestDistance = Mathf.Infinity;

            colliders = Physics.OverlapSphere(transform.position, entity.entityData.maxDetectionRange, seekerLayer);

            for (int i = 0; i < colliders.Length; i++)
            {
                IInteractable interactable = colliders[i].GetComponent<IInteractable>();

                if (interactable != null && interactable.Equals(null) == false)
                {
                    if (interactable.GetGameObject().GetInstanceID() == entityMotor.gameObject.GetInstanceID())
                    {
                        continue;
                    }

                    if(!entity.entityData.interactsWith.HasFlag(interactable.GetInteractionData().interactionType))
                    {
                        continue;
                    }

                    float distance = Vector3.Distance(transform.position, interactable.GetGameObject().transform.position);

                    if (distance > interactable.GetInteractionData().interactionDistance)
                    {
                        continue;
                    }

                    if(interactable.HasBeenInteractedWith())
                    {
                        continue;
                    }

                    if(!interactable.IsActive())
                    {
                        continue;
                    }

                    Vector3 direction = interactable.GetGameObject().transform.position - entityMotor.transform.position;

                    if (Vector3.Dot(direction, entityMotor.meshRoot.forward) < 0)
                    {
                        if (distance > entity.entityData.hearDetectionRange)
                        {
                            continue;
                        }
                    }

                    listOfInteractables.Add(interactable);
                }
            }

            if (listOfInteractables.Count < 1)
            {
                currentInteracble = null;
            }

            for (int i = 0; i < listOfInteractables.Count; i++)
            {
                float distanceFromInteractable = Vector3.Distance(transform.position, listOfInteractables[i].GetGameObject().transform.position);
                listOfInteractables[i].SetAsSought();
                if (distanceFromInteractable < shortestDistance)
                {
                    currentInteracble = listOfInteractables[i];
                    shortestDistance = distanceFromInteractable;
                }
            }

            yield return new WaitForEndOfFrame();
        }
    }

    public bool IsInteractableInLOS()
    {
        if (currentInteracble == null || currentInteracble.Equals(null)) return false;

        Vector3 origin = entityMotor.transform.position;
        Vector3 targetPosition = currentInteracble.GetGameObject().transform.position;

        Vector3 direction = targetPosition - origin;

        if(Physics.Raycast(origin, direction, out RaycastHit hit, entity.entityData.maxDetectionRange, losLayer, QueryTriggerInteraction.UseGlobal))
        {
            return (seekerLayer & (1 << hit.collider.gameObject.layer)) != 0;
        }
        return false;
    }

    private void ClearAllInteractableStatus()
    {
        for (int i = 0; i < listOfInteractables.Count; i++)
        {
            listOfInteractables[i].ClearSeekStatus();
        }
        listOfInteractables = new List<IInteractable>();
    }
}
