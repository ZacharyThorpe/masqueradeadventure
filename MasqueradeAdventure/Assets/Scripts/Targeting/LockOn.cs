﻿using System;
using UnityEngine;

public class LockOn : MonoBehaviour
{
    [SerializeField] private InputHandler inputHandler = default;

    [SerializeField] private Seeker seeker = default;

    public bool LockedOn;

    public IInteractable lockOnInteractable;

    public Action lostTarget;
    public Action gainTarget;

    void Start()
    {
        lockOnInteractable = null;
        inputHandler.LTriggerOn += AddTarget;
        inputHandler.LTriggerOff += ReleaseTarget;
    }

    void OnDestroy()
    {
        inputHandler.LTriggerOn -= AddTarget;
        inputHandler.LTriggerOff -= ReleaseTarget;
    }

    void AddTarget()
    {
        LockedOn = true;
        if (seeker.currentInteracble != null && seeker.currentInteracble.Equals(null) == false)
        {
            lockOnInteractable = seeker.currentInteracble;
        }
        gainTarget?.Invoke();
    }

    void ReleaseTarget()
    {
        LoseTarget();
        lockOnInteractable = null;
    }

    void OutOfRangeTarget()
    {
        LoseTarget();
        lockOnInteractable = null;
    }

    void LoseTarget()
    {
        if (CheckIfLockOnInteractionIsValid()) lockOnInteractable.ClearSeekStatus();
        LockedOn = false;
        lostTarget?.Invoke();
    }

    void Update()
    {
        if (CheckIfLockOnInteractionIsValid())
        {
            lockOnInteractable.SetAsLockedOn();
            float distance = Vector3.Distance(transform.position, GetSeekedPosition());
            if (distance > lockOnInteractable.GetInteractionData().maxdistance)
                OutOfRangeTarget();
        }
    }

    public bool CheckIfLockOnInteractionIsValid()
    {
        if(lockOnInteractable == null || lockOnInteractable.Equals(null))
        {
            return false;
        }
        else
        {
            Interactable interactable = lockOnInteractable as Interactable;
            if (interactable != null && interactable.entity != null)
            {
                return !interactable.entity.entityMotor.isDieing && !interactable.entity.entityMotor.isExtracting;
            }
            else
            {
                return interactable != null;
            }
        }
    }

    public Vector3 GetSeekedPosition()
    {
        if(lockOnInteractable == null || lockOnInteractable.Equals(null)) return Vector3.zero;
        GameObject gameObject = lockOnInteractable.GetGameObject();
        if(gameObject == null) return Vector3.zero;
        else return gameObject.transform.position;
    }
}
