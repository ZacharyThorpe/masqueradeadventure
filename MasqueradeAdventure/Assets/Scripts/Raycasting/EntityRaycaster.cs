﻿using UnityEngine;

public class EntityRaycaster : MonoBehaviour
{
    [SerializeField] Entity entity;
    [SerializeField] private Transform wallFinder;
    [SerializeField] private Transform wallHeightFinder;

    [SerializeField] private LayerMask groundMask = default;

    public bool CheckIfCanHop()
    {
        return Physics.Raycast(wallFinder.position, wallFinder.forward, out RaycastHit hit, 1f, groundMask);
    }
    public bool CheckIfWallTooHigh()
    {
        return Physics.Raycast(wallHeightFinder.position, wallHeightFinder.forward, out RaycastHit hit, 2f, groundMask);
    }
}
