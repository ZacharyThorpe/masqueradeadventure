public class PlayerQuestsRepository : Repository<PlayerQuestData>
{
    public override void Delete()
    {
        context.Delete();
    }

    public override void Modify(PlayerQuestData entry)
    {
        context.gameData = entry;
    }

    public override void Load()
    {
        context.Load<PlayerQuestData>();
    }

    public override void Save()
    {
        context.Save<PlayerQuestData>();
    }

    public override PlayerQuestData GetData()
    {
        return context.gameData as PlayerQuestData;
    }
}
