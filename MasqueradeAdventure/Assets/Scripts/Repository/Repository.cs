using System.Collections.Generic;
using UnityEngine;

public abstract class Repository<T> : MonoBehaviour where T : GameData
{
    [SerializeField] protected DataContext context;

    public abstract void Modify(T entry);

    public abstract void Delete();

    public abstract void Save();

    public abstract void Load();

    public abstract T GetData();
}
