﻿using UnityEngine;
using System;
using Random = UnityEngine.Random;

public class InterableLoot : MonoBehaviour
{
    public Item item;

    [SerializeField] private RarityLookup rarityLookup;

    [SerializeField] private Transform meshRoot;
    [SerializeField] private Rigidbody rigidBody;

    [SerializeField] private float explosionForce = 4;
    [SerializeField] private float explosionRadius = 4;

    private float timeSpentAlive = 0;

    private void Awake()
    {
        GlobalEventsManager.OnGroundLootSuccess += SuccessfulLoot;
    }

    private void OnDestroy()
    {
        GlobalEventsManager.OnGroundLootSuccess -= SuccessfulLoot;
    }

    public void Initialize(Item _item)
    {
        item = _item;

        ItemDataSO itemData = DatabaseManager.Instance.FindItem(item.itemData.dataGUID);

        if(itemData.prefab != null)
        {
            var clone = Instantiate(itemData.prefab, meshRoot);
            clone.transform.rotation = Quaternion.Euler(Random.Range(-180, 180), Random.Range(-180, 180), Random.Range(-180, 180));
            clone.SetColor(rarityLookup.rarityAssetsByRarity[itemData.rarity].rarityColor);
            clone.SetIcon(itemData.icon);
        }

        rigidBody.AddExplosionForce(explosionForce, transform.position + Vector3.down + new Vector3(Random.Range(-1, 1), 0, Random.Range(-1, 1)), explosionRadius);
    }

    private void FixedUpdate()
    {
        timeSpentAlive += Time.deltaTime;

        if (rigidBody.velocity.y < -4)
        {
            rigidBody.velocity = new Vector3(rigidBody.velocity.x, -10, rigidBody.velocity.z);
        }

        if(timeSpentAlive > 1)
        {
            rigidBody.velocity = new Vector3(0, rigidBody.velocity.y, 0);
        }
    }

    public void LootItem()
    {
        GlobalEventsManager.OnGroundLootAttempt(this);
    }

    public void SuccessfulLoot(InterableLoot iLoot)
    {
        if(iLoot == this)
        {
            Destroy(gameObject);
        }
    }
}
