using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Inventory", menuName = "Data/Entity Inventory", order = 1)]
public class EntityInventorySO : InventorySO
{
    public Weapon mainHand = default;
    public Shield shield = default;
    public Mask mask = default;
    public Bag bag = default;

    public InventoryItem consumable = default;

    public int defaultBagSlots = 6;

    public Action<Weapon> OnWeaponEquipped;
    public Action<Shield> OnShieldEquipped;
    public Action<Mask> OnMaskEquipped;
    public Action<Bag> OnBagEquipped;
    public Action<InventoryItem> OnConsumeEquipped;

    public Action<EntityInventorySO> OnSavePlayerInventoryRequest;

    public override void SaveInventory()
    {
        OnSavePlayerInventoryRequest?.Invoke(this);
    }

    public void UpdateFromEntity(EntityInventory entityInventory)
    {
        mainHand = entityInventory.mainHand;
        shield = entityInventory.shield;
        mask = entityInventory.mask;
        bag = entityInventory.bag;
        
        consumable = entityInventory.consumable;

        inventory.items = entityInventory.items;
    }

    public bool CheckIfMainHandValid()
    {
        if (mainHand == null) return false;
        return mainHand.IsValid();
    }
    public bool CheckIfShieldValid()
    {
        if (shield == null) return false;
        return shield.IsValid();
    }
    public bool CheckIfMaskIsValid()
    {
        if (mask == null) return false;
        return mask.IsValid();
    }
    public bool CheckIfBagIsValid()
    {
        if (bag == null) return false;
        return bag.IsValid();
    }

    public bool CheckIfConsumableSlot01IsValid()
    {
        if (consumable == null || consumable.item == null) return false;
        return consumable.item.IsValid();
    }

    public bool IsSpaceInBag(Item _inventoryItem)
    {
        ItemDataSO itemDataSO = DatabaseManager.Instance.FindItem(_inventoryItem.itemData.dataGUID);

        if(itemDataSO.stackable)
        {
            foreach (var item in inventory.items)
            {
                ItemDataSO itemSO = DatabaseManager.Instance.FindItem(item.item.itemData.dataGUID);

                if (itemSO.GUID == itemDataSO.GUID)
                {
                    return true;
                }
            }
        }

        return GetTotalBagSlots() > inventory.items.Count;
    }

    public bool CanDowngradeToDefaultBagSize()
    {
        return defaultBagSlots >= inventory.items.Count;
    }
    
    public bool SpotOpenForBag()
    {
        return defaultBagSlots >= inventory.items.Count + 1;
    }

    public int GetTotalBagSlots()
    {
        if (bag == null || !bag.IsValid()) return defaultBagSlots;
        return defaultBagSlots + bag.AdditionalBagSpace;
    }

    public void EquipWeapon(Weapon _weaponData)
    {
        mainHand = _weaponData;
        OnWeaponEquipped?.Invoke(mainHand);
        SaveInventory();
    }

    public void EquipShield(Shield _shieldData)
    {
        shield = _shieldData;
        OnShieldEquipped?.Invoke(shield);
        SaveInventory();
    }
    public void EquipMask(Mask _maskData)
    {
        mask = _maskData;
        OnMaskEquipped?.Invoke(mask);
        SaveInventory();
    }

    public void EquipBag(Bag _bagData)
    {
        bag = _bagData;
        OnBagEquipped?.Invoke(bag);
        SaveInventory();
    }

    public void EquipSlot01(InventoryItem _consumableData)
    {
        consumable = _consumableData;
        OnConsumeEquipped?.Invoke(consumable);
        SaveInventory();
    }

    public override void ClearInventory()
    {
        inventory.items = new List<InventoryItem>();
        shield = null;
        mainHand = null;
        mask = null;
        bag = null;
        consumable = null;
        SaveInventory();
    }
}
