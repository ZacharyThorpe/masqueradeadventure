﻿using System;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    [SerializeField] private EntityInventorySO inventorySO = default;

    [SerializeField] private PlayerSpawner playerSpawner = default;

    public Action<Item> itemLooted;

    private void Awake()
    {
        GlobalEventsManager.OnGroundLootAttempt += AddAnItem;

        if(playerSpawner != null)
        {
            playerSpawner.Death += PlayerDied;
        }
    }

    private void OnDestroy()
    {
        GlobalEventsManager.OnGroundLootAttempt -= AddAnItem;
    }

    void OnApplicationQuit()
    {
        inventorySO.ClearInventory();
    }

    private void PlayerDied(Entity _entity)
    {
        inventorySO.ClearInventory();
    }

    public void AddAnItem(InterableLoot _interableLoot)
    {
        if(inventorySO.IsSpaceInBag(_interableLoot.item))
        {
            inventorySO.AddAnItem(_interableLoot.item);
            itemLooted?.Invoke(_interableLoot.item);
            GlobalEventsManager.OnGroundLootSuccess?.Invoke(_interableLoot);
            AudioManager.Instance.LootItem();
        }
        else
        {
            GlobalEventsManager.OnPopupError?.Invoke("Bags are full");
        }
    }
}
