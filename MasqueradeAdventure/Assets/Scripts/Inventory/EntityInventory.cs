using System;
using System.Collections.Generic;

public class EntityInventory : Inventory
{
    public Weapon mainHand = default;
    public Shield shield = default;
    public Mask mask = default;
    public Bag bag = default;

    public InventoryItem consumable = default;

    public int defaultBagSlots = 6;

    public Action<Weapon> OnWeaponEquipped;
    public Action<Shield> OnShieldEquipped;
    public Action<Mask> OnMaskEquipped;
    public Action<Bag> OnBagEquipped;
    public Action<InventoryItem> OnConsumeEquipped;

    public void SetFromInventorySO(EntityInventorySO entityInventory)
    {
        mainHand = entityInventory.mainHand;
        shield = entityInventory.shield;
        mask = entityInventory.mask;
        bag = entityInventory.bag;

        consumable = entityInventory.consumable;
        defaultBagSlots = entityInventory.defaultBagSlots;

        items = entityInventory.inventory.items;
    }

    public bool CheckIfMainHandValid()
    {
        if (mainHand == null) return false;
        return mainHand.IsValid();
    }
    public bool CheckIfShieldValid()
    {
        if (shield == null) return false;
        return shield.IsValid();
    }
    public bool CheckIfMaskIsValid()
    {
        if (mask == null) return false;
        return mask.IsValid();
    }
    public bool CheckIfBagIsValid()
    {
        if (bag == null) return false;
        return bag.IsValid();
    }

    public bool CheckIfConsumableSlot01IsValid()
    {
        if (consumable == null || consumable.item == null) return false;
        return consumable.item.IsValid();
    }

    public bool IsSpaceInBag(Item _inventoryItem)
    {
        ItemDataSO itemDataSO = DatabaseManager.Instance.FindItem(_inventoryItem.itemData.dataGUID);

        if (itemDataSO.stackable)
        {
            foreach (var item in items)
            {
                ItemDataSO itemSO = DatabaseManager.Instance.FindItem(item.item.itemData.dataGUID);

                if (itemSO.GUID == itemDataSO.GUID)
                {
                    return true;
                }
            }
        }

        return GetTotalBagSlots() > items.Count;
    }

    public bool CanDowngradeToDefaultBagSize()
    {
        return defaultBagSlots >= items.Count;
    }

    public bool SpotOpenForBag()
    {
        return defaultBagSlots >= items.Count + 1;
    }

    public int GetTotalBagSlots()
    {
        if (bag == null || !bag.IsValid()) return defaultBagSlots;
        return defaultBagSlots + bag.AdditionalBagSpace;
    }

    public void EquipWeapon(Weapon _weaponData)
    {
        mainHand = _weaponData;
        OnWeaponEquipped?.Invoke(mainHand);
    }

    public void EquipShield(Shield _shieldData)
    {
        shield = _shieldData;
        OnShieldEquipped?.Invoke(shield);
    }

    public void EquipMask(Mask _maskData)
    {
        mask = _maskData;
        OnMaskEquipped?.Invoke(mask);
    }

    public void EquipBag(Bag _bagData)
    {
        bag = _bagData;
        OnBagEquipped?.Invoke(bag);
    }

    public void EquipSlot01(InventoryItem _consumableData)
    {
        consumable = _consumableData;
        OnConsumeEquipped?.Invoke(consumable);
    }

    public void ClearInventory()
    {
        items = new List<InventoryItem>();
        shield = null;
        mainHand = null;
        mask = null;
        bag = null;
        consumable = null;
    }
}
