﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Linq;

[CreateAssetMenu(fileName = "Inventory", menuName = "Data/Inventory", order = 1)]
public class InventorySO : ScriptableObject
{
    public string GUID = "INVENTORY";
    public Inventory inventory = default;

    public Action<InventorySO> OnSaveRequest;

    [Button("Clear Duplicates")]
    private void ClearExtras()
    {
        for (int i = 0; i < inventory.items.Count; i++)
        {
            InventoryItem duplicate = inventory.items.Find(item => item.item.itemData.GUID == inventory.items[i].item.itemData.GUID && item != inventory.items[i]);
            if(duplicate != null)
            {
                inventory.RemoveFromList(duplicate);
            }
        }
    }

    [Button("Generate GUID")]
    private void GenerateGUIDs()
    {
        for (int i = 0; i < inventory.items.Count; i++)
        {
            inventory.items[i].GenerateNewGUID();
        }
    }

    [Button("Save Inventory")]
    public virtual void SaveInventory()
    {
        OnSaveRequest?.Invoke(this);
    }

    public void InstaniateSaveRequest()
    {
        inventory.OnListChanged += SaveInventory;
    }

    public virtual void ClearInventory()
    {
        inventory.items = new List<InventoryItem>();
        SaveInventory();
    }

    public void AddAnItem(Item _inventoryItem)
    {
        ItemDataSO itemDataSO = DatabaseManager.Instance.FindItem(_inventoryItem.itemData.dataGUID);

        if (itemDataSO.stackable)
        {
            foreach (var item in inventory.items)
            {
                if (_inventoryItem.itemData.dataGUID == item.item.itemData.dataGUID)
                {
                    inventory.IncrementToAmount(inventory.items.IndexOf(item));
                    return;
                }
            }
        }

        InventoryItem inventoryItem = new(_inventoryItem, 1);
        inventory.AddToList(inventoryItem);
    }

    public void AddAnItem(ItemDataSO _inventoryItem, int amount)
    {
        if (_inventoryItem.stackable)
        {
            foreach (var item in inventory.items)
            {
                if (_inventoryItem.GUID == item.item.itemData.dataGUID)
                {
                    inventory.AddToAmount(inventory.items.IndexOf(item), amount);
                    return;
                }
            }
            InventoryItem stackableInventoryItem = new(_inventoryItem, amount);
            inventory.AddToList(stackableInventoryItem);
            return;
        }

        for (int i = 0; i < amount; i++)
        {
            InventoryItem inventoryItem = new(_inventoryItem, 1);
            inventory.AddToList(inventoryItem);
        }
    }

    public void AddAnItem(InventoryItem _inventoryItem)
    {
        ItemDataSO itemDataSO = DatabaseManager.Instance.FindItem(_inventoryItem.item.itemData.dataGUID);

        if (itemDataSO.stackable)
        {
            foreach (var item in inventory.items)
            {
                if (_inventoryItem.item.itemData.dataGUID == item.item.itemData.dataGUID)
                {
                    inventory.AddToAmount(inventory.items.IndexOf(item), _inventoryItem.amount);
                    return;
                }
            }
        }

        inventory.AddToList(_inventoryItem);
    }

    public void AddAnItemFirstSlot(Item _inventoryItem)
    {
        ItemDataSO itemDataSO = DatabaseManager.Instance.FindItem(_inventoryItem.itemData.dataGUID);

        if (itemDataSO.stackable)
        {
            foreach (var item in inventory.items)
            {
                if (_inventoryItem.itemData.dataGUID == item.item.itemData.dataGUID)
                {
                    inventory.IncrementToAmount(inventory.items.IndexOf(item));
                    return;
                }
            }
        }

        InventoryItem inventoryItem = new(_inventoryItem, 1);
        inventory.Prepend(inventoryItem);
    }

    public int RemoveItem(InventoryItem _inventoryItem)
    {
        foreach (var item in inventory.items)
        {
            if (item.item.itemData.GUID == _inventoryItem.item.itemData.GUID)
            {
                int index = inventory.items.IndexOf(item);
                inventory.RemoveAt(index);
                return index;
            }
        }
        return -1;
    }

    public int RemoveItem(InventoryItem _inventoryItem, int _amount)
    {
        foreach (var item in inventory.items)
        {
            if (item.item.itemData.dataGUID == _inventoryItem.item.itemData.dataGUID)
            {
                int index = inventory.items.IndexOf(item);

                inventory.AddToAmount(index, -_amount);

                if(inventory.items[index].amount <= 0)
                {
                    inventory.RemoveAt(index);
                }

                return index;
            }
        }
        return -1;
    }

    public void RemoveItem(ItemDataSO _itemData, int _amount)
    {
        foreach (var item in inventory.items)
        {
            if (item.item.itemData.dataGUID == _itemData.GUID)
            {
                int index = inventory.items.IndexOf(item);

                inventory.AddToAmount(index, -_amount);

                if (inventory.items[index].amount <= 0)
                {
                    inventory.RemoveAt(index);
                }
                return;
            }
        }
    }

    public void RemoveItemLast()
    {
        inventory.RemoveAt(inventory.items.Count - 1);
    }

    public int CheckAmount(Item _item)
    {
        ItemDataSO itemDataSO = DatabaseManager.Instance.FindItem(_item.itemData.dataGUID);

        foreach (var item in inventory.items)
        {
            ItemDataSO itemData = DatabaseManager.Instance.FindItem(item.item.itemData.dataGUID);

            if (itemData.GUID == itemDataSO.GUID)
            {
                return item.amount;
            }
        }
        return 0;
    }
}
