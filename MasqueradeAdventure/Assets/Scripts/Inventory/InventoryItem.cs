﻿using System;
using UnityEngine;

[Serializable]
public class InventoryItem
{
    public int amount = 1;
    [SerializeReference]
    public Item item;

    public InventoryItem()
    {

    }

    public InventoryItem(Item _newItem, int _amount)
    {
        amount = _amount;
        item = _newItem;
    }

    public InventoryItem(ItemDataSO itemDataSO, int _amount)
    {
        amount = _amount;

        switch (itemDataSO.itemType)
        {
            case ItemType.TradeGood:
                item = new TradeGood(itemDataSO);
                break;
            case ItemType.Weapon:
                item = new Weapon(itemDataSO);
                break;
            case ItemType.Shield:
                item = new Shield(itemDataSO);
                break;
            case ItemType.ItemAffix:
                item = new ItemAffixItem(itemDataSO);
                break;
            case ItemType.Mask:
                item = new Mask(itemDataSO);
                break;
            case ItemType.Bag:
                item = new Bag(itemDataSO);
                break;
            case ItemType.Consumable:
                item = new Consumable(itemDataSO);
                break;
            case ItemType.None:
            default:
                Debug.Log("No Item type found");
                break;
        }
    }

    public void GenerateNewGUID()
    {
        item.GenerateNewGUID();
    }

}
