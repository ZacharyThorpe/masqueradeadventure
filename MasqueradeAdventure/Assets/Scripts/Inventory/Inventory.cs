using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class Inventory
{
    public Action OnListChanged = default;

    [SerializeReference]
    public List<InventoryItem> items = new List<InventoryItem>();

    public void AddToList(InventoryItem _item)
    {
        items.Add(_item);
        OnListChanged?.Invoke();
    }

    public void RemoveFromList(InventoryItem _item)
    {
        items.Remove(_item);
        OnListChanged?.Invoke();
    }

    public void RemoveAt(int _index)
    {
        items.RemoveAt(_index);
        OnListChanged?.Invoke();
    }

    public void Prepend(InventoryItem _item)
    {
        items = items.Prepend(_item).ToList();
        OnListChanged?.Invoke();
    }

    public void IncrementToAmount(int _index)
    {
        items[_index].amount++;
        OnListChanged?.Invoke();
    }

    public void AddToAmount(int _index, int amount)
    {
        items[_index].amount += amount;
        OnListChanged?.Invoke();
    }
}
