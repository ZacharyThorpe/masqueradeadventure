using System.Collections.Generic;

public class EntityInventoryJSON
{
    public ItemData mainHand = default;
    public ItemData shield = default;
    public ItemData mask = default;
    public ItemData bag = default;
    public InventoryItemJSON consumeSlot01 = default;

    public List<InventoryItemJSON> items = new List<InventoryItemJSON>();

    public EntityInventoryJSON()
    {
        mainHand = null;
        shield = null;
        mask = null;
        bag = null;
        consumeSlot01 = null;
        items = new List<InventoryItemJSON>();
    }
}

public class InventoryItemJSON
{
    public int amount = 0;
    public ItemData item = new ItemData();
}
