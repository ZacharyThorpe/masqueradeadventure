﻿using System.Collections;
using UnityEngine;

public class GroundLootChecker : MonoBehaviour
{
    public LayerMask lootableLayer = default;

    public InterableLoot currentLootable = default;

    void Start()
    {
        StartCoroutine(CheckForLoot());   
    }

    private void OnDestroy()
    {
        InspectionGroundUI.Instance.InspectItem(null, null);
    }

    private IEnumerator CheckForLoot()
    {
        while(true)
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, 2, lootableLayer);
            if (colliders.Length > 0)
            {
                Collider closest = FindClosest(colliders);

                if (closest != null)
                {
                    InterableLoot loot = closest.GetComponent<InterableLoot>();

                    if (loot != null)
                    {
                        currentLootable = loot;
                        InspectionGroundUI.Instance.InspectItem(loot.item, loot.gameObject);
                    }
                }
            }
            else
            {
                currentLootable = null;
                InspectionGroundUI.Instance.InspectItem(null, null);
            }
            yield return new WaitForEndOfFrame();
        }
    }

    private Collider FindClosest(Collider[] colliders)
    {
        Collider closest = null;
        float closestDistance = float.MaxValue;
        foreach(Collider collider in colliders) 
        {
            float currentDistance = Vector3.Distance(collider.transform.position, transform.position);
            if (currentDistance < closestDistance)
            {
                closest = collider;
                closestDistance = currentDistance;
            }
        }
        return closest;
    }
}
