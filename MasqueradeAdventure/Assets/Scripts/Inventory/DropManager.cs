﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class DropManager : MonoBehaviour
{
    [SerializeField] private PlayerSpawner playerSpawner;

    [SerializeField] private List<EntitySpawner> spawners = new List<EntitySpawner>();

    [SerializeField] private List<Breakable> breakables = new List<Breakable>();

    [SerializeField] private InterableLoot dropableLoot;
    [SerializeField] private Transform groundLootParent;

    public Action<InterableLoot> lootDropped;

    private void Awake()
    {
        GlobalEventsManager.OnItemDropped += DropItemOnGround;

        for (int i = 0; i < spawners.Count; i++)
        {
            if (spawners[i] == null) continue;
            spawners[i].DropLoot += DropLoot;
        }

        for (int i = 0; i < breakables.Count; i++)
        {
            if (breakables[i] == null) continue;
            breakables[i].DropLoot += DropLoot;
        }
    }

    private void OnDestroy()
    {
        GlobalEventsManager.OnItemDropped -= DropItemOnGround;
    }

    private void DropItemOnGround(Item item)
    {
        var clone = Instantiate(dropableLoot, groundLootParent);
        clone.Initialize(item);
        clone.transform.position = playerSpawner.Player.transform.position;
        GlobalEventsManager.OnLootDropped?.Invoke(clone);
    }

    private void DropLoot(Entity _entity)
    {
        var clone = Instantiate(dropableLoot, groundLootParent);
        clone.Initialize(_entity.entityData.GetRandomLoot());
        clone.transform.position = _entity.transform.position;
        GlobalEventsManager.OnLootDropped?.Invoke(clone);
    }

    private void DropLoot(Breakable _breakable)
    {
        var clone = Instantiate(dropableLoot, groundLootParent);
        clone.Initialize(_breakable.GetRandomLoot());
        clone.transform.position = _breakable.transform.position + Vector3.up;
        breakables.Remove(_breakable);
        GlobalEventsManager.OnLootDropped?.Invoke(clone);
    }
}
