using DG.Tweening;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

public class PrefabEditor : OdinEditorWindow
{
    [MenuItem("Tools/Level Editor Tools")]
    private static void OpenWindow()
    {
        GetWindow<PrefabEditor>().Show();
    }

    [Button(ButtonSizes.Gigantic), GUIColor(0, 1, 0)]
    public void RotateAll()
    {
        var allGOs = Selection.gameObjects;

        for (int i = 0; i < allGOs.Length; i++)
        {
            allGOs[i].transform.eulerAngles = new Vector3(0,Random.Range(-360,360), 0);
        }
    }
}
