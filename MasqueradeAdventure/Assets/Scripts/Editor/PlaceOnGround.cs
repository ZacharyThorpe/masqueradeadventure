using UnityEngine;
using UnityEditor;

public class PlaceOnGround
{
    [MenuItem("Tools/Put All Selection On Ground")]

    private static void PlaceOnGroundAction()
    {
        var selections = Selection.objects;

        for (int i = 0; i < selections.Length; i++)
        {
            if(selections[i].GetType() == typeof(GameObject))
            {
                GameObject go = selections[i] as GameObject;
                if(Physics.Raycast(go.transform.position + Vector3.up, -Vector3.up, out RaycastHit hit))
                {
                    go.transform.position = hit.point;
                }
            }
        }
    }

    [MenuItem("Tools/Put All Selection 1 Unit from Ground")]

    private static void PlaceOneUnitFromGroundAction()
    {
        var selections = Selection.objects;

        for (int i = 0; i < selections.Length; i++)
        {
            if (selections[i].GetType() == typeof(GameObject))
            {
                GameObject go = selections[i] as GameObject;
                if (Physics.Raycast(go.transform.position + Vector3.up, -Vector3.up, out RaycastHit hit))
                {
                    go.transform.position = hit.point + Vector3.up;
                }
            }
        }
    }

    [MenuItem("Tools/Put All Selection Normals")]

    private static void PlaceNormalFromGroundAction()
    {
        var selections = Selection.objects;

        for (int i = 0; i < selections.Length; i++)
        {
            if (selections[i].GetType() == typeof(GameObject))
            {
                GameObject go = selections[i] as GameObject;
                if (Physics.Raycast(go.transform.position + Vector3.up, -Vector3.up, out RaycastHit hit))
                {
                    var terrain = hit.transform.GetComponent<Terrain>();

                    if (terrain != null)
                    {
                        var offset = 0.1f;

                        var heightLeft = terrain.SampleHeight(go.transform.position + Vector3.left * offset);
                        var heightRight = terrain.SampleHeight(go.transform.position + Vector3.right * offset);
                        var heightForward = terrain.SampleHeight(go.transform.position + Vector3.forward * offset);
                        var heightBack = terrain.SampleHeight(go.transform.position + Vector3.back * offset);

                        var tangentX = new Vector3(2.0f * offset, heightRight - heightLeft, 0).normalized;
                        var tangentZ = new Vector3(0, heightForward - heightBack, 2.0f * offset).normalized;

                        var normal = Vector3.Cross(tangentZ, tangentX).normalized;

                        go.transform.rotation = Quaternion.LookRotation(normal);
                        go.transform.Rotate(Vector3.right * 90f);

                        Debug.DrawRay(go.transform.position, normal, Color.red, 5);

                    }
                    else
                    {
                        go.transform.position = hit.point;
                        go.transform.eulerAngles = hit.normal;
                    }
                }
            }
        }
    }
}