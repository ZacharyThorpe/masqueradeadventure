using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public static class UILayoutCreator
{
    [MenuItem("GameObject/UI Creation/Create Hori Layout", false, 0)]
    private static void CreateHoriLayout(MenuCommand menuCommand)
    {
        if(Selection.activeTransform != null)
        {
            GameObject newObject = ObjectFactory.CreateGameObject("HoriLayout");
            newObject.transform.parent = Selection.activeTransform;
            newObject.transform.localPosition = Vector3.zero;
            newObject.transform.localRotation = Quaternion.identity;

            var rectTransform = newObject.AddComponent<RectTransform>();

            rectTransform.anchorMin = new Vector2(0, 0);
            rectTransform.anchorMax = new Vector2(1, 1);
            rectTransform.pivot = new Vector2(0.5f, 0.5f);

            rectTransform.offsetMin = new Vector2(0, 0);
            rectTransform.offsetMax = new Vector2(0, 0);

            var component = newObject.AddComponent<HorizontalLayoutGroup>();
            component.childControlHeight = true;
            component.childControlWidth = true;
            component.childForceExpandHeight = false;
            component.childForceExpandWidth = false;

            for (int i = 0; i < 2; i++)
            {
                MakeElement(newObject.transform);
            }            
        }
    }

    private static void MakeElement(Transform parent)
    {
        GameObject element = ObjectFactory.CreateGameObject("Element");
        element.transform.parent = parent.transform;
        element.transform.localPosition = Vector3.zero;
        element.transform.localRotation = Quaternion.identity;

        var elementComponent = element.AddComponent<LayoutElement>();
        elementComponent.flexibleWidth = 1;
        elementComponent.flexibleHeight = 1;
    }
}
