using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class TerrainEditor : OdinEditorWindow
{
    public TerrainData terrainData;

    public TreeInstance[] trees;

    public float allowedDistance;

    [MenuItem("Terrain Tools/Edit Terrain")]
    private static void OpenWindow()
    {
        GetWindow<TerrainEditor>().Show();
    }
    [Button(ButtonSizes.Gigantic), GUIColor(0, 1, 0)]
    public void GetAll()
    {
        trees = terrainData.treeInstances;
    }

    [Button(ButtonSizes.Gigantic), GUIColor(0, 1, 0)]
    public void RandomizeTrees()
    {
        for (int i = 0; i < trees.Length; i++)
        {
            if(trees[i].prototypeIndex <= 3)
            {
                int newIndex = Random.Range(0, 3);
                trees[i].prototypeIndex = newIndex;
            }
            else if(trees[i].prototypeIndex <= 6)
            {
                int newIndex = Random.Range(4, 6);
                trees[i].prototypeIndex = newIndex;
            }
        }

        terrainData.SetTreeInstances(trees, true);
    }

    [Button(ButtonSizes.Gigantic), GUIColor(0, 1, 0)]
    public void RemoveTreeThatAreTooClose()
    {
        List<TreeInstance> currentTrees = trees.ToList();
        List<TreeInstance> acceptableTrees = new List<TreeInstance>();

        for (int i = 0; i < currentTrees.Count - 1; i++)
        {
            if (!currentTrees.Find(tree => tree.position != currentTrees[i].position && Vector3.Distance(currentTrees[i].position, tree.position) < allowedDistance).Equals(default(TreeInstance)))
            {
                Debug.Log("Tree is too close!");
            }
            else
            {
                acceptableTrees.Add(currentTrees[i]);
            }
        }

        terrainData.SetTreeInstances(acceptableTrees.ToArray(), true);
    }
    [Button(ButtonSizes.Gigantic), GUIColor(0, 1, 0)]
    public void MatchHeightToWidth()
    {
        for (int i = 0; i < trees.Length; i++)
        {
            if(trees[i].heightScale <= 0.75f)
            {
                trees[i].heightScale = 0.75f;
                trees[i].widthScale = 0.75f;
            }
        }
        terrainData.SetTreeInstances(trees, true);
    }
}
