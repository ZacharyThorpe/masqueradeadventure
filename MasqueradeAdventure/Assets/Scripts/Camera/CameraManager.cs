﻿using UnityEngine;

public class CameraManager : MonoBehaviour
{
    [SerializeField] private PlayerSpawner playerSpawner = default;

    [SerializeField] private CameraTrack cameraTrack = default;
    [SerializeField] private CameraLookAt cameraLookAt = default;

    private void OnEnable()
    {
        playerSpawner.NewSpawn += NewPlayer;
    }

    private void NewPlayer(Entity entity)
    {
        EntityCamera entityCamera = entity.GetComponent<EntityCamera>();
        if(entityCamera != null)
        {
            cameraLookAt.target = entityCamera.playerHead;
            cameraTrack.playerHead = entityCamera.playerHead;
            cameraTrack.cameraGuide = entityCamera.cameraOffset;
        }
    }
}
