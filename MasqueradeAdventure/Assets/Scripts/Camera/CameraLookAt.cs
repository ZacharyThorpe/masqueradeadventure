﻿using UnityEngine;
public class CameraLookAt : MonoBehaviour
{
    public Transform target;

    void LateUpdate()
    {
        if (target != null)
        {
            transform.rotation = Quaternion.LookRotation(target.transform.position - transform.position);
        }
    }
}
