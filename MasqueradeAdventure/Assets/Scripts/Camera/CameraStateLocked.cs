﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class CameraStateLocked : CameraState
{
    [SerializeField] private float nonSinged = 0;
    [SerializeField] private float speed = 1;
    [SerializeField] private Quaternion desiredRotation;
    [SerializeField] LTDescr resettingRotation;
    [SerializeField] LTDescr resettingZoom;

    public CameraStateLocked(EntityCamera entityCamera) : base(entityCamera)
    {
        isLockedState = true;
    }
    public override IEnumerator BeginState()
    {
        UpdateRotation(_entityCamera.entityMeshTransform.rotation, _entityCamera.cameraSettings.resetRotationTimer);
        SetZoomTwean(_entityCamera.cameraSettings.defaultZoom, _entityCamera.cameraSettings.resetZoomTimer);
        yield break;
    }
    public override void Rotation()
    {
        if(movedCamera)
        {
            if (Mathf.Abs(_entityCamera.cameraInput.x) > _entityCamera.cameraSettings.threshold)
                UpdateRotation(_entityCamera.cameraInput.x);
        }
        else if (_entityCamera.entity.lockOn.CheckIfLockOnInteractionIsValid())
        {
            float singedAngle = Vector3.SignedAngle(_entityCamera.cameraPivot.forward.normalized, _entityCamera.entityMeshTransform.forward.normalized, Vector3.up);
            nonSinged = Mathf.Abs(singedAngle);

            if (nonSinged < _entityCamera.cameraSettings.rotationOffsetRange.x || nonSinged > _entityCamera.cameraSettings.rotationOffsetRange.y)
            {
                float targetAmount = nonSinged > _entityCamera.cameraSettings.rotationOffsetRange.y ? _entityCamera.cameraSettings.rotationOffsetRange.y : _entityCamera.cameraSettings.rotationOffsetRange.x;
                targetAmount = singedAngle < 0 ? targetAmount : targetAmount * -1;
                desiredRotation = Quaternion.Euler(_entityCamera.entityMeshTransform.eulerAngles + Vector3.up * targetAmount);

                float power = GetAmountOffFromTarget(nonSinged) * GetDistanceFromTarget();
                speed = Mathf.Clamp(Time.deltaTime * Mathf.Pow(_entityCamera.cameraSettings.followRotationPower, power), _entityCamera.cameraSettings.rotationSpeedLimits.x, _entityCamera.cameraSettings.rotationSpeedLimits.y);
            }
            _entityCamera.cameraPivot.rotation = Quaternion.RotateTowards(_entityCamera.cameraPivot.rotation, desiredRotation, speed);
        }
    }
    public override void Zoom()
    {
        if (Mathf.Abs(_entityCamera.cameraInput.y) > _entityCamera.cameraSettings.threshold)
            UpdateZoom(_entityCamera.cameraInput.y, _entityCamera.cameraSettings.minMaxAxis);

        if (_entityCamera.cameraInput.magnitude > _entityCamera.cameraSettings.threshold)
            movedCamera = true;

        if (!movedCamera)
        {
            RunAutoZoom();
        }
    }
    public override void PlaceCameraOrigin()
    {
        _entityCamera.pickedLocalPosition = Vector3.Lerp(_entityCamera.cameraOrigin.localPosition, _entityCamera.cameraSettings.orginalPOS, _entityCamera.cameraSettings.crawlingSpeed * Time.deltaTime);
    }
    public override void PlacePlayerHead()
    {
        _entityCamera.pickedLocalHeadPosition = Vector3.Lerp(_entityCamera.playerHead.localPosition, _entityCamera.cameraSettings.orginalHeadPOS, _entityCamera.cameraSettings.crawlingSpeed * Time.deltaTime);
    }
    public override void ForceUpdate()
    {
        UpdateRotation(0);
        UpdateZoom(0, _entityCamera.cameraSettings.minMaxAxis);
    }
    void RunAutoZoom()
    {
        Vector2 moveInput = _entityCamera.entityInput;
        moveInput.y = -moveInput.y;
        if (Mathf.Abs(moveInput.y) > _entityCamera.cameraSettings.threshold)
            UpdateZoom(moveInput.y * _entityCamera.cameraSettings.zoomModifer, _entityCamera.cameraSettings.resetMinMax);
    }
    void UpdateRotation(float inputX)
    {
        if (resettingRotation != null)
            LeanTween.cancel(resettingRotation.id);

        float adjustedSentivity = _entityCamera.cameraSettings.ySentivity;
        _entityCamera.cameraPivot.Rotate(adjustedSentivity * inputX * Vector3.up);
    }
    void UpdateZoom(float _value)
    {
        iAxis = Mathf.Clamp(_value, _entityCamera.cameraSettings.minMaxAxis.x, _entityCamera.cameraSettings.minMaxAxis.y);
        _entityCamera.cameraOffset.localPosition = _entityCamera.cameraSettings.curve.FindBezierPos(iAxis);
    }
    void UpdateZoom(float inputY, Vector2 _range)
    {
        iAxis = Mathf.Clamp(iAxis + inputY * _entityCamera.cameraSettings.xSentivity, _range.x, _range.y);
        _entityCamera.cameraOffset.localPosition = _entityCamera.cameraSettings.curve.FindBezierPos(iAxis);
    }
    float GetAmountOffFromTarget(float _amount)
    {
        if (_amount < _entityCamera.cameraSettings.rotationOffsetRange.x)
            return _entityCamera.cameraSettings.rotationOffsetRange.x - _amount;
        else if (_amount > _entityCamera.cameraSettings.rotationOffsetRange.y)
            return _amount - _entityCamera.cameraSettings.rotationOffsetRange.y;
        return 1;
    }
    float GetDistanceFromTarget()
    {
        float distance = Vector3.Distance(_entityCamera.transform.position, _entityCamera.entity.lockOn.GetSeekedPosition());
        return Remap.RemapFloat(distance, _entityCamera.cameraSettings.allowRotationDistance.x, 0, _entityCamera.cameraSettings.allowRotationDistance.y, 1);
    }

    public void UpdateRotation(Quaternion _rotation, float _time)
    {
        if (IsTweening())
            return;
        resettingRotation = _entityCamera.cameraPivot.LeanRotate(_rotation.eulerAngles, _time);
    }

    bool IsTweening()
    {
        if (resettingRotation != null && LeanTween.isTweening(resettingRotation.id))
            return true;
        else
            return false;
    }
    public void SetZoomTwean(Vector2 _range, float _time)
    {
        if (resettingZoom != null && LeanTween.isTweening(resettingZoom.id))
            return;

        float range = iAxis;
        range = range > _range.y ? _range.y : range;
        range = range < _range.x ? _range.x : range;
        resettingZoom = LeanTween.value(_entityCamera.cameraOffset.gameObject, UpdateZoom, iAxis, range, _time).setEaseOutCirc();
    }
}
