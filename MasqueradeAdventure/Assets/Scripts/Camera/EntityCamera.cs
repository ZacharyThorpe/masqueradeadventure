﻿using UnityEngine;

public class EntityCamera : MonoBehaviour
{
    [SerializeField] private InputHandler inputHandler = default;

    public Entity entity;
    public CameraSettings cameraSettings;

    public Transform playerHead;

    public Vector3 pickedLocalPosition;
    public Vector3 pickedLocalHeadPosition;

    public Transform cameraOrigin;
    public Transform cameraPivot;
    public Transform cameraOffset;
    public Transform entityMeshTransform;

    public Vector2 cameraInput;
    public Vector2 entityInput;

    private CameraState _currentState = default;

    void Start()
    {
        pickedLocalPosition = cameraOrigin.localPosition;
        pickedLocalHeadPosition = playerHead.localPosition;

        inputHandler.LTriggerOn += AttemptToLockOn;
        inputHandler.LTriggerOff += ReleaseTarget;

        entity.lockOn.lostTarget += ReleaseTarget;

        SetState(new CameraStateIdle(this));

        _currentState.ForceUpdate();
    }

    private void FixedUpdate()
    {
        if (entity.isActive == false || entity.entityMotor.isExtracting == true) return;

        cameraInput = inputHandler.CameraInput;
        entityInput = inputHandler.MoveInput;

        if(_currentState != null)
        {
            _currentState.Rotation();
            _currentState.Zoom();
            _currentState.PlaceCameraOrigin();
            _currentState.PlacePlayerHead();
        }
        cameraOrigin.localPosition = pickedLocalPosition;
        playerHead.localPosition = pickedLocalHeadPosition;
    }

    public void SetState(CameraState cameraState)
    {
        StopAllCoroutines();

        if(_currentState != null)
        {
            cameraState.movedCamera = _currentState.movedCamera;
            cameraState.iAxis = _currentState.iAxis;
        }

        _currentState = cameraState;

        StartCoroutine(_currentState.BeginState());
    }

    private void ReleaseTarget()
    {
        _currentState.movedCamera = true;
        SetState(new CameraStateIdle(this));
    }

    private void AttemptToLockOn()
    {
        _currentState.movedCamera = false;
        SetState(new CameraStateLocked(this));
    }
}
