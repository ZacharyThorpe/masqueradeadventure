﻿using UnityEngine;
using System;

[Serializable]
public class CameraStateIdle : CameraState
{
    public CameraStateIdle(EntityCamera entityCamera) : base(entityCamera)
    {
        isLockedState = false;
    }
    public override void Rotation()
    {
        if (Mathf.Abs(_entityCamera.cameraInput.x) > _entityCamera.cameraSettings.threshold)
            UpdateRotation(_entityCamera.cameraInput.x);
    }
    public override void Zoom()
    {
        if (Mathf.Abs(_entityCamera.cameraInput.y) > _entityCamera.cameraSettings.threshold)
            UpdateZoom(_entityCamera.cameraInput.y, _entityCamera.cameraSettings.minMaxAxis);

        if (_entityCamera.cameraInput.magnitude > _entityCamera.cameraSettings.threshold)
            movedCamera = true;

        if(!movedCamera)
        {
            RunAutoZoom();
        }
    }
    public override void ForceUpdate()
    {
        UpdateRotation(0);
        UpdateZoom(0, _entityCamera.cameraSettings.minMaxAxis);
    }
    public override void PlaceCameraOrigin()
    {
        _entityCamera.pickedLocalPosition = Vector3.Lerp(_entityCamera.cameraOrigin.localPosition, _entityCamera.cameraSettings.orginalPOS, _entityCamera.cameraSettings.crawlingSpeed * Time.deltaTime);
    }
    public override void PlacePlayerHead()
    {
        _entityCamera.pickedLocalHeadPosition = Vector3.Lerp(_entityCamera.playerHead.localPosition, _entityCamera.cameraSettings.orginalHeadPOS, _entityCamera.cameraSettings.crawlingSpeed * Time.deltaTime);
    }
    void RunAutoZoom()
    {
        Vector2 moveInput = _entityCamera.entityInput;
        moveInput.y = -moveInput.y;
        if (Mathf.Abs(moveInput.y) > _entityCamera.cameraSettings.threshold)
            UpdateZoom(moveInput.y * _entityCamera.cameraSettings.zoomModifer, _entityCamera.cameraSettings.resetMinMax);
    }
    void UpdateRotation(float inputX)
    {
        float adjustedSentivity = _entityCamera.cameraSettings.ySentivity;
        _entityCamera.cameraPivot.Rotate(adjustedSentivity * inputX * Vector3.up);
    }
    void UpdateZoom(float inputY, Vector2 _range)
    {
        iAxis = Mathf.Clamp(iAxis + inputY * _entityCamera.cameraSettings.xSentivity, _range.x, _range.y);
        _entityCamera.cameraOffset.localPosition = _entityCamera.cameraSettings.curve.FindBezierPos(iAxis);
    }
}
