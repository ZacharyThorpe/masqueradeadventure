﻿using UnityEngine;

public class CameraTrack : MonoBehaviour
    {
    public Transform playerHead = default;
    public Transform cameraGuide = default;
    [SerializeField] private CameraSettings cameraSettings;
    [SerializeField] private RaycastHit hit = default;
    [SerializeField] private bool onWall = default;

    void Start()
        {
        if (playerHead == null || cameraGuide == null)
            {
            enabled = false;
            Debug.Log("Check that variables are assigned");
            }
        }

    void LateUpdate()
        {
        if(playerHead != null && cameraGuide != null)
        {
            transform.position = GetCameraPositionBasedOnCollision() + AddedBufferIfTooCloseToPlayer();
        }
    }

    Vector3 GetCameraPositionBasedOnCollision()
        {
        if (Physics.SphereCast(playerHead.position, cameraSettings.amountOffset, cameraGuide.position - playerHead.position, out hit, Vector3.Distance(playerHead.position, cameraGuide.position), cameraSettings.cameraInteractLayer))
            {
            onWall = true;
            return hit.point + hit.normal * cameraSettings.amountOffset;
            }
        else
            {
            onWall = false;
            return cameraGuide.position;
            }
        }

    Vector3 AddedBufferIfTooCloseToPlayer()
        {
        if (onWall)
            return Vector3.Lerp(Vector3.up * cameraSettings.safeDistance, Vector3.zero, Vector3.Distance(hit.point, playerHead.position) / cameraSettings.safeDistance);
        else
            return Vector3.zero;
        }
    }
