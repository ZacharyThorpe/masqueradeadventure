﻿using UnityEngine;

[CreateAssetMenu(menuName = "Player/PlayerCamera")]
public class CameraSettings : ScriptableObject
    {
    public LayerMask cameraInteractLayer = 8 >> 1;
    public float amountOffset = 0.35f;
    public float safeDistance = 2.2f;
    public Vector2 defaultZoom = new Vector2(0.25f, 0.4f);
    public float resetRotationTimer = 0.25f;
    public float resetZoomTimer = 1f;

    public float ySentivity = 0.6f;
    public float xSentivity = -0.006f;
    public float threshold = 0.01f;

    public Vector2 minMaxAxis = new Vector2(0.07f, 0.85f);
    public Vector2 resetMinMax = new Vector2(0.2f, 0.5f);

    public BeizerCurve curve;

    public Vector2 lockedOnDistance = new Vector2(0.15f, 0.5f);
    public float lockedOnSmooth = 0.3f;

    public Vector2 rotationOffsetRange = new Vector2(15,45);
    public float followRotationSpeed = 0.25f;
    public float followRotationPower = 1.1f;
    public Vector2 allowRotationDistance = new Vector2(3, 4);
    public Vector2 rotationSpeedLimits = new Vector2(0, 4);

    public float zoomModifer = 0.5f;
    public float zoomModiferCrawling = 0.25f;

    public Vector3 orginalPOS = Vector3.zero;
    public Vector3 crawlingOffset = -Vector3.one;
    public float crawlingSpeed = 5f;


    public Vector3 orginalHeadPOS = Vector3.zero;
    public Vector3 crawlingHeadOffset = -Vector3.one;
}