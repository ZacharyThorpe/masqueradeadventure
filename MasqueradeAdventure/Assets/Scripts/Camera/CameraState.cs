﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class CameraState
{
    protected readonly EntityCamera _entityCamera;

    public bool isLockedState;

    public bool movedCamera = false;
    public float iAxis = 0.4f;

    public CameraState(EntityCamera entityCamera)
    {
        _entityCamera = entityCamera;
    }
    public virtual IEnumerator BeginState() { yield break; }
    public virtual void Rotation() { }
    public virtual void Zoom() { }
    public virtual void ForceUpdate() { }
    public virtual void PlaceCameraOrigin() { }
    public virtual void PlacePlayerHead() { }
}
