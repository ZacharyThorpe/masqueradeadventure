﻿using UnityEngine;

public static class Remap
{
    public static float RemapFloat(float value, float low1, float low2, float high1, float high2)
        {
        float normal = Mathf.InverseLerp(low1, high1, value);
        return Mathf.Lerp(low2, high2, normal);
        }
}
