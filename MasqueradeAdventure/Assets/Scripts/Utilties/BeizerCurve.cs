﻿using System;
using UnityEngine;

[Serializable]
public class BeizerCurve
{    
    public Vector3 p0 = new Vector3(0, 0.7f, -0.75f);
    public Vector3 p1 = new Vector3(0, 2f, -5.75f);
    public Vector3 p2 = new Vector3(0, 5f, -8f);
    public Vector3 p3 = new Vector3(0, 8f, -9f);

    public Vector3 FindBezierPos(float t)
        {
        //Basic parts of formula for ease of reading
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;

        //Point decided
        Vector3 p = uuu * p0;
        p += 3 * uu * t * p1;
        p += 3 * u * tt * p2;
        p += ttt * p3;

        return p;
        }

    public Vector3 FindBezierPos(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
        //Basic parts of formula for ease of reading
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;

        //Point decided
        Vector3 p = uuu * p0;
        p += 3 * uu * t * p1;
        p += 3 * u * tt * p2;
        p += ttt * p3;

        return p;
        }

    public BezierPoints GetPoints()
        {
        BezierPoints points = new BezierPoints();
        points.p0 = p0;
        points.p1 = p1;
        points.p2 = p2;
        points.p3 = p3;
        return points;
        }
    }
