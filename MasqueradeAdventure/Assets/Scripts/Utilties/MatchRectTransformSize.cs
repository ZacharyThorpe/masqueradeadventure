using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class MatchRectTransformSize : MonoBehaviour
{
    [SerializeField] private RectTransform source;
    [SerializeField] private RectTransform target;

    private void Update()
    {
        if (source == null || target == null) return;
        target.sizeDelta = source.sizeDelta;
    }
}
