using Sirenix.OdinInspector;

public class QuestObjectiveKills : QuestObjective
{
    public int amountNeed = default;
    public string entityGUID = string.Empty;
    public EntityData entityData = default;

    public string GetQuestObjective()
    {
        return string.Format("Kill {0}", entityData.Name);
    }

    [Button("Update GUID")]
    public void UpdateGUIData()
    {
        if (entityData != null)
        {
            entityGUID = entityData.GUID;
        }
    }
}
