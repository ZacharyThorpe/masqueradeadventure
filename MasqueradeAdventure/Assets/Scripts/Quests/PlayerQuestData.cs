using System.Collections.Generic;

public class PlayerQuestData : GameData
{
    public HashSet<string> currentQuests = new HashSet<string>();
    public HashSet<string> completedQuests = new HashSet<string>();

    public List<QuestProgress> questProgress = new List<QuestProgress>();

    public PlayerQuestData()
    {
        currentQuests = new HashSet<string>();
        completedQuests = new HashSet<string>();
        questProgress = new List<QuestProgress>();
    }
}