using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UnityEngine;
public class PlayerQuestManager : MonoBehaviour
{
    public static PlayerQuestManager Instance;
    public Action QuestUpdated = default;

    [SerializeField] private InventorySO playerStash;
    [SerializeField] private PlayerQuestsRepository playerQuestRerpository;

    private PlayerQuestData playerQuestData = new PlayerQuestData();

    private void Awake()
    {
        if (Instance != null) Destroy(gameObject);
        Instance = this;
    }

    private void Start()
    {
        playerQuestRerpository.Load();
        playerQuestData = playerQuestRerpository.GetData();
        if(playerQuestData == null)
        {
            playerQuestData = new PlayerQuestData();
        }
    }

    public void ClearQuestLog()
    {
        playerQuestData.currentQuests = new HashSet<string>();
        playerQuestRerpository.Modify(playerQuestData);
        QuestUpdated?.Invoke();
    }

    public List<string> GetCurrentQuests()
    {
        return new List<string>(playerQuestData.currentQuests);
    }

    public void GetNewNPCQuests(string npcID)
    {
        ClearAllCurrentQuests(npcID);
        AddAllValidQuestsFromNPC(npcID);
    }

    public void AddAllValidQuestsFromNPC(string npcID)
    {
        var newQuest = QuestDatabase.Instance.GetRandomNPCQuest(npcID);
        if (newQuest == null) return;

        AddToList(newQuest.GUID);
        ModifyAndSave();
    }

    public void AddToList(string questID)
    {
        playerQuestData.currentQuests.Add(questID);
        ModifyAndSave();
        QuestUpdated?.Invoke();
    }

    public void CompleteQuest(QuestSO quest)
    {
        var isComplete = IsQuestComplete(quest);

        if(isComplete)
        {
            if(quest.questObjective.questType == QuestType.Loot)
            {
                var questObjective = quest.questObjective as QuestObjectiveLoot;
                playerStash.RemoveItem(questObjective.itemData, questObjective.amountNeed);
                playerStash.AddAnItem(quest.questReward.item, quest.questReward.amount);
            }
        }

        playerQuestData.currentQuests.Remove(quest.GUID);

        if(quest.isUnique)
        {
            playerQuestData.completedQuests.Add(quest.GUID);
        }

        var questProgress = playerQuestData.questProgress.Find(questProgress => questProgress.QuestID == quest.GUID);
        if(questProgress != null)
        {
            playerQuestData.questProgress.Remove(questProgress);
        }

        AddToList(QuestDatabase.Instance.GetRandomNPCQuest(quest.npcThatOffers.GUID).GUID);
        ModifyAndSave();
    }

    public void UpdateQuestProgress(QuestSO questData, int amount)
    {
        var questProgress = playerQuestData.questProgress.Find(questProgress => questProgress.QuestID == questData.GUID);
        if(questProgress != null)
        {
            questProgress.progressAmount += amount;
            ModifyAndSave();
        }
        else
        {
            var newQuestProgress = new QuestProgress()
            {
                QuestID = questData.GUID,
                progressAmount = amount,
            };
            playerQuestData.questProgress.Add(newQuestProgress);
            ModifyAndSave();
        }
    }

    public bool IsQuestComplete(QuestSO questData)
    {
        if (questData.questObjective.questType == QuestType.Kill)
        {
            var questProgress = playerQuestData.questProgress.Find(questProgress => questProgress.QuestID == questData.GUID);
            if(questProgress != null)
            {
                var questObjective = questData.questObjective as QuestObjectiveKills;
                return questObjective.amountNeed <= questProgress.progressAmount;
            }
        }
        else if (questData.questObjective.questType == QuestType.Loot)
        {
            var questObjective = questData.questObjective as QuestObjectiveLoot;
            var itemsInInventory = playerStash.inventory.items.FindAll(item => item.item.itemData.dataGUID == questObjective.itemGUID);
            int amount = 0;
            foreach (var item in itemsInInventory)
            {
                amount += item.amount;
            }
            return questObjective.amountNeed <= amount;
        }
        return false;
    }

    public int GetCurrentProgress(QuestSO questData)
    {
        if (questData.questObjective.questType == QuestType.Kill)
        {
            var questProgress = playerQuestData.questProgress.Find(questProgress => questProgress.QuestID == questData.GUID);
            if (questProgress != null)
            {
                var questObjective = questData.questObjective as QuestObjectiveKills;
                return questProgress.progressAmount;
            }
        }
        else if (questData.questObjective.questType == QuestType.Loot)
        {
            var questObjective = questData.questObjective as QuestObjectiveLoot;
            var itemsInInventory = playerStash.inventory.items.FindAll(item => item.item.itemData.dataGUID == questObjective.itemGUID);
            int amount = 0;
            foreach (var item in itemsInInventory)
            {
                amount += item.amount;
            }
            return amount;
        }
        return 0;
    }

    [Button("Save")]
    private void ModifyAndSave()
    {
        playerQuestRerpository.Modify(playerQuestData);
        playerQuestRerpository.Save();
    }

    private void ClearAllCurrentQuests(string npcID)
    {
        var runningList = new List<string>(playerQuestData.currentQuests);

        for (int i = 0; i < runningList.Count; i++)
        {
            var questData = QuestDatabase.Instance.GetQuestByID(runningList[i]);
            if (questData != null && questData.npcThatOffers.GUID == npcID)
            {
                playerQuestData.currentQuests.Remove(runningList[i]);
            }
        }
    }
}
