using Sirenix.OdinInspector;

public class QuestObjectiveLoot : QuestObjective
{
    public int amountNeed = default;
    public string itemGUID = string.Empty;
    public ItemDataSO itemData = default;

    public string GetQuestObjective()
    {
        return string.Format("Gather {0}", itemData.displayName);
    }

    [Button("Update GUID")]
    public void UpdateGUIData()
    {
        if (itemData != null)
        {
            itemGUID = itemData.GUID;
        }
    }
}
