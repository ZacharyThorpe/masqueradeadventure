using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Quest Collection Data", menuName = "Data/Quest Collection", order = 1)]
public class QuestCatalog : ScriptableObject
{
    public List<QuestSO> questCollection = new List<QuestSO>();
}
