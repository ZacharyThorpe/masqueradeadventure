using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Random = UnityEngine.Random;

public class QuestDatabase : MonoBehaviour
{
    public static QuestDatabase Instance;

    private List<QuestSO> listOfAllQuest = new List<QuestSO>();

    private static Dictionary<string, QuestSO> questByID = new Dictionary<string, QuestSO>();
    private const string ADDRESSABLETAG = "Quests";

    private void Awake()
    {
        if (Instance != null) Destroy(this);
        Instance = this;
    }

    private void Start()
    {
        var loadAssetTask = Addressables.LoadAssetsAsync<QuestCatalog>(ADDRESSABLETAG, AssetLoaded);
    }

    private void AssetLoaded(QuestCatalog questCatalog)
    {
        for (int i = 0; i < questCatalog.questCollection.Count; i++)
        {
            listOfAllQuest.Add(questCatalog.questCollection[i]);
            questByID.Add(questCatalog.questCollection[i].GUID, questCatalog.questCollection[i]);
        }
    }

    public QuestSO GetQuestByID(string questID)
    {
        if (questByID.ContainsKey(questID)) return questByID[questID];
        return null;
    }

    public QuestSO GetRandomNPCQuest(string NPCID)
    {
        var npcQuests = listOfAllQuest.FindAll(quest => quest.npcThatOffers.GUID == NPCID);

        if (npcQuests.Count <= 0) return null;

        return npcQuests[Random.Range(0, npcQuests.Count)];
    }
}
