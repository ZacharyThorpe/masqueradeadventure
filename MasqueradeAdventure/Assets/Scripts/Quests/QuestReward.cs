using System;
using UnityEngine;

[Serializable]
public class QuestReward 
{
    public int amount = 1;
    [SerializeReference]
    public ItemDataSO item = default;
}
