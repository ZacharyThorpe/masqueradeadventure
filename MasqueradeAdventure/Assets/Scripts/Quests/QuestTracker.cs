using System.Collections.Generic;
using UnityEngine;

public class QuestTracker : MonoBehaviour
{
    [SerializeField] private List<EntitySpawner> spawners = new List<EntitySpawner>();
    [SerializeField] private List<QuestSO> activeQuests = new List<QuestSO>();

    private void Awake()
    {
        for (int i = 0; i < spawners.Count; i++)
        {
            if (spawners[i] == null) continue;
            spawners[i].Death += UpdateQuestProgress;
        }
    }

    private void Start()
    {
        activeQuests = new List<QuestSO>();
        var listOfQuestsIDS = PlayerQuestManager.Instance.GetCurrentQuests();
        for (int i = 0; i < listOfQuestsIDS.Count; i++)
        {
            var questData = QuestDatabase.Instance.GetQuestByID(listOfQuestsIDS[i]);
            if(questData != null)
            {
                if (questData.questObjective.questType == QuestType.Kill)
                {
                    activeQuests.Add(questData);
                }
            }
        }
    }

    private void UpdateQuestProgress(Entity _entity)
    {
        foreach (var activeQuest in activeQuests)
        {
            switch (activeQuest.questObjective.questType)
            {
                case QuestType.Kill:
                    QuestObjectiveKills qok = activeQuest.questObjective as QuestObjectiveKills;
                    if (qok.entityData.GUID == _entity.entityData.GUID)
                    {
                        PlayerQuestManager.Instance.UpdateQuestProgress(activeQuest, 1);
                    }
                    break;
                case QuestType.Loot:
                case QuestType.None:
                default:
                    break;
            }
        }
    }
}
