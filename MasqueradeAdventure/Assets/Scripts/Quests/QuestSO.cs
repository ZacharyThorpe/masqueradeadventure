using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Quest Data", menuName = "Data/Quest", order = 1)]
public class QuestSO : ScriptableObject
{
    public string questName = "Quest";
    public string GUID = string.Empty;
    public bool isUnique = false;

    public NPCSO npcThatOffers = default;

    [SerializeReference]
    public QuestObjective questObjective = default;

    [SerializeReference]
    public QuestReward questReward = default;
}
