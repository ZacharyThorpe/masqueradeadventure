﻿using UnityEngine;
using UnityEngine.UI;

public class LockedOnIcon : MonoBehaviour
{
    [SerializeField] private Image lockOnImage = default;

    private void Start()
    {
        ClearIcon();
    }

    public void ClearIcon()
    {
        lockOnImage.enabled = false;
    }

    public void SoughtTarget()
    {
        lockOnImage.enabled = true;
        lockOnImage.color = Color.yellow;
    }

    public void LockedOnTarget()
    {
        lockOnImage.enabled = true;
        lockOnImage.color = Color.red;
    }
    private void Update()
    {
        transform.LookAt(Camera.main.transform.position);
    }
}
