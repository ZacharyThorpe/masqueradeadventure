using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class ConfirmationUI : UIPanel
{
    [SerializeField] private TextMeshProUGUI txt_message = default;

    [SerializeField] private Button confirmButton = default;

    [SerializeField] private ConfirmationChannelSO confirmationChannelSO = default;

    [SerializeField] private GameObject defaultSelection = default;

    [SerializeField] private GameObject lastSelection = default;

    private void OnEnable()
    {
        confirmationChannelSO.OpenConfirmationAction += SetConfirmation;
    }

    private void OnDisable()
    {
        confirmationChannelSO.OpenConfirmationAction -= SetConfirmation;
    }

    private void Start()
    {
        TogglePanel(false);
    }

    public void CloseConfirmation()
    {
        confirmButton.onClick.RemoveAllListeners();
        TogglePanel(false);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(lastSelection);
    }

    private void SetConfirmation(string message, Action callback)
    {
        txt_message.text = message;
        confirmButton.onClick.AddListener(() => callback?.Invoke());
        confirmButton.onClick.AddListener(() => TogglePanel(false));

        lastSelection = EventSystem.current.currentSelectedGameObject;

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(defaultSelection);

        TogglePanel(true);
    }
}
