﻿using UnityEngine;
using UnityEngine.UI;

public class EnemyHealthBar : MonoBehaviour
{
    [SerializeField] private Slider healthBar = default;

    private float timeActive = 0;

    public void SetHealth(int _heath)
    {
        healthBar.value = _heath;
        healthBar.gameObject.SetActive(true);
        timeActive = 3;
    }

    public void SetMaxHealth(int _maxheath)
    {
        healthBar.maxValue = _maxheath;
    }

    private void Update()
    {
        transform.LookAt(Camera.main.transform.position);
        if(timeActive > 0)
        {
            timeActive -= Time.deltaTime;
        }
        if(timeActive <= 0)
        {
            healthBar.gameObject.SetActive(false);
        }
    }
}
