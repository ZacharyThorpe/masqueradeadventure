using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InspectionGroundUI : MonoBehaviour
{
    public static InspectionGroundUI Instance;

    [SerializeField] private GameObject panel;

    [SerializeField] private Image backgroundImage;
    [SerializeField] private TextMeshProUGUI txtName;
    [SerializeField] private TextMeshProUGUI txtStats;

    [SerializeField] private float m_offsetAmount = 50;

    [SerializeField] private GameObject currentElement;

    [SerializeField] private RarityLookup rarityLookup;

    private void Awake()
    {
        if (Instance != null) Destroy(this);
        Instance = this;
    }

    private void Start()
    {
        panel.SetActive(false);
    }

    private void Update()
    {
        if (currentElement == null)
        {
            ClearInspection();
        }
        else
        {
            Vector2 selectedPosition = Camera.main.WorldToScreenPoint(currentElement.gameObject.transform.position);

            bool rightDirection = selectedPosition.x < Screen.width / 2f;
            Vector2 direction = rightDirection ? Vector2.right : Vector2.left;

            bool upDirection = selectedPosition.y > Screen.height / 2f;

            panel.GetComponent<RectTransform>().pivot = new Vector2(rightDirection ? 0 : 1, upDirection ? 1 : 0);
            panel.transform.position = selectedPosition + (direction * m_offsetAmount);
        }
    }

    public void InspectItem(Item _inspectedItem, GameObject element)
    {
        if (_inspectedItem == null || !_inspectedItem.IsValid() || element == null)
        {
            currentElement = null;
            return;
        }

        currentElement = element;

        ItemDataSO itemSO = DatabaseManager.Instance.FindItem(_inspectedItem.itemData.dataGUID);

        var color = rarityLookup.rarityAssetsByRarity[itemSO.rarity].rarityColor * 0.25f;
        color.a = 1;
        backgroundImage.color = color;

        txtName.text = itemSO.displayName;
        string stats = string.Empty;

        switch (itemSO.itemType)
        {
            case ItemType.None:
                break;
            case ItemType.Weapon:
                var weapon = _inspectedItem as Weapon;
                stats += weapon.GetDescription();
                break;
            case ItemType.Shield:
                var shield = _inspectedItem as Shield;
                stats += shield.GetDescription();
                break;
            case ItemType.Mask:
                var mask = _inspectedItem as Mask;
                stats += mask.GetDescription();
                break;
            case ItemType.ItemAffix:
                var affixItem = _inspectedItem as ItemAffixItem;
                stats += affixItem.GetDescription();
                break;
            case ItemType.Bag:
                var bag = _inspectedItem as Bag;
                stats += bag.GetDescription();
                break;
            case ItemType.Consumable:
                var consume = _inspectedItem as Consumable;
                stats += consume.GetDescription();
                break;
            case ItemType.TradeGood:
            default:
                break;
        }

        txtStats.text = stats;
        panel.SetActive(true);
    }

    public void ClearInspection()
    {
        txtName.text = string.Empty;
        txtStats.text = string.Empty;
        panel.SetActive(false);
    }

}
