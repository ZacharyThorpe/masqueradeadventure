using TMPro;
using UnityEngine;

public class UIForgeStats : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI TXT_stateName;
    [SerializeField] private TextMeshProUGUI TXT_stateAmount;

    public void SetText(string name, string amount)
    {
        TXT_stateName.text = name;
        TXT_stateAmount.text = amount;
    }
}
