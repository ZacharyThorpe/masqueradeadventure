using UnityEngine;
using UnityEngine.UI;

public class UIInteractionTimer : UIPanel
{
    [SerializeField] private Slider interactionSlider;

    private void Start()
    {
        TogglePanel(false);
    }

    public void UpdateTimer(float _timer)
    {
        interactionSlider.value = _timer;
        TogglePanel(_timer > 0);
    }
}
