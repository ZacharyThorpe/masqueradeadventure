using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;

public class QuestPanelReward : MonoBehaviour
{
    [SerializeField] private List<Image >rewardIcon = default;
    [SerializeField] private List<TextMeshProUGUI> rewardAmount = default;

    public void SetQuestImage(Sprite sprite)
    {
        for (int i = 0; i < rewardIcon.Count; i++)
        {
            rewardIcon[i].sprite = sprite;
        }
    }
    public void SetQuestAmount(string amount)
    {
        for (int i = 0; i < rewardIcon.Count; i++)
        {
            rewardAmount[i].text = string.Format("x{0}", amount);
        }
    }
}
