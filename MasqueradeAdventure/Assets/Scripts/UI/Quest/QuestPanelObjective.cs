using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class QuestPanelObjective : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI objectivesName = default;
    [SerializeField] private TextMeshProUGUI objectiveProgress = default;
    [SerializeField] private Slider sliderProgress = default;

    public void SetQuestText(string questObjective, string title, int currentAmount, int amountNeeded)
    {
        objectivesName.text = string.Format("{2}\n<size=75%>{0} {1}</size>", questObjective, amountNeeded, title);
        objectiveProgress.text = string.Format("{0} / {1}", currentAmount, amountNeeded);

        sliderProgress.maxValue = amountNeeded;
        sliderProgress.value = currentAmount;
    }

    public void SetAsComplete()
    {
        objectivesName.color = Color.green;
    }

    public void SetAsFailed()
    {
        objectivesName.color = Color.red;
    }
}
