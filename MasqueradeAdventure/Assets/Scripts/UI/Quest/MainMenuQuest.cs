using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MainMenuQuest : MonoBehaviour
{
    [SerializeField] private GameObject defaultSelection = default;

    [SerializeField] private GameObject questPanel = default;

    [SerializeField] private QuestInfoPanel questInfoPanel;
    [SerializeField] private List<QuestInfoPanel> listOfQuestInfoPanels = new List<QuestInfoPanel>();

    [SerializeField] NPCMenuLister npcMenuList = default;

    private void OnEnable()
    {
        PlayerQuestManager.Instance.QuestUpdated += () => QuestsUpdated(null);
        npcMenuList.NewSelection += QuestsUpdated;
    }

    private void OnDisable()
    {
        PlayerQuestManager.Instance.QuestUpdated -= () => QuestsUpdated(null);
        npcMenuList.NewSelection -= QuestsUpdated;
    }

    private void Start()
    {
        questInfoPanel.OnCompleteQuestRequest += TryToCompleteQuest;
    }

    public void ToggleQuestPanel(bool _isActive)
    {
        questPanel.SetActive(_isActive); 
        
        if (_isActive)
        {
            StartCoroutine(SetNewSelection());
        }
    }

    private IEnumerator SetNewSelection()
    {
        yield return new WaitForEndOfFrame();
        EventSystem.current.SetSelectedGameObject(defaultSelection);
    }

    private void QuestsUpdated(NPCSO npcSO)
    {
        questInfoPanel.gameObject.SetActive(false);

        ClearAllQuestInfoPanels();

        var currentQuests = PlayerQuestManager.Instance.GetCurrentQuests();

        for (int i = 0; i < currentQuests.Count; i++)
        {
            QuestSO questSO = QuestDatabase.Instance.GetQuestByID(currentQuests[i]);

            if (questSO != null && questSO.npcThatOffers.GUID == npcMenuList.CurrentNPCSO.GUID)
            {
                QuestInfoPanel newQuestInfoPanel = Instantiate(questInfoPanel, questInfoPanel.transform.parent);
                newQuestInfoPanel.SetupQuestPanel(questSO, PlayerQuestManager.Instance.IsQuestComplete(questSO), PlayerQuestManager.Instance.GetCurrentProgress(questSO));
                newQuestInfoPanel.gameObject.SetActive(true);
                newQuestInfoPanel.OnCompleteQuestRequest += TryToCompleteQuest;
                listOfQuestInfoPanels.Add(newQuestInfoPanel);
            }
        }
    }

    private void TryToCompleteQuest(QuestSO questSO)
    {
        PlayerQuestManager.Instance.CompleteQuest(questSO);
    }

    private void ClearAllQuestInfoPanels()
    {
        for (int i = 0; i < listOfQuestInfoPanels.Count; i++)
        {
            Destroy(listOfQuestInfoPanels[i].gameObject);
        }
        listOfQuestInfoPanels = new List<QuestInfoPanel>();
    }
}
