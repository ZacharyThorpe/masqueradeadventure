using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class QuestInfoPanel : MonoBehaviour
{
    public Action<QuestSO> OnCompleteQuestRequest;

    [SerializeField] private QuestPanelObjective questObjective;
    [SerializeField] private QuestPanelReward questReward;

    [SerializeField] private GameObject claimElement;
    [SerializeField] private GameObject rewardSimpleElement;
    [SerializeField] private GameObject rewardFullElement;
    [SerializeField] private GameObject progressElement;

    private QuestSO assignedQuestSO;

    public void SetupQuestPanel(QuestSO questSO, bool isComplete, int progress)
    {
        assignedQuestSO = questSO;

        claimElement.SetActive(isComplete);
        rewardSimpleElement.SetActive(isComplete);
        rewardFullElement.SetActive(!isComplete);
        progressElement.SetActive(!isComplete);

        switch (assignedQuestSO.questObjective.questType)
        {
            case QuestType.Kill:
                QuestObjectiveKills killObjective = assignedQuestSO.questObjective as QuestObjectiveKills;
                questObjective.SetQuestText(killObjective.GetQuestObjective(), assignedQuestSO.questName, progress, killObjective.amountNeed);
                break;
            case QuestType.Loot:
                QuestObjectiveLoot lootObjective = assignedQuestSO.questObjective as QuestObjectiveLoot;
                questObjective.SetQuestText(lootObjective.GetQuestObjective(), assignedQuestSO.questName, progress, lootObjective.amountNeed);
                break;
            case QuestType.None:
            default:
                break;
        }

        questReward.SetQuestAmount(assignedQuestSO.questReward.amount.ToString());
        questReward.SetQuestImage(assignedQuestSO.questReward.item.icon);
    }

    public void SetQuestAsComplete()
    {
        OnCompleteQuestRequest?.Invoke(assignedQuestSO);
    }
}
