using System;
using System.Collections;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PostGamePanel : UIPanel
{
    [SerializeField] private GameObject defaultSelection = default;

    [SerializeField] private Image img_extractionBanner;

    [SerializeField] private Sprite sprite_successful;
    [SerializeField] private Sprite sprite_failed;

    [SerializeField] private TextMeshProUGUI postGameScreenText = default;
    [SerializeField] private TextMeshProUGUI txt_extractedLocation = default;
    [SerializeField] private TextMeshProUGUI txt_extractedDuration = default;

    private void Awake()
    {
        panel.SetActive(false);
    }

    private void OnEnable()
    {
        InputModeDetection.OnControlChanged += UpdateKeyboardAndMouseSettings;
    }

    private void OnDisable()
    {
        InputModeDetection.OnControlChanged -= UpdateKeyboardAndMouseSettings;
    }

    public void OpenPostScreenMenu(PostGameResults newPostGameResults)
    {
        postGameScreenText.text = newPostGameResults.message;
        img_extractionBanner.sprite = newPostGameResults.isSuccessful ? sprite_successful : sprite_failed;
        TimeSpan time = TimeSpan.FromSeconds(newPostGameResults.duration);

        txt_extractedDuration.text = string.Format("<size=75%>Duration</size>\n{0}", time.ToString("hh':'mm':'ss"));
        txt_extractedLocation.text = string.Format("<size=75%>Extracted From</size>\n{0}", CultureInfo.CurrentCulture.TextInfo.ToTitleCase(newPostGameResults.extractionLocation.ToLower()));

        OpenPanel();

        StartCoroutine(DelaySelecting());
    }

    private IEnumerator DelaySelecting()
    {
        yield return new WaitForEndOfFrame();

        EventSystem.current.SetSelectedGameObject(defaultSelection);
    }

    private void UpdateKeyboardAndMouseSettings(bool _isMouseAndKeyboard)
    {
        if (panel.activeInHierarchy)
        {
            EventSystem.current.SetSelectedGameObject(null);
            if (!_isMouseAndKeyboard)
            {
                EventSystem.current.SetSelectedGameObject(defaultSelection);
            }

            Cursor.lockState = _isMouseAndKeyboard ? CursorLockMode.None : CursorLockMode.Locked;
        }
    }
}
