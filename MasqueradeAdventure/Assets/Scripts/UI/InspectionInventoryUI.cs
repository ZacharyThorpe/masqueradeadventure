using UnityEngine;
using TMPro;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;

public class InspectionInventoryUI : MonoBehaviour
{
    public static InspectionInventoryUI Instance;

    [SerializeField] private GameObject panel;

    [SerializeField] private TextMeshProUGUI txtName;
    [SerializeField] private TextMeshProUGUI txtStats;

    [SerializeField] private float m_offsetAmount = 50;

    [SerializeField] private GameObject currentElement;

    private void Awake()
    {
        if (Instance != null) Destroy(this);
        Instance = this;
    }

    private void Start()
    {
        panel.SetActive(false);
    }

    private void Update()
    {
        Vector2 selectedPosition = Vector2.zero;

        if (InputModeDetection.currentlyKeyboard == true)
        {
            selectedPosition = new Vector2(Mouse.current.position.ReadValue().x, Mouse.current.position.ReadValue().y);
        }
        else if(EventSystem.current != null && EventSystem.current.currentSelectedGameObject != null)
        {
            selectedPosition = EventSystem.current.currentSelectedGameObject.transform.position;
        }

        bool rightDirection = selectedPosition.x < Screen.width / 2f;
        Vector2 direction = rightDirection ? Vector2.right : Vector2.left;

        bool upDirection = selectedPosition.y > Screen.height / 2f;

        panel.GetComponent<RectTransform>().pivot = new Vector2(rightDirection ? 0 : 1, upDirection ? 1 : 0);
        panel.transform.position = selectedPosition + (direction * m_offsetAmount);

        if (currentElement == null)
        {
            ClearInspection();
        }
    }

    public void ClearInspection()
    {
        txtName.text = string.Empty;
        txtStats.text = string.Empty;
        panel.SetActive(false);
    }

    public void InspectNPC(NPCSO _npcData, GameObject element)
    {
        if (_npcData == null)
        {
            currentElement = null;
            return;
        }

        panel.SetActive(true);
        txtName.text = _npcData.npcName;
        txtStats.text = string.Empty;
    }

    public void InspectItem(Item _inspectedItem, GameObject element, bool _showBuyAmount = false, bool _showLevelUpDescription = false)
    {
        if(_inspectedItem == null || !_inspectedItem.IsValid() || element == null)
        {
            currentElement = null;
            return;
        }

        currentElement = element;

        ItemDataSO itemSO = DatabaseManager.Instance.FindItem(_inspectedItem.itemData.dataGUID);

        txtName.text = itemSO.displayName;
        string stats = string.Empty;

        switch (itemSO.itemType)
        {
            case ItemType.None:
                break;
            case ItemType.Weapon:
                var weapon = _inspectedItem as Weapon;
                if(_showLevelUpDescription) stats += weapon.GetLevelUpDescription();
                else stats += weapon.GetDescription();
                break;
            case ItemType.Shield:
                var shield = _inspectedItem as Shield;
                if (_showLevelUpDescription) stats += shield.GetLevelUpDescription();
                else stats += shield.GetDescription();
                break;
            case ItemType.Mask:
                var mask = _inspectedItem as Mask;
                if (_showLevelUpDescription) stats += mask.GetLevelUpDescription();
                else stats += mask.GetDescription();
                break;
            case ItemType.ItemAffix:
                var affixItem = _inspectedItem as ItemAffixItem;
                stats += affixItem.GetDescription();
                break;
            case ItemType.Bag:
                var bag = _inspectedItem as Bag;
                stats += bag.GetDescription();
                break;
            case ItemType.Consumable:
                var consume = _inspectedItem as Consumable;
                stats += consume.GetDescription();
                break;
            case ItemType.TradeGood:
            default:
                break;
        }
        stats += "\n" + GetBuySellAmounts(_inspectedItem, _showBuyAmount);
        txtStats.text = stats;
        panel.SetActive(true);
    }

    private string GetBuySellAmounts(Item item, bool showBuyAmount)
    {
        ItemDataSO itemSO = DatabaseManager.Instance.FindItem(item.itemData.dataGUID);
        string buySellAmounts = "";
        if (itemSO)
        {
            if(showBuyAmount)
            {
                buySellAmounts += "Buy Amount: " + itemSO.buyAmount + "\n";
            }

            if (itemSO.sellAmount > 0)
            {
                buySellAmounts += "Sell Amount: " + itemSO.sellAmount + "\n";
            }
        }
        return buySellAmounts;
    }
}
