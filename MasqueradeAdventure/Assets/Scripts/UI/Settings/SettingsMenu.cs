using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    [SerializeField] private GameObject defaultSelection = default;
    [SerializeField] private GameObject settingsPanel = default;

    [SerializeField] private Slider slider_master = default;
    [SerializeField] private Slider slider_music = default;
    [SerializeField] private Slider slider_sfx = default;

    [SerializeField] private SettingsSO settings;

    private float cacheMaster = 1;
    private float cacheMusic = 1;
    private float cacheSFX = 1;

    private void Start()
    {
        CacheValues();
        ToggleSettingsPanel(false);
    }

    private void OnEnable()
    {
        InputModeDetection.OnControlChanged += UpdateKeyboardAndMouseSettings;
    }

    private void OnDisable()
    {
        InputModeDetection.OnControlChanged -= UpdateKeyboardAndMouseSettings;
    }

    public void SetMaster(float newValue)
    {
        settings.SetMaster(newValue);
    }

    public void SetMusic(float newValue)
    {
        settings.SetMusic(newValue);
    }

    public void SetSFX(float newValue)
    {
        settings.SetSFX(newValue);
    }

    public void ToggleSettingsPanel(bool _isActive)
    {
        settingsPanel.SetActive(_isActive);

        if(_isActive == true)
        {
            slider_master.SetValueWithoutNotify(settings.masterVolume);
            slider_music.SetValueWithoutNotify(settings.musicVolume);
            slider_sfx.SetValueWithoutNotify(settings.SFXVolume);

            CacheValues();
        }

        if (_isActive)
        {
            StartCoroutine(SetNewSelection());
        }

        if(_isActive == false)
        {
            settings.masterVolume = cacheMaster;
            settings.musicVolume = cacheMusic;
            settings.SFXVolume = cacheSFX;
            settings.RefreshAll();
            settings.SaveSettings();
        }
    }

    public void CacheValues()
    {
        cacheMaster = settings.masterVolume;
        cacheMusic = settings.musicVolume;
        cacheSFX = settings.SFXVolume;
    }

    private IEnumerator SetNewSelection()
    {
        yield return new WaitForEndOfFrame();
        EventSystem.current.SetSelectedGameObject(defaultSelection);
    }

    private void UpdateKeyboardAndMouseSettings(bool _isMouseAndKeyboard)
    {
        if (settingsPanel.activeInHierarchy)
        {
            EventSystem.current.SetSelectedGameObject(null);
            if (!_isMouseAndKeyboard)
            {
                EventSystem.current.SetSelectedGameObject(defaultSelection);
            }

            Cursor.lockState = _isMouseAndKeyboard ? CursorLockMode.None : CursorLockMode.Locked;
        }
    }
}
