﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class FloatingLootNotification : MonoBehaviour
{
    [SerializeField] private CanvasGroup canvas = default;

    [SerializeField] private TextMeshProUGUI TXT_Amount = default;
    [SerializeField] private Image IMG_itemIcon = default;

    private RectTransform rectTransform = default;

    public void PopupImage(Sprite _itemSprite)
    {
        IMG_itemIcon.sprite = _itemSprite;
        IMG_itemIcon.enabled = IMG_itemIcon.sprite != null;
    }
    public void PopupAmount(int amount)
    {
        TXT_Amount.text = string.Format("+{0}",amount.ToString());
    }

    public void SetAnimation()
    {
        canvas.alpha = 0;
        rectTransform = canvas.GetComponent<RectTransform>();
        rectTransform.DOLocalMoveY(25, 1f).SetEase(Ease.OutExpo);

        Sequence seq = DOTween.Sequence();
        seq.Append(canvas.DOFade(1, 0.15f));
        seq.AppendInterval(2);
        seq.Append(canvas.DOFade(0, 1f));
        seq.onComplete += Death;
    }

    private void Death()
    {
        DOTween.Kill(rectTransform);
        Destroy(gameObject);
    }
}
