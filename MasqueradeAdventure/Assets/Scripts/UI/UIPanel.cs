using UnityEngine;

public abstract class UIPanel : MonoBehaviour
{
    [SerializeField] protected GameObject panel = default;

    public virtual void Instantiate()
    {
        panel.SetActive(true);
    }

    public virtual void TogglePanel(bool _isOn)
    {
        if(_isOn == true)
        {
            OpenPanel();
        }
        else
        {
            ClosePanel();
        }
    }

    public virtual void OpenPanel()
    {
        panel.SetActive(true);
    }

    public virtual void ClosePanel()
    {
        panel.SetActive(false);
    }
}
