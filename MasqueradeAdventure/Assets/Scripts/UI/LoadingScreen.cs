using UnityEngine;

public class LoadingScreen : UIPanel
{
    [SerializeField] private Camera loadingCamera;

    public override void TogglePanel(bool _isOn)
    {
        base.TogglePanel(_isOn);
        loadingCamera.enabled = _isOn;
    }
}
