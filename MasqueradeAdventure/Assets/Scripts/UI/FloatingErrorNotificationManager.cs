using UnityEngine;

public class FloatingErrorNotificationManager : MonoBehaviour
{
    [SerializeField] private FloatingErrorNotification floatingText = default;
    [SerializeField] private Transform startPosition = default;

    private void Awake()
    {
        GlobalEventsManager.OnPopupError += TriggerPopup;
    }

    private void OnDestroy()
    {
        GlobalEventsManager.OnPopupError -= TriggerPopup;
    }

    private void Start()
    {
        floatingText.gameObject.SetActive(false);
    }

    public void TriggerPopup(string error)
    {
        var clone = Instantiate(floatingText, floatingText.transform.parent);
        clone.transform.position = startPosition.position;
        clone.PopupError(error);
        clone.gameObject.SetActive(true);
        clone.SetAnimation();
    }
}