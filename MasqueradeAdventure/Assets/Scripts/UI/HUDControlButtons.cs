using System;
using UnityEngine;

public class HUDControlButtons : MonoBehaviour
{
    [SerializeField] private UIButtonElement attackActionButton = default;

    [SerializeField] private UIButtonElement primaryActionButton = default;
    [SerializeField] private UIButtonElement secondaryActionButton = default;
    [SerializeField] private UIButtonElement consumableActionButton = default;

    [SerializeField] private StringSpriteDictionary spriteByString = default;

    public void SetUIActions(Entity entity)
    {
        GlobalEventsManager.OnWeaponUpdated += WeaponUpdated;
        entity.PrimaryActionText += UpdatePrimaryText;
        entity.SecondaryActionText += UpdateSecondaryText;
        entity.ConsumableActionButtonUpdate += UpdateConsumableButton;
    }

    public void ClearUIActions(Entity entity)
    {
        GlobalEventsManager.OnWeaponUpdated -= WeaponUpdated;
        entity.PrimaryActionText -= UpdatePrimaryText;
        entity.SecondaryActionText -= UpdateSecondaryText;
        entity.ConsumableActionButtonUpdate -= UpdateConsumableButton;
    }

    private void UpdatePrimaryText(string newText)
    {
        primaryActionButton.SetIcon(GetSpriteByString(newText));
    }

    private void UpdateSecondaryText(string newText)
    {
        secondaryActionButton.SetIcon(GetSpriteByString(newText));
    }

    private void UpdateConsumableButton(string newText, Sprite sprite)
    {
        consumableActionButton.SetIcon(sprite);
    }

    private void WeaponUpdated(Entity entity, Weapon weapon)
    {
        if (entity.isPlayerControlled == false) return;

        if(weapon == null)
        {
            attackActionButton.SetIcon(GetSpriteByString(""));
        }
        else
        {
            attackActionButton.SetIcon(GetSpriteByString("Attack"));
        }
    }

    private Sprite GetSpriteByString(string spriteRequested)
    {
        if(spriteByString.ContainsKey(spriteRequested))
        {
            return spriteByString[spriteRequested];
        }
        return null;
    }
}

