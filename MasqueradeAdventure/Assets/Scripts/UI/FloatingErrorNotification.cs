using DG.Tweening;
using TMPro;
using UnityEngine;

public class FloatingErrorNotification : MonoBehaviour
{
    [SerializeField] private CanvasGroup canvas = default;

    [SerializeField] private TextMeshProUGUI TXT_Amount = default;

    private RectTransform rectTransform = default;

    public void PopupError(string errorMessage)
    {
        TXT_Amount.text = errorMessage.ToString();
    }

    public void SetAnimation()
    {
        canvas.alpha = 0;
        rectTransform = canvas.GetComponent<RectTransform>();
        rectTransform.DOMoveY(rectTransform.position.y + 25f, 1f).SetEase(Ease.OutExpo);

        Sequence seq = DOTween.Sequence();
        seq.Append(canvas.DOFade(1, 0.15f));
        seq.Append(canvas.DOFade(0, 1f));
        seq.onComplete += Death;
    }

    private void Death()
    {
        DOTween.Kill(rectTransform);
        Destroy(gameObject);
    }
}
