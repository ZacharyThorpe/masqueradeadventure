using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPCMenuLister : MonoBehaviour
{
    public NPCSO CurrentNPCSO = default;
    public Action<NPCSO> NewSelection = default;

    public Image img_splash;

    [Header("NPC Data")]
    [SerializeField] private NPCInteractionType interactionType = NPCInteractionType.Quest;
    [SerializeField] private UINPC npcUI = default;
    [SerializeField] private List<UINPC> listOfNPCs = default;

    private void Start()
    {
        npcUI.gameObject.SetActive(false);
        for (int i = 0; i < listOfNPCs.Count; i++)
        {
            listOfNPCs[i].OnLeftClickEvent = null;
            Destroy(listOfNPCs[i].gameObject);
        }

        listOfNPCs = new List<UINPC>();

        var allNPCs = NPCDatabaseManager.Instance.GetAll();

        allNPCs = allNPCs.FindAll(npc => npc.interactionType.HasFlag(interactionType));

        for (int i = 0; i < allNPCs.Count; i++)
        {
            var clone = Instantiate(npcUI, npcUI.transform.parent);
            clone.Instantiate(allNPCs[i]);
            clone.gameObject.SetActive(true);
            clone.OnLeftClickEvent += SetAsActiveNPC;
            clone.OnHoverEvent += InspectItem;
            clone.button.interactable = true;
            listOfNPCs.Add(clone);
        }

        foreach (var npcPanel in listOfNPCs)
        {
            npcPanel.SetAsSelected(false);
        }

        if (listOfNPCs.Count > 0)
        {
            SetAsActiveNPC(listOfNPCs[0].npcSO, listOfNPCs[0].gameObject);
        }
    }

    private void SetAsActiveNPC(NPCSO _npcSO, GameObject selectedElement)
    {
        CurrentNPCSO = _npcSO;
        foreach (var npcPanel in listOfNPCs)
        {
            npcPanel.SetAsSelected(npcPanel.npcSO.GUID == _npcSO.GUID);
        }

        if(img_splash != null)
        {
            img_splash.sprite = CurrentNPCSO.npcSplash;
            img_splash.enabled = img_splash.sprite != null;
        }

        NewSelection?.Invoke(CurrentNPCSO);
    }

    private void InspectItem(NPCSO _npcData, GameObject selectedElement)
    {
        if (_npcData == null) InspectionInventoryUI.Instance.InspectNPC(null, selectedElement);
        else InspectionInventoryUI.Instance.InspectNPC(_npcData, selectedElement);
    }
}
