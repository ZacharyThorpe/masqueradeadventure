﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using System.Collections.Generic;
using static TMPro.SpriteAssetUtilities.TexturePacker_JsonArray;

public class UIInventoryItem : MonoBehaviour
{
    public InventoryItem item;
    public Button button = default;
    public Image image = default;
    public Image frame = default;
    public TextMeshProUGUI amountText = default;

    [SerializeField] private List<Image> runeElements;

    [SerializeField] private RarityLookup rarityLookup;

    public Action<InventoryItem> OnSendToContainer;
    public Action<InventoryItem> OnSendToInventory;
    public Action<InventoryItem, GameObject> OnDropItem;
    public Action<InventoryItem, GameObject> OnEquipItem;
    public Action<InventoryItem, GameObject> OnSelectItem;

    public Action<InventoryItem, GameObject> OnHoverItem;

    public void Instantiate(InventoryItem _item)
    {
        item = _item;
        RefreshSlot();
    }

    public void RefreshSlot()
    {
        ResetSlot();

        if (item == null || item.item == null || string.IsNullOrEmpty(item.item.itemData.GUID))
        {
            return;
        }

        ItemDataSO itemSO = DatabaseManager.Instance.FindItem(item.item.itemData.dataGUID);

        image.sprite = itemSO.icon;
        frame.sprite = rarityLookup.rarityAssetsByRarity[itemSO.rarity].frameSprite;
        image.enabled = true;

        if (item.amount > 1)
        {
            amountText.text = item.amount.ToString();
        }
        else
        {
            amountText.text = string.Empty;
        }

        if (item.item.GetType().IsSubclassOf(typeof(ItemForgable)))
        {
            ItemForgableData forgableData = item.item.itemData as ItemForgableData;

            for (int i = 0; i < forgableData.listOfAffixesGUIDs.Count; i++)
            {
                ItemAffixSO itemAffix = DatabaseManager.Instance.FindItem(forgableData.listOfAffixesGUIDs[i]) as ItemAffixSO;
                runeElements[i].color = itemAffix.affix.affixColor;
                runeElements[i].transform.parent.gameObject.SetActive(true);
            }
        }
        button.interactable = true;
    }

    public void ResetSlot()
    {
        image.sprite = null;
        image.enabled = false;
        amountText.text = string.Empty;

        DisableAllRunes();
    }

    public void SendToContainer()
    {
        OnSendToContainer?.Invoke(item);
    }

    public void SendToInventory()
    {
        OnSendToInventory?.Invoke(item);
    }

    public void DropItem()
    {
        OnDropItem?.Invoke(item, gameObject);
    }

    public void EquipItem()
    {
        OnEquipItem?.Invoke(item, gameObject);
    }

    public void SelectItem()
    {
        OnSelectItem?.Invoke(item, gameObject);
    }

    public void OnHover()
    {
        OnHoverItem?.Invoke(item, gameObject);
    }

    public void OnExitHover()
    {
        OnHoverItem?.Invoke(null, gameObject);
    }

    private void DisableAllRunes()
    {
        for (int i = 0; i < runeElements.Count; i++)
        {
            runeElements[i].transform.parent.gameObject.SetActive(false);
        }
    }
}
