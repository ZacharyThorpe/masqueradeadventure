using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class InventoryUI : UIPanel
{
    [SerializeField] private GameObject defaultSelection = default;

    [SerializeField] private bool CanDropItems = false;

    [SerializeField] private bool autoSelectInventory = false;
    [SerializeField] private bool containerTakeAll = true;

    [SerializeField] private GameObject extraButton = default;

    [SerializeField] private TextMeshProUGUI TMP_Bag = default;

    [SerializeField] private EntityInventorySO inventory = default;
    [SerializeField] private UIInventoryItem inventoryUI = default;

    [SerializeField] private List<UIInventoryItem> listOfInventoryItems = default;

    [SerializeField] private GameObject containerPanel;
    [SerializeField] private TextMeshProUGUI containerTitle;

    [SerializeField] private UIInventoryItem containerUI = default;
    [SerializeField] private List<UIInventoryItem> listOfContainerItems = default;

    [SerializeField] private Inventory currentOpenedContainer = default;

    [SerializeField] private UIInventoryItem weaponSlot = default;
    [SerializeField] private UIInventoryItem shieldSlot = default;
    [SerializeField] private UIInventoryItem maskSlot = default;
    [SerializeField] private UIInventoryItem bagSlot = default;
    [SerializeField] private UIInventoryItem consumableSlot01 = default;

    [SerializeField] private GameObject containerSendAll = default;
    [SerializeField] private GameObject inventorySendAll = default;

    [SerializeField] private PlatformHinterSwapper swapContainerHelper = default;

    private GameObject lastCategory = default;
    private int selectedIndex = -1;

    public Action CloseInventoryEvent = default;

    private void Awake()
    {
        TogglePanel(false);

        containerPanel.SetActive(false);
        inventoryUI.gameObject.SetActive(false);
        containerUI.gameObject.SetActive(false);

        containerTitle.text = string.Empty;
    }

    private void OnEnable()
    {
        weaponSlot.OnEquipItem += UnEquipWeapon;
        shieldSlot.OnEquipItem += UnEquipShield;
        maskSlot.OnEquipItem += UnEquipMask;
        bagSlot.OnEquipItem += UnEquipBag;
        consumableSlot01.OnEquipItem += UnEquipSlot01;

        weaponSlot.OnSendToContainer += UnEquipWeapon;
        shieldSlot.OnSendToContainer += UnEquipShield;
        maskSlot.OnSendToContainer += UnEquipMask;
        bagSlot.OnSendToContainer += UnEquipBag;
        consumableSlot01.OnSendToContainer += UnEquipSlot01;

        weaponSlot.OnHoverItem += InspectItem;
        shieldSlot.OnHoverItem += InspectItem;
        maskSlot.OnHoverItem += InspectItem;
        bagSlot.OnHoverItem += InspectItem;
        consumableSlot01.OnHoverItem += InspectItem;

        InputModeDetection.OnControlChanged += UpdateKeyboardAndMouseSettings;
    }

    private void OnDisable()
    {
        weaponSlot.OnEquipItem -= UnEquipWeapon;
        shieldSlot.OnEquipItem -= UnEquipShield;
        maskSlot.OnEquipItem -= UnEquipMask;
        bagSlot.OnEquipItem -= UnEquipBag;
        consumableSlot01.OnEquipItem -= UnEquipSlot01;

        weaponSlot.OnSendToContainer -= UnEquipWeapon;
        shieldSlot.OnSendToContainer -= UnEquipShield;
        maskSlot.OnSendToContainer -= UnEquipMask;
        bagSlot.OnSendToContainer -= UnEquipBag;
        consumableSlot01.OnSendToContainer -= UnEquipSlot01;

        weaponSlot.OnHoverItem -= InspectItem;
        shieldSlot.OnHoverItem -= InspectItem;
        maskSlot.OnHoverItem -= InspectItem;
        bagSlot.OnHoverItem -= InspectItem;
        consumableSlot01.OnHoverItem -= InspectItem;

        InputModeDetection.OnControlChanged -= UpdateKeyboardAndMouseSettings;
    }

    public override void TogglePanel(bool _isOn)
    {
        InspectionInventoryUI.Instance.InspectItem(null, null);
        panel.SetActive(_isOn);
        if (_isOn == true)
        {
            InstaniateUI();
        }
        else
        {
            if (extraButton != null)
            {
                extraButton.SetActive(true);
            }
            currentOpenedContainer = null;
            swapContainerHelper.gameObject.SetActive(false);
        }
    }

    public void OpenWithOtherInventory(ref Inventory otherInventory, string _name)
    {
        if(extraButton != null)
        {
            extraButton.SetActive(false);
        }
        containerTitle.text = _name;
        currentOpenedContainer = otherInventory;
        swapContainerHelper.gameObject.SetActive(true);
    }

    public void CloseMenu()
    {
        CloseInventoryEvent?.Invoke();
    }

    private void InstaniateUI()
    {
        if (autoSelectInventory == true)
        {
            SetToFirstInventorySlot();
        }

        RefreshUI();
    }

    public virtual void RefreshUI()
    {
        if (currentOpenedContainer != null)
        {
            InstaniateList(ref inventory.inventory, ref listOfInventoryItems, ref inventoryUI, inventory.GetTotalBagSlots());
            InstaniateList(ref currentOpenedContainer, ref listOfContainerItems, ref containerUI, 20);
            containerPanel.SetActive(true);
        }
        else
        {
            InstaniateList(ref inventory.inventory, ref listOfInventoryItems, ref inventoryUI, inventory.GetTotalBagSlots());
            containerPanel.SetActive(false);
        }

        SetToEquipmentSlot(inventory.mainHand, weaponSlot);
        SetToEquipmentSlot(inventory.shield, shieldSlot);
        SetToEquipmentSlot(inventory.mask, maskSlot);
        SetToEquipmentSlot(inventory.bag, bagSlot);
        SetToEquipmentSlot(inventory.consumable, consumableSlot01);

        SetBagText();

        StartCoroutine(SetNewSelection());

        containerSendAll.SetActive(currentOpenedContainer != null && containerTakeAll);
        inventorySendAll.SetActive(currentOpenedContainer != null);

        InspectItem(null, null);
    }

    private IEnumerator SetNewSelection()
    {
        yield return new WaitForEndOfFrame();

        if (InputModeDetection.currentlyKeyboard == false)
        {
            if (lastCategory != null && selectedIndex > -1)
            {
                if (lastCategory.transform.childCount > 1)
                {
                    int selectedIndexInList = selectedIndex;
                    if (lastCategory.transform.childCount <= selectedIndexInList)
                    {
                        selectedIndexInList = 1;
                    }
                    UIInventoryItem uiInventory = lastCategory.transform.GetChild(selectedIndexInList).GetComponent<UIInventoryItem>();
                    if (uiInventory != null && uiInventory.isActiveAndEnabled && uiInventory.button.interactable)
                    {
                        EventSystem.current.SetSelectedGameObject(uiInventory.button.gameObject);
                    }
                    else
                    {
                        selectedIndexInList++;
                        uiInventory = lastCategory.transform.GetChild(selectedIndexInList).GetComponent<UIInventoryItem>();
                        if (uiInventory != null && uiInventory.isActiveAndEnabled && uiInventory.button.interactable)
                        {
                            EventSystem.current.SetSelectedGameObject(uiInventory.button.gameObject);
                        }
                        else
                        {
                            EventSystem.current.SetSelectedGameObject(defaultSelection);
                        }
                    }
                }
                else
                {
                    EventSystem.current.SetSelectedGameObject(defaultSelection);
                }
            }
            else
            {
                EventSystem.current.SetSelectedGameObject(defaultSelection);
            }

            lastCategory = null;
            selectedIndex = -1;
        }
        else
        {
            EventSystem.current.SetSelectedGameObject(null);
        }
    }

    private void EquipItem(InventoryItem _item, GameObject selectedElement)
    {
        bool isInInventory = IsInInventory(_item);

        ItemDataSO itemSO = DatabaseManager.Instance.FindItem(_item.item.itemData.dataGUID);

        //Need a middle man for holding the item about to be added

        switch (itemSO.itemType)
        {
            case ItemType.Weapon:
                if (inventory.CheckIfMainHandValid() == true)
                {
                    if(isInInventory)
                    {
                        SendToInventory(weaponSlot.item);
                        inventory.EquipWeapon(null);
                    }
                    else if(currentOpenedContainer != null)
                    {
                        SendToContainer(weaponSlot.item);
                        inventory.EquipWeapon(null);
                    }
                }
                if(inventory.CheckIfMainHandValid() == false) EquipItem(_item, isInInventory);
                break;
            case ItemType.Shield:
                if (inventory.CheckIfShieldValid())
                {
                    if (isInInventory)
                    {
                        SendToInventory(shieldSlot.item);
                        inventory.EquipSlot01(null);
                    }
                    else if (currentOpenedContainer != null)
                    {
                        SendToContainer(shieldSlot.item);
                        inventory.EquipSlot01(null);
                    }
                }
                if (inventory.CheckIfShieldValid() == false) EquipItem(_item, isInInventory);
                break;
            case ItemType.Mask:
                if (inventory.CheckIfMaskIsValid())
                {
                    if (isInInventory)
                    {
                        SendToInventory(maskSlot.item);
                        inventory.EquipMask(null);
                    }
                    else if (currentOpenedContainer != null)
                    {
                        SendToContainer(maskSlot.item);
                        inventory.EquipMask(null);
                    }
                }
                if (inventory.CheckIfMaskIsValid() == false) EquipItem(_item, isInInventory);
                break;
            case ItemType.Bag:
                if (inventory.CheckIfBagIsValid())
                {
                    if (isInInventory)
                    {
                        SendToInventory(bagSlot.item);
                        inventory.EquipBag(null);
                    }
                    else if (currentOpenedContainer != null)
                    {
                        SendToContainer(bagSlot.item);
                        inventory.EquipBag(null);
                    }
                }
                if (inventory.CheckIfBagIsValid() == false) EquipItem(_item, isInInventory);
                break;
            case ItemType.Consumable:
                if (inventory.CheckIfConsumableSlot01IsValid())
                {
                    if (isInInventory)
                    {
                        SendToInventory(consumableSlot01.item);
                        inventory.EquipSlot01(null);
                    }
                    else if (currentOpenedContainer != null)
                    {
                        SendToContainer(consumableSlot01.item);
                        inventory.EquipSlot01(null);
                    }
                }
                if (inventory.CheckIfConsumableSlot01IsValid() == false) EquipItem(_item, isInInventory);
                break;
            case ItemType.None:
            case ItemType.TradeGood:
            case ItemType.ItemAffix:
            default:
                if(isInInventory == false)
                {
                    SendToInventory(_item);
                }
                break;
        }
        RefreshUI();
    }

    private void DropItem(InventoryItem _item, GameObject gameObject)
    {
        if (CanDropItems == false) return;

        bool isInInventory = inventory.inventory.items.Any(item => item.item.itemData.GUID == _item.item.itemData.GUID);

        if (isInInventory == true)
        {
            if(_item.amount > 1)
            {
                selectedIndex = inventory.RemoveItem(_item, 1);
            }
            else
            {
                selectedIndex = inventory.RemoveItem(_item);
            }
            GlobalEventsManager.OnItemDropped?.Invoke(_item.item);
            RefreshUI();
        }
        else
        {
            selectedIndex = RemoveItemFromContainer(_item);
            GlobalEventsManager.OnItemDropped?.Invoke(_item.item);
            RefreshUI();
        }

        if (selectedIndex == 0)
        {
            SetToFirstInventorySlot();
        }

        AudioManager.Instance.DropItem();
    }

    private void SendToInventory(InventoryItem _item)
    {
        SendToInventory(_item, true);
    }

    private void SendToInventory(InventoryItem _item, bool _refreshUI = true)
    {
        if (inventory.IsSpaceInBag(_item.item))
        {
            lastCategory = containerUI.transform.parent.gameObject;
            selectedIndex = RemoveItemFromContainer(_item);

            if (selectedIndex == 0)
            {
                SetToFirstInventorySlot();
            }

            inventory.AddAnItem(_item);

            if (_refreshUI == true)
            {
                RefreshUI();
            }
        }
    }
    private void SendToContainer(InventoryItem _item)
    {
        if (currentOpenedContainer == null) return;

        SendToContainer(_item, true);
    }

    private void SendToContainer(InventoryItem _item, bool _refreshUI = true)
    {
        lastCategory = inventoryUI.transform.parent.gameObject;
        selectedIndex = inventory.RemoveItem(_item);

        if (selectedIndex == 0)
        {
            SetToFirstInventorySlot();
        }

        AddToOtherInventory(_item);

        if(_refreshUI == true)
        {
            RefreshUI();
        }
    }

    public void SendEverythingToOtherContainer()
    {
        if (currentOpenedContainer != null)
        {
            for (int i = 0; i < listOfInventoryItems.Count; i++)
            {
                if (listOfInventoryItems[i].item != null)
                {
                    SendToContainer(listOfInventoryItems[i].item, false);
                }
            }
            SetToFirstInventorySlot();

            if(weaponSlot.item.item != null)
            {
                AddToOtherInventory(weaponSlot.item);
                inventory.EquipWeapon(null);
            }
            if (shieldSlot.item.item != null)
            {
                AddToOtherInventory(shieldSlot.item);
                inventory.EquipShield(null);
            }
            if (maskSlot.item.item != null)
            {
                AddToOtherInventory(maskSlot.item);
                inventory.EquipMask(null);
            }
            if (bagSlot.item.item != null)
            {
                AddToOtherInventory(bagSlot.item);
                inventory.EquipBag(null);
            }
            if (consumableSlot01.item.item != null)
            {
                AddToOtherInventory(consumableSlot01.item);
                inventory.EquipSlot01(null);
            }

            RefreshUI();
        }
    }

    public void SendAllToContainer()
    {
        for (int i = 0; i < listOfInventoryItems.Count; i++)
        {
            if (listOfInventoryItems[i].item != null)
            {
                SendToContainer(listOfInventoryItems[i].item, false);
            }
        }
        RefreshUI();
    }

    public void SendAllToInventory()
    {
        for (int i = 0; i < listOfContainerItems.Count; i++)
        {
            SendToInventory(listOfContainerItems[i].item, false);
        }
        RefreshUI();
    }

    private void InstaniateList(ref Inventory listOfInventoryItems, ref List<UIInventoryItem> listOfItems, ref UIInventoryItem inventoryUI, int minSlots)
    {
        inventoryUI.gameObject.SetActive(false);
        for (int i = 0; i < listOfItems.Count; i++)
        {
            listOfItems[i].OnDropItem = null;
            listOfItems[i].OnEquipItem = null;
            listOfItems[i].OnHoverItem = null;
            listOfItems[i].OnSendToContainer = null;
            listOfItems[i].OnSendToInventory = null;

            Destroy(listOfItems[i].gameObject);
        }

        listOfItems = new List<UIInventoryItem>();

        for (int i = 0; i < listOfInventoryItems.items.Count; i++)
        {
            var clone = Instantiate(inventoryUI, inventoryUI.transform.parent);
            clone.Instantiate(listOfInventoryItems.items[i]);
            clone.gameObject.SetActive(true);

            clone.OnSendToInventory += SendToInventory;
            clone.OnSendToContainer += SendToContainer;

            clone.OnDropItem += DropItem;
            clone.OnEquipItem += EquipItem;

            clone.OnHoverItem += InspectItem;

            ItemDataSO itemSO = DatabaseManager.Instance.FindItem(listOfInventoryItems.items[i].item.itemData.dataGUID);

            clone.gameObject.name = string.Format("UIInventory_{0}_{1}", itemSO.displayName, i.ToString("D2") );
            listOfItems.Add(clone);
        }

        for(int i = listOfInventoryItems.items.Count; i < minSlots; i++)
        {
            var clone = Instantiate(inventoryUI, inventoryUI.transform.parent);
            clone.Instantiate(null);
            clone.gameObject.SetActive(true);
            clone.gameObject.name = string.Format("UIInventory_{0}_{1}", "Empty", i.ToString("D2"));
            listOfItems.Add(clone);
        }
    }

    private void SetBagText()
    {
        TMP_Bag.text = string.Format("Bag {0}/{1}", inventory.inventory.items.Count, inventory.GetTotalBagSlots());
    }

    public void AddToOtherInventory(InventoryItem _inventoryItem)
    {
        ItemDataSO itemSO = DatabaseManager.Instance.FindItem(_inventoryItem.item.itemData.dataGUID);

        if (itemSO.stackable == true)
        {
            foreach (var item in currentOpenedContainer.items)
            {
                ItemDataSO itemContainerElement = DatabaseManager.Instance.FindItem(item.item.itemData.dataGUID);

                if (itemContainerElement.GUID == itemSO.GUID)
                {
                    currentOpenedContainer.items[currentOpenedContainer.items.IndexOf(item)].amount += _inventoryItem.amount;
                    return;
                }
            }
        }

        currentOpenedContainer.AddToList(_inventoryItem);
    }

    public int RemoveItemFromContainer(InventoryItem _inventoryItem)
    {
        foreach (var item in currentOpenedContainer.items)
        {
            if (item.item.itemData.GUID == _inventoryItem.item.itemData.GUID)
            {
                int index = currentOpenedContainer.items.IndexOf(item);

                currentOpenedContainer.RemoveAt(index);

                return index;
            }
        }
        return -1;
    }

    private void EquipItem(InventoryItem _item, bool _isInInventory)
    {
        ItemDataSO itemSO = DatabaseManager.Instance.FindItem(_item.item.itemData.dataGUID);

        if (itemSO.itemType == ItemType.Weapon)
        {
            RemoveItemFromInventory(_item, _isInInventory);
            EquipWeapon(_item);
        }
        else if (itemSO.itemType == ItemType.Shield)
        {
            RemoveItemFromInventory(_item, _isInInventory);
            EquipShield(_item);
        }
        else if (itemSO.itemType == ItemType.Mask)
        {
            RemoveItemFromInventory(_item, _isInInventory);
            EquipMask(_item);
        }
        else if (itemSO.itemType == ItemType.Bag)
        {
            RemoveItemFromInventory(_item, _isInInventory);
            EquipBag(_item);
        }
        else if(itemSO.itemType == ItemType.Consumable)
        {
            if(!inventory.CheckIfConsumableSlot01IsValid())
            {
                RemoveItemFromInventory(_item, _isInInventory);
                EquipdSlot01(_item);
            }
            else if(inventory.consumable.item.itemData.dataGUID == itemSO.GUID)
            {
                RemoveItemFromInventory(_item, _isInInventory);
                AddToEquipdSlot01(_item);
            }
        }
        AudioManager.Instance.EquipItem();
    }

    private void RemoveItemFromInventory(InventoryItem _item, bool _isInInventory)
    {
        if(_isInInventory == true)
        {
            lastCategory = inventoryUI.transform.parent.gameObject;
            selectedIndex = inventory.RemoveItem(_item);
        }
        else
        {
            lastCategory = containerUI.transform.parent.gameObject;
            selectedIndex = currentOpenedContainer.items.IndexOf(_item);
            currentOpenedContainer.RemoveFromList(_item);
        }

        if (selectedIndex == 0)
        {
            SetToFirstInventorySlot();
        }
    }

    public void UnEquipWeapon(InventoryItem _item)
    {
        if (_item == null) return;

        if (currentOpenedContainer != null)
        {
            SetToFirstInventorySlot();
            AddToOtherInventory(_item);
            inventory.EquipWeapon(null);
            RefreshUI();
        }
        InspectionInventoryUI.Instance.ClearInspection();
    }

    public void UnEquipWeapon(InventoryItem _item, GameObject selectedElement)
    {
        if (_item == null) return;

        if (inventory != null && inventory.IsSpaceInBag(_item.item))
        {
            SetToFirstInventorySlot();
            inventory.AddAnItem(_item.item);
            inventory.EquipWeapon(null);
            RefreshUI();
        }
        else if(currentOpenedContainer != null)
        {
            SetToFirstInventorySlot();
            AddToOtherInventory(_item);
            inventory.EquipWeapon(null);
            RefreshUI();
        }
        InspectionInventoryUI.Instance.ClearInspection();
    }

    public void UnEquipShield(InventoryItem _item)
    {
        if (_item == null) return;

        if (currentOpenedContainer != null)
        {
            SetToFirstInventorySlot();
            AddToOtherInventory(_item);
            inventory.EquipShield(null);
            RefreshUI();
        }
        InspectionInventoryUI.Instance.ClearInspection();
    }

    public void UnEquipShield(InventoryItem _item, GameObject selectedElement)
    {
        if (_item == null) return;

        if (inventory.IsSpaceInBag(_item.item))
        {
            SetToFirstInventorySlot();
            inventory.AddAnItem(_item.item);
            inventory.EquipShield(null);
            RefreshUI();
        }
        else if (currentOpenedContainer != null)
        {
            SetToFirstInventorySlot();
            AddToOtherInventory(_item);
            inventory.EquipShield(null);
            RefreshUI();
        }
        InspectionInventoryUI.Instance.ClearInspection();
    }

    public void UnEquipMask(InventoryItem _item)
    {
        if (_item == null) return;

        if (currentOpenedContainer != null)
        {
            SetToFirstInventorySlot();
            AddToOtherInventory(_item);
            inventory.EquipMask(null);
            RefreshUI();
        }
        InspectionInventoryUI.Instance.ClearInspection();
    }

    public void UnEquipMask(InventoryItem _item, GameObject selectedElement)
    {
        if (_item == null) return;

        if (inventory.IsSpaceInBag(_item.item))
        {
            SetToFirstInventorySlot();
            inventory.AddAnItem(_item.item);
            inventory.EquipMask(null);
            RefreshUI();
        }
        else if (currentOpenedContainer != null)
        {
            SetToFirstInventorySlot();
            AddToOtherInventory(_item);
            inventory.EquipMask(null);
            RefreshUI();
        }
        InspectionInventoryUI.Instance.ClearInspection();
    }

    public void UnEquipSlot01(InventoryItem _item)
    {
        if (_item == null) return;

        if (currentOpenedContainer != null)
        {
            SetToFirstInventorySlot();
            AddToOtherInventory(_item);
            inventory.EquipSlot01(null);
            RefreshUI();
        }
        InspectionInventoryUI.Instance.ClearInspection();
    }

    public void UnEquipSlot01(InventoryItem _item, GameObject selectedElement)
    {
        if (_item == null) return;

        if (inventory.IsSpaceInBag(_item.item))
        {
            SetToFirstInventorySlot();
            inventory.AddAnItem(_item);
            inventory.EquipSlot01(null);
            RefreshUI();
        }
        else if (currentOpenedContainer != null)
        {
            SetToFirstInventorySlot();
            AddToOtherInventory(_item);
            inventory.EquipSlot01(null);
            RefreshUI();
        }
        InspectionInventoryUI.Instance.ClearInspection();
    }

    public void UnEquipBag(InventoryItem _item)
    {
        if (_item == null) return;

        if (currentOpenedContainer != null)
        {
            SetToFirstInventorySlot();
            AddToOtherInventory(_item);
            inventory.EquipBag(null);
            RefreshUI();
        }
        InspectionInventoryUI.Instance.ClearInspection();
    }


    public void UnEquipBag(InventoryItem _item, GameObject selectedObject)
    {
        if (_item == null) return;

        if (inventory.CanDowngradeToDefaultBagSize() && inventory.SpotOpenForBag())
        {
            SetToFirstInventorySlot();
            inventory.AddAnItem(_item.item);
            inventory.EquipBag(null);
            RefreshUI();
        }
        else if (currentOpenedContainer != null)
        {
            SetToFirstInventorySlot();
            AddToOtherInventory(_item);
            inventory.EquipBag(null);
            RefreshUI();
        }
        InspectionInventoryUI.Instance.ClearInspection();
    }

    public void EquipWeapon(InventoryItem _item)
    {
        inventory.EquipWeapon(_item.item as Weapon);

        SetToEquipmentSlot(inventory.mainHand, weaponSlot);

        RefreshUI();
    }

    public void EquipMask(InventoryItem _item)
    {
        inventory.EquipMask(_item.item as Mask);

        SetToEquipmentSlot(inventory.mask, maskSlot);

        RefreshUI();
    }

    public void EquipShield(InventoryItem _item)
    {
        inventory.EquipShield(_item.item as Shield);

        SetToEquipmentSlot(inventory.shield, shieldSlot);

        RefreshUI();
    }

    public void EquipBag(InventoryItem _item)
    {
        inventory.EquipBag(_item.item as Bag);

        SetToEquipmentSlot(inventory.bag, bagSlot);

        RefreshUI();
    }

    public void EquipdSlot01(InventoryItem _item)
    {
        inventory.EquipSlot01(_item);

        SetToEquipmentSlot(inventory.consumable, consumableSlot01);

        RefreshUI();
    }
    public void AddToEquipdSlot01(InventoryItem _item)
    {
        if(inventory.consumable != null)
        {
            inventory.consumable.amount += _item.amount;
            SetToEquipmentSlot(inventory.consumable, consumableSlot01);
            RefreshUI();
        }
    }

    public void SetToEquipmentSlot(Item _item, UIInventoryItem _inventoryItem)
    {
        _inventoryItem.ResetSlot();
        _inventoryItem.gameObject.SetActive(false);

        if (_item == null || !_item.IsValid()) return;

        ItemDataSO itemSO = DatabaseManager.Instance.FindItem(_item.itemData.dataGUID);

        if (itemSO == null) return;

        InventoryItem newEquipment = new(_item, 1);

        _inventoryItem.Instantiate(newEquipment);
        _inventoryItem.gameObject.SetActive(true);
    }

    public void SetToEquipmentSlot(InventoryItem _item, UIInventoryItem _inventoryItem)
    {
        _inventoryItem.ResetSlot();
        _inventoryItem.gameObject.SetActive(false);

        if (_item == null || _item.item == null || !_item.item.IsValid()) return;

        _inventoryItem.Instantiate(_item);
        _inventoryItem.gameObject.SetActive(true);
    }

    private void InspectItem(InventoryItem _item, GameObject selectedElement)
    {
        if (_item == null) InspectionInventoryUI.Instance.InspectItem(null, selectedElement);
        else InspectionInventoryUI.Instance.InspectItem(_item.item, selectedElement);
    }

    private void SetToFirstInventorySlot()
    {
        if (currentOpenedContainer != null && currentOpenedContainer.items.Count > 0)
        {
            lastCategory = containerUI.transform.parent.gameObject;
            selectedIndex = 0;
        }
        else if (inventory.inventory.items.Count > 0)
        {
            lastCategory = inventoryUI.transform.parent.gameObject;
            selectedIndex = 0;
        }
    }

    private void UpdateKeyboardAndMouseSettings(bool _isMouseAndKeyboard)
    {
        if(panel.activeInHierarchy)
        {
            EventSystem.current.SetSelectedGameObject(null);
            if (!_isMouseAndKeyboard)
            {
                EventSystem.current.SetSelectedGameObject(defaultSelection);
            }

            Cursor.lockState = _isMouseAndKeyboard ? CursorLockMode.None : CursorLockMode.Locked;
        }
    }

    private bool IsInInventory(InventoryItem inventoryItem)
    {
        if(currentOpenedContainer != null)
        {
           if(currentOpenedContainer.items.Any(item => item.item.itemData.GUID == inventoryItem.item.itemData.GUID))
            {
                return false;
            }
        }
        return inventory.inventory.items.Any(item => item.item.itemData.GUID == inventoryItem.item.itemData.GUID);
    }

    private bool IsInIOtherInventory(InventoryItem inventoryItem)
    {
        if (currentOpenedContainer != null)
        {
            return currentOpenedContainer.items.Any(item => item.item.itemData.GUID == inventoryItem.item.itemData.GUID);
        }
        return false;
    }
}
