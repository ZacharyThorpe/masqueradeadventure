using DG.Tweening;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    [SerializeField] private Vector3 rotation;
    [SerializeField] private float speed = 10;

    private void OnEnable()
    {
        GetComponent<RectTransform>().DORotate(rotation, speed, RotateMode.FastBeyond360).SetRelative(true)
           .SetEase(Ease.Linear).SetLoops(-1);
    }

    private void OnDisable()
    {
        DOTween.Kill(this);
    }
}
