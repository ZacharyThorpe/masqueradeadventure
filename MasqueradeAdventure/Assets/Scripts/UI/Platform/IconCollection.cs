using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Sprites/Platform Icon Collection")]
public class IconCollection : ScriptableObject
{
    public PlatformTypeSpriteDictionary iconTypeToSprite = new PlatformTypeSpriteDictionary();
}
