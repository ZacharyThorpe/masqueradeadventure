using IEVO.UI.uGUIDirectedNavigation;
using UnityEngine;
using UnityEngine.UI;

public class NavigationChanger : MonoBehaviour
{
    [SerializeField] private DirectedNavigation directedNavigation = default;

    private void Awake()
    {
        InputModeDetection.OnControlChanged += UpdateDirectionActivation;
    }

    private void OnDestroy()
    {
        InputModeDetection.OnControlChanged -= UpdateDirectionActivation;
    }

    private void UpdateDirectionActivation(bool isKeyboard)
    {
        directedNavigation.Active = !isKeyboard;
        if(isKeyboard)
        {
            GetComponent<Button>().navigation = new Navigation()
            {
                mode = Navigation.Mode.None,
            };
        }
        else
        {
            GetComponent<Button>().navigation = new Navigation()
            {
                mode = Navigation.Mode.Explicit,
            };
        }
    }
}
