using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SliderDetect : MonoBehaviour, IPointerUpHandler
{
    [SerializeField] private Slider effectedSlider;

    private void Awake()
    {
        effectedSlider.onValueChanged.AddListener(OnValueChanged);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (InputModeDetection.currentlyKeyboard == false) return;
        AudioManager.Instance.ButtonSelect();
    }

    private void OnValueChanged(float value)
    {
        if (InputModeDetection.currentlyKeyboard == true) return;
        AudioManager.Instance.ButtonSelect();
    }

}
