using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class HoverSelectState : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, ISelectHandler, IDeselectHandler
{
    public bool isSelected = false;

    [SerializeField] private UnityEvent OnHoverSelect;
    [SerializeField] private UnityEvent OnExit;

    private void OnDisable()
    {
        isSelected = false;
    }

    public void OnPointerEnter(PointerEventData _eventData)
    {
        if (InputModeDetection.currentlyKeyboard == false) return;
        OnHoverSelect?.Invoke();
        isSelected = true;
        AudioManager.Instance.ButtonHover();
    }

    public void OnPointerExit(PointerEventData _eventData)
    {
        if (InputModeDetection.currentlyKeyboard == false) return;
        OnExit?.Invoke();
        isSelected = false;
    }

    void ISelectHandler.OnSelect(BaseEventData eventData)
    {
        if (InputModeDetection.currentlyKeyboard == true) return;
        OnHoverSelect?.Invoke();
        AudioManager.Instance.ButtonHover();
        isSelected = true;
    }

    public void OnDeselect(BaseEventData eventData)
    {
        if (InputModeDetection.currentlyKeyboard == true) return;
        isSelected = false;
    }
}
