using UnityEngine;
using UnityEngine.UI;

public class IconPlatform : MonoBehaviour
{
    [SerializeField] private IconPlatformType consoleTypeIcon;
    [SerializeField] private IconPlatformType keyboardTypeIcon;

    [SerializeField] private Image iconToChange;

    [SerializeField] private IconCollection iconsForPC;
    [SerializeField] private IconCollection iconsForXbox;

    private void Awake()
    {
        InputModeDetection.OnControlChanged += UpdateKeyboardAndMouseSettings;
    }

    private void OnDestroy()
    {
        InputModeDetection.OnControlChanged -= UpdateKeyboardAndMouseSettings;
    }

    private void UpdateKeyboardAndMouseSettings(bool isKeyboard)
    {
        Sprite foundSprite = null;
        if (isKeyboard)
        {
            foundSprite = iconsForPC.iconTypeToSprite[keyboardTypeIcon];
        }
        else
        {
            foundSprite = iconsForXbox.iconTypeToSprite[consoleTypeIcon];
        }

        if (foundSprite != null && iconToChange != null)
        {
            iconToChange.sprite = foundSprite;
        }
    }
}
