public enum ButtonAction
{
    None,
    SendToContainer,
    SendToInventory,
    DropItem,
    EquipItem,
    SelectItem,
    CloseMenu,
}
