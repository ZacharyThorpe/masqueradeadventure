using UnityEngine;
using UnityEngine.UI;

public class PlatformHinterSwapper : MonoBehaviour
{
    [SerializeField] private Sprite iconPC;
    [SerializeField] private Sprite iconAdditivePC;
    [SerializeField] private Sprite iconConsole;

    [SerializeField] private Image iconToChange;
    [SerializeField] private Image iconToChangePCAdditive;

    private void Awake()
    {
        InputModeDetection.OnControlChanged += UpdateKeyboardAndMouseSettings;
    }

    private void OnDestroy()
    {
        InputModeDetection.OnControlChanged -= UpdateKeyboardAndMouseSettings;
    }

    private void Start()
    {
        UpdateKeyboardAndMouseSettings(InputModeDetection.currentlyKeyboard);
    }

    private void UpdateKeyboardAndMouseSettings(bool isKeyboard)
    {
        iconToChange.sprite = isKeyboard ? iconPC : iconConsole;

        iconToChangePCAdditive.sprite = iconAdditivePC;
        iconToChangePCAdditive.transform.parent.gameObject.SetActive(isKeyboard && iconAdditivePC != null);
    }
}
