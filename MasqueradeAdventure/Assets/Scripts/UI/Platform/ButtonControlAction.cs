using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ButtonControlAction : MonoBehaviour
{
    [SerializeField] private HoverSelectState hoverState;
    [SerializeField] private UnityEvent Action;

    public ButtonAction pickedAction;

    private void Awake()
    {
        UIInputManager.Instance.AddToList(this);
    }

    private void OnDestroy()
    {
        UIInputManager.Instance.RemoveFromList(this);
    }

    public bool IsSelected()
    {
        return hoverState.isSelected;
    }

    public void InvokeAction()
    {
        if (InputModeDetection.currentlyKeyboard == false)
        {
            if (EventSystem.current.currentSelectedGameObject == gameObject)
            {
                Action?.Invoke();
                AudioManager.Instance.ButtonSelect();
            }
        }
        else if (hoverState.isSelected == true)
        {
            Action?.Invoke();
            AudioManager.Instance.ButtonSelect();
        }
    }
}
