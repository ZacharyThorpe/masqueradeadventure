﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class InGameHudManager : MonoBehaviour
{
    PlayerAction m_Controls = default;

    [SerializeField] private HealthBar healthBar = default;
    [SerializeField] private InventoryUI inventoryMenu = default;
    [SerializeField] private PostGamePanel postGamePanel = default;
    [SerializeField] private UIInteractionTimer uiInteractionTimer = default;

    [SerializeField] private PlayerSpawner playerSpawner = default;

    [SerializeField] private HUDControlButtons hudcontrols = default;

    private bool menuOpen = false;

    private void Awake()
    {
        GlobalEventsManager.OnDamageTaken += UpdateHealthBar;

        if (playerSpawner != null)
        {
            playerSpawner.HealTaken += UpdateHealthBar;
            playerSpawner.MaskEquipmentUpdate += UpdateHealthBar;

            GlobalEventsManager.OnShieldValueUpdated += UpdateShieldBar;

            playerSpawner.NewSpawn += SetUIActions;
            playerSpawner.Death += ClearUIActions;
            playerSpawner.InteractionTimerUpdated += UpdateInteractionTimer;
        }

        if(inventoryMenu != null)
        {
            inventoryMenu.CloseInventoryEvent += CloseMenu;
        }

        m_Controls = new PlayerAction();
        m_Controls.Player.Inventory.started += (InputAction.CallbackContext _inputCallback) => ToggleMenu();
    }

    public void OnEnable()
    {
        m_Controls.Enable();
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void OnDisable()
    {
        m_Controls.Disable();
    }

    private void Start()
    {
        healthBar.Instantiate();
    }

    private void OnDestroy()
    {
        GlobalEventsManager.OnDamageTaken -= UpdateHealthBar;
    }

    private void ClearUIActions(Entity entity)
    {
        healthBar.SetHealthBars(entity.currentHealth);

        if (entity.equipment.EntityInventory.shield.IsValid() == true)
        {
            healthBar.SetShieldBar((int)entity.equipment.GetCurrentShield().GetCurrentHealth());
        }
        else
        {
            EmptyShieldBar();
        }

        hudcontrols.ClearUIActions(entity);
    }

    private void SetUIActions(Entity entity)
    {
        healthBar.SetHealthBars(entity.currentHealth);
        if(entity.equipment.GetCurrentShield() != null)
        {
            healthBar.SetShieldBar((int)entity.equipment.GetCurrentShield().GetCurrentHealth());
        }
        else
        {
            EmptyShieldBar();
        }
        hudcontrols.SetUIActions(entity);
    }

    private void UpdateHealthBar(Entity _entity, int amount, bool isShield)
    {
        if(_entity == playerSpawner.Player)
        {
            healthBar.UpdateHealthBars(_entity.currentHealth);
        }
    }

    private void UpdateHealthBar(Entity _entity)
    {
        healthBar.UpdateHealthBars(_entity.currentHealth);
    }

    private void UpdateShieldBar(Entity _entity, float value)
    {
        if (_entity.isPlayerControlled == false) return;

        var shield = _entity.equipment.GetCurrentShield();

        if (shield != null)
        {
            healthBar.SetShieldBar(_entity.equipment.EntityInventory.shield.shieldCharges);
            healthBar.UpdateShieldBars((int)value);
        }
        else
        {
            EmptyShieldBar();
        }
    }

    private void EmptyShieldBar()
    {
        healthBar.SetShieldBar(1);
        healthBar.UpdateShieldBars(0);
    }

    private void ToggleMenu()
    {
        menuOpen = !menuOpen;

        inventoryMenu.TogglePanel(menuOpen);

        if(menuOpen == true) 
        {
            AudioManager.Instance.OpenMenu();
        }
        else
        {
            AudioManager.Instance.CloseMenu();
        }

        Time.timeScale = menuOpen ? 0 : 1;

        if (InputModeDetection.currentlyKeyboard == true)
        {
            Cursor.lockState = menuOpen ? CursorLockMode.None : CursorLockMode.Locked;
        }
    }

    private void OpenMenu()
    {
        if (menuOpen == true) return;

        menuOpen = true;

        inventoryMenu.TogglePanel(menuOpen);

        if (menuOpen == true)
        {
            AudioManager.Instance.OpenMenu();
        }
        else
        {
            AudioManager.Instance.CloseMenu();
        }

        Time.timeScale = menuOpen ? 0 : 1;

        if (InputModeDetection.currentlyKeyboard == true)
        {
            Cursor.lockState = menuOpen ? CursorLockMode.None : CursorLockMode.Locked;
        }
    }

    private void CloseMenu()
    {
        if (menuOpen == false) return;

        menuOpen = false;

        inventoryMenu.TogglePanel(menuOpen);

        if (menuOpen == true)
        {
            AudioManager.Instance.OpenMenu();
        }
        else
        {
            AudioManager.Instance.CloseMenu();
        }

        Time.timeScale = menuOpen ? 0 : 1;

        if (InputModeDetection.currentlyKeyboard == true)
        {
            Cursor.lockState = menuOpen ? CursorLockMode.None : CursorLockMode.Locked;
        }
    }

    public void OpenPostScreenMenu(PostGameResults newPostGameResults)
    {
        m_Controls.Disable();

        if (InputModeDetection.currentlyKeyboard == true)
        {
            Cursor.lockState = CursorLockMode.None;
        }

        postGamePanel.OpenPostScreenMenu(newPostGameResults);
    }

    public void OpenInventoryWithContainer(InteractableContainer _interactableContainer)
    {
        inventoryMenu.OpenWithOtherInventory(ref _interactableContainer.containerInventory, "Container");
        OpenMenu();
    }

    private void UpdateInteractionTimer(Entity _entity, float _value)
    {
        uiInteractionTimer.UpdateTimer(_value);
    }
}
