using UnityEngine;
using UnityEngine.UI;

public class UISurpriseIcon : MonoBehaviour
{
    [SerializeField] private Image IMG_SurpriseIcon = default;

    private void Start()
    {
        ClearIcon();
    }

    public void ClearIcon()
    {
        ToggleSurprise(false);
    }

    public void ToggleSurprise(bool isOn)
    {
        IMG_SurpriseIcon.enabled = isOn;
    }

    private void Update()
    {
        transform.LookAt(Camera.main.transform.position);
    }
}
