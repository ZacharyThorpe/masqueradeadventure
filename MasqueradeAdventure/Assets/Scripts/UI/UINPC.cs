using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class UINPC : MonoBehaviour
{
    public NPCSO npcSO;
    public Button button = default;
    public Image image = default;
    public Image frame = default;
    public GameObject selectedImage = default;
    public TextMeshProUGUI m_npcName = default;

    public Action<NPCSO, GameObject> OnLeftClickEvent;
    public Action<NPCSO, GameObject> OnRightClickEvent;
    public Action<NPCSO, GameObject> OnHoverEvent;

    private Color defaultColor = Color.white;

    public void Instantiate(NPCSO _npcSO)
    {
        defaultColor = frame.color;
        npcSO = _npcSO;
        RefreshSlot();
    }

    public void SetAsSelected(bool _isOn)
    {
        selectedImage.SetActive(_isOn);
        frame.color = _isOn ? Color.green : defaultColor;
    }

    public void RefreshSlot()
    {
        if (npcSO == null || string.IsNullOrEmpty(npcSO.GUID))
        {
            return;
        }

        image.sprite = npcSO.npcSprite;
        m_npcName.text = npcSO.npcName;
    }

    public void OnLeftClick()
    {
        OnLeftClickEvent?.Invoke(npcSO, gameObject);
    }
    public void OnRightClick()
    {
        OnRightClickEvent?.Invoke(npcSO, gameObject);
    }

    public void OnHover()
    {
        OnHoverEvent?.Invoke(npcSO, gameObject);
        frame.color = Color.green;
    }

    public void OnExitHover()
    {
        OnHoverEvent?.Invoke(null, gameObject);
        frame.color = defaultColor;
    }
}
