﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : UIPanel
{
    [SerializeField] private Image healthImage = default;
    [SerializeField] private Slider shieldSlider = default;

    private float m_totalHealth = 0;

    void Awake()
    {
        panel.SetActive(false);
    }

    public void SetHealthBars(int totalHealth)
    {
        m_totalHealth = totalHealth;
        healthImage.fillAmount = m_totalHealth;
    }

    public void SetShieldBar(int totalShield)
    {
        shieldSlider.maxValue = totalShield;
    }

    public void UpdateHealthBars(int totalHealth)
    {
        healthImage.fillAmount = totalHealth / m_totalHealth;
    }

    public void UpdateShieldBars(int totalShield)
    {
        if(Time.timeScale <= 0)
        {
            shieldSlider.DOValue(totalShield, 0.15f).OnComplete(() => shieldSlider.value = totalShield);
        }
    }
}
