﻿using UnityEngine;
using UnityEngine.UI;

public class UIButtonElement : MonoBehaviour
{
    public Image IMG_icon = default;

    private bool HasBeenAssigned = false;

    private void Start()
    {
        if (IMG_icon == null) return;
        if (HasBeenAssigned == true) return;
        IMG_icon.enabled = false;
        IMG_icon.sprite = null;
    }

    public void SetIcon(Sprite icon)
    {
        if (IMG_icon == null) return;
        IMG_icon.sprite = icon;
        IMG_icon.enabled = IMG_icon.sprite != null;
        HasBeenAssigned = true;
    }    
}
