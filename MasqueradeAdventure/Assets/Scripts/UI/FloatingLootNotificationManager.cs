﻿using UnityEngine;

public class FloatingLootNotificationManager : MonoBehaviour
{
    [SerializeField] private FloatingLootNotification floatingText = default;
    [SerializeField] private InventoryManager inventoryManager = default;

    [SerializeField] private Transform startPosition = default;

    private void OnEnable()
    {
        inventoryManager.itemLooted += ProgressLoot;
    }

    private void OnDisable()
    {
        inventoryManager.itemLooted -= ProgressLoot;
    }

    private void Start()
    {
        floatingText.gameObject.SetActive(false);
    }

    private void ProgressLoot(Item _item)
    {
        ItemDataSO itemSO = DatabaseManager.Instance.FindItem(_item.itemData.dataGUID);
        PopupImage(itemSO.icon);
    }

    public void PopupImage(Sprite sprite)
    {
        var clone = Instantiate(floatingText, floatingText.transform.parent);
        clone.transform.position = startPosition.position;
        clone.PopupImage(sprite);
        clone.PopupAmount(1);
        clone.gameObject.SetActive(true);
        clone.SetAnimation();
    }
}
