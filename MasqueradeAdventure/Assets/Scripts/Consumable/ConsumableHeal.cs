public class ConsumableHeal : ItemConsumable
{
    public int heal = 1;
    public float time = 1;

    public override string GetDescription()
    {
        return string.Format("Heal {0} over {1} seconds", heal, time);
    }
}
