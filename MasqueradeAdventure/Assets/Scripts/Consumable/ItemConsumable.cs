using System;

[Serializable]
public abstract class ItemConsumable
{
    public ConsumableType consumableType;

    public virtual string GetDescription()
    {
        return "";
    }
}
