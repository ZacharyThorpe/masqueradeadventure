using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using UnityEngine.AddressableAssets;

public class Initialization : MonoBehaviour
{
    [SerializeField] private GameSceneSO _persistentManager = default;
    [SerializeField] private GameSceneSO _sceneToLoad = default;

    [SerializeField] private AssetReference _menuLoadChannel = default;

    private void Start()
    {
        _persistentManager.sceneReference.LoadSceneAsync(LoadSceneMode.Additive, true).Completed += LoadEventChannel;
    }

    private void LoadEventChannel(AsyncOperationHandle<SceneInstance> obj)
    {
        _menuLoadChannel.LoadAssetAsync<LoadEventChannelSO>().Completed += LoadMainMenu;
    }

    private void LoadMainMenu(AsyncOperationHandle<LoadEventChannelSO> obj)
    {
        obj.Result.RaiseEvent(_sceneToLoad);
        SceneManager.UnloadSceneAsync(0);
    }
}
