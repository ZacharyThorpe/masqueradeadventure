using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCDatabaseManager : MonoSingleton<NPCDatabaseManager>
{
    public NPCSO failedToFind = default;

    public List<NPCSO> listOfNPCs = new List<NPCSO>();

    private Dictionary<string, NPCSO> NPCsByGUID = new Dictionary<string, NPCSO>();

    private void Start()
    {
        for (int i = 0; i < listOfNPCs.Count; i++)
        {
            if (NPCsByGUID.ContainsKey(listOfNPCs[i].GUID))
            {
                Debug.Log("item is already added " + listOfNPCs[i].GUID);
            }

            NPCsByGUID.Add(listOfNPCs[i].GUID, listOfNPCs[i]);
        }
    }

    public NPCSO FindItem(string GUID)
    {
        if (!NPCsByGUID.ContainsKey(GUID)) return failedToFind;

        return NPCsByGUID[GUID];
    }

    public List<NPCSO> GetAll()
    {
        return listOfNPCs;
    }
}
