using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class DatabaseManager : MonoSingleton<DatabaseManager>
{
    public ItemDataSO failedToFind = default;

    public List<ItemDataSO> listOfItems = new List<ItemDataSO>();

    private Dictionary<string, ItemDataSO> itemsByGUID = new Dictionary<string, ItemDataSO>();

    [Button("Load All Items")]
    public void LoadAllItems()
    {
        listOfItems.Clear();

        var guids = AssetDatabase.FindAssets($"t:{typeof(ItemDataSO)}");
        foreach (var t in guids)
        {
            var assetPath = AssetDatabase.GUIDToAssetPath(t);
            var asset = AssetDatabase.LoadAssetAtPath<ItemDataSO>(assetPath);
            if (asset != null)
            {
                listOfItems.Add(asset);
            }
        }
    }

    private void Start()
    {
        for (int i = 0; i < listOfItems.Count; i++)
        {
            if (itemsByGUID.ContainsKey(listOfItems[i].GUID))
            {
                Debug.Log("item is already added " + listOfItems[i].GUID);
            }

            itemsByGUID.Add(listOfItems[i].GUID, listOfItems[i]);
        }
    }

    public ItemDataSO FindItem(string GUID)
    {
        if (!itemsByGUID.ContainsKey(GUID)) return failedToFind;

        return itemsByGUID[GUID];
    }
}
