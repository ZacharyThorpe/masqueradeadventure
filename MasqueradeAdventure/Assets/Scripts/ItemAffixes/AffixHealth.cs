using System;

[Serializable]
public class AffixHealth : ItemAffix
{
    public int healthIncrease = 0;

    public override string GetDescription()
    {
        return string.Format("+{0} health", healthIncrease);
    }
}
