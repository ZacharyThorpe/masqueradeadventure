public enum AffixType
{
    None,
    Attack,
    AttackSpeed,
    Health,
    Movement,
}
