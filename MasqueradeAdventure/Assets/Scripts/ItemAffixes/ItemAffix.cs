using System.Collections;
using System;
using UnityEngine;

[Serializable]
public abstract class ItemAffix
{
    public AffixType affixType;
    public Color affixColor = Color.grey;

    public virtual string GetDescription()
    {
        return "";
    }
}
