using System;

[Serializable]
public class AffixDamage : ItemAffix
{
    public int damageIncrease = 0;

    public override string GetDescription()
    {
        return string.Format("+{0} damage", damageIncrease);
    }
}
