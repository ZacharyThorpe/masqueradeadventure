using System;

[Serializable]
public class AffixMovement : ItemAffix
{
    public int movementSpeedIncrease = 0;

    public override string GetDescription()
    {
        return string.Format("{0}% movement", movementSpeedIncrease);
    }
}
