using System;

[Serializable]
public class AffixAttackSpeed : ItemAffix
{
    public float attackSpeed = 0;

    public override string GetDescription()
    {
        return string.Format("+{0}% attackspeed", attackSpeed);
    }
}
