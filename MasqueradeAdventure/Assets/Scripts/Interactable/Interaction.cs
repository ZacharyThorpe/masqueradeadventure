﻿using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Data/Interaction", order = 1)]
public class Interaction : ScriptableObject
{
    public float interactionDistance = 5;
    public float maxdistance = 10;
    public float targetStrength = 1;
    public bool canBeInteractedWith = false;
    public float interactionInSeconds = 1;
    public InteractionType interactionType;
}
