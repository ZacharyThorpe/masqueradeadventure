using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class Breakable : MonoBehaviour, IHitOwner
{
    public Action<Breakable> DropLoot;
    public LootTableSO lootTableSO = default;

    [SerializeField] private MeshDestroy m_meshDestory;
    [SerializeField] private AudioSource m_breakAS;

    private bool broken = false;

    public int GetID()
    {
        return gameObject.GetInstanceID();
    }

    public Transform GetTransform()
    {
        return transform;
    }

    public Item GetRandomLoot()
    {
        return lootTableSO.lootTable.GetRandomLoot();
    }

    public void TakeHit(HitInfo hitInfo, bool isShield)
    {
        if(broken == false)
        {
            broken = true;
            PlayerSoundEffect();
            DropLoot?.Invoke(this);
            m_meshDestory.DestroyMesh();
            gameObject.layer = 16;
            Destroy(gameObject, 3);
        }
    }

    private void PlayerSoundEffect()
    {
        m_breakAS.pitch = Random.Range(0.9f, 1.1f);
        m_breakAS.Play();
    }
}
