﻿using System;

[Flags]
public enum InteractionType
{
    None = 0,
    Player = 1,
    Monster = 2,
    Interactable = 4,
}