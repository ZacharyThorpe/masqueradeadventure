﻿public class InteractableDrop : Interactable
{
    private bool beenPickedUp = false;

    public override bool HasBeenInteractedWith()
    {
        return beenPickedUp;
    }

    public override void Action(Entity entity)
    {
        base.Action(entity);
        beenPickedUp = true;
    }
}
