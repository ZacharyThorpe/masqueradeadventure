﻿using System;
using UnityEngine;

public interface IInteractable
{
    void Action(Entity entity);
    Interaction GetInteractionData();
    GameObject GetGameObject();
    bool HasBeenInteractedWith();

    bool IsActive();
    void SetAsLockedOn();
    void SetAsSought();
    void ClearSeekStatus();
}
