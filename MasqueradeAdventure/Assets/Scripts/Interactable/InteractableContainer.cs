﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class InteractableContainer : Interactable
{
    public Inventory containerInventory = default;

    public float spawnChance = 0.5f;

    public LootTableSO lootTableSO = default;

    public UnityEvent interaction;
    public Action<InteractableContainer> openedContainer;

    [SerializeField] private List<Renderer> renderers = new List<Renderer>();

    public void AssignLootFromLootTable()
    {
        int lootAmount = Random.Range(1, 4);
        containerInventory.items = new List<InventoryItem>();
        for (int i = 0; i < lootAmount; i++)
        {
            Item newItem = lootTableSO.lootTable.GetRandomLoot();
            bool foundStackable = false;

            ItemDataSO data = DatabaseManager.Instance.FindItem(newItem.itemData.dataGUID);

            if(data.stackable == true)
            {
                foreach (InventoryItem item in containerInventory.items)
                {
                    ItemDataSO itemData = DatabaseManager.Instance.FindItem(item.item.itemData.dataGUID);

                    if (itemData.GUID == data.GUID)
                    {
                        item.amount++;
                        foundStackable = true;
                        break;
                    }
                }
            }

            if(foundStackable == false)
            {
                InventoryItem lootItem = new InventoryItem(newItem, 1);
                containerInventory.AddToList(lootItem);
            }
        }
    }

    public void AddToLootTable(Item _item, int _amount)
    {
        InventoryItem lootItem = new InventoryItem(_item, _amount);
        containerInventory.AddToList(lootItem);
    }

    public override bool HasBeenInteractedWith()
    {
        bool beenInteractedWith = containerInventory.items.Count <= 0;
        if (beenInteractedWith == true)
        {
            for (int i = 0; i < renderers.Count; i++)
            {
                for(int j = 0; j < renderers[i].materials.Length; j++) 
                {
                    renderers[i].materials[j].SetFloat("_OutlineScale", 0);
                }
            }
        }

        return beenInteractedWith;
    }

    public override void Action(Entity entity)
    {
        base.Action(entity);
        openedContainer?.Invoke(this);
        interaction?.Invoke();
    }
}
