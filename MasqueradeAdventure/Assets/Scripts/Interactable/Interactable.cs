﻿using System;
using UnityEngine;

public abstract class Interactable : MonoBehaviour, IInteractable
{
    public string InteractionName = "Interactable";

    public Entity entity;
    public Interaction assignInteraction;

    public LockedOnIcon lockedOnIcon;

    public bool isActiveInteractable = false;

    public Action<Interactable, Entity> action;
    public Action onDestoryed;

    public virtual void Action(Entity entity)
    {
        action?.Invoke(this, entity);
    }

    public virtual Interaction GetInteractionData()
    {
        return assignInteraction;
    }
    public virtual GameObject GetGameObject()
    {
        return gameObject;
    }

    public virtual bool HasBeenInteractedWith()
    {
        return false;
    }

    public virtual bool IsActive()
    {
        return isActiveInteractable;
    }

    public Action GetOnDestoryed()
    {
        return onDestoryed;
    }

    public void SetAsLockedOn()
    {
        if(lockedOnIcon != null) lockedOnIcon.LockedOnTarget();
    }

    public void SetAsSought()
    {
        if (lockedOnIcon != null) lockedOnIcon.SoughtTarget();
    }

    public void ClearSeekStatus()
    {
        if (lockedOnIcon != null) lockedOnIcon.ClearIcon();
    }
}
