using UnityEngine;
using UnityEngine.VFX;

public class InteractableExtraction : Interactable
{
    [SerializeField] private VisualEffect systemA, systemB;
    [SerializeField] private MeshRenderer stoneRenderer = default;
    [SerializeField] private AudioSource m_ASextractionReady;

    private Material sharedMaterial;

    private void Awake()
    {
        sharedMaterial = stoneRenderer.sharedMaterial;

        systemA.SetFloat("Toggle", 0);
        systemB.SetFloat("Toggle", 0);
    }

    public override void Action(Entity entity)
    {
        if(isActiveInteractable)
        {
            base.Action(entity);
        }
    }

    public void SetAsActive(bool _isActive)
    {
        isActiveInteractable = _isActive;
        if(isActiveInteractable == true)
        {
            systemA.SetFloat("Toggle", 1);
            systemB.SetFloat("Toggle", 1);
            sharedMaterial.SetColor("_EmissionColor", new Vector4(0, 0.25f, 0, -2f));
            m_ASextractionReady.Play();
        }
        else
        {
            systemA.SetFloat("Toggle", 0);
            systemB.SetFloat("Toggle", 0);
            sharedMaterial.SetColor("_EmissionColor", new Vector4(0,0,0,-10));
            m_ASextractionReady.Stop();
        }
    }
}

