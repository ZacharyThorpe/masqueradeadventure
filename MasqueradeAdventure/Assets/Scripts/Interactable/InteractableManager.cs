﻿using System.Collections.Generic;
using UnityEngine;

public class InteractableManager : MonoBehaviour
{
    public GameStateManager gameStateManger = default;
    public InGameHudManager ingameHUD = default;

    [SerializeField] private MapData mapData = default;

    public List<InteractableExtraction> listOfExtractions = new List<InteractableExtraction>();

    public List<Interactable> listOfInteractables = new List<Interactable>();

    private List<InteractableContainer> listOfContainers = new List<InteractableContainer>();

    [SerializeField] private bool extractionsActive = false;

    private void Start()
    {
        SetUpExtractions();
        GetAllInteractableContainers();

        for (int i = 0; i < listOfContainers.Count; i++)
        {
            listOfContainers[i].openedContainer += InteractableContainer;
            listOfContainers[i].isActiveInteractable = true;
            listOfContainers[i].AssignLootFromLootTable();
        }
    }

    private void Update()
    {
        if(gameStateManger.currentGameTime > mapData.secondsUntilExtractionAllowed && extractionsActive == false)
        {
            for (int i = 0; i < listOfExtractions.Count; i++)
            {
                listOfExtractions[i].SetAsActive(true);
            }
            extractionsActive = true;
        }
    }

    private void GetAllInteractableContainers()
    {
        for (int i = 0; i < listOfInteractables.Count; i++)
        {
            if (listOfInteractables[i].GetType() == typeof(InteractableContainer))
            {
                InteractableContainer interactableContainer = listOfInteractables[i] as InteractableContainer;
                if (Random.Range(0, 1f) > interactableContainer.spawnChance)
                {
                    interactableContainer.gameObject.name += "_" + i;
                    listOfContainers.Add(interactableContainer);
                }
                else
                {
                    interactableContainer.gameObject.SetActive(false);
                }
            }
        }
    }

    private void SetUpExtractions()
    {
        for (int i = 0; i < listOfExtractions.Count; i++)
        {
            if (listOfExtractions[i] == null) continue;
            listOfExtractions[i].action += ExtractionInteraction;
            listOfExtractions[i].SetAsActive(false);
        }
    }

    private void InteractableContainer(InteractableContainer _interactableContainer)
    {
        ingameHUD.OpenInventoryWithContainer(_interactableContainer);
    }

    public void ExtractionInteraction(Interactable _interactableExtraction, Entity entity)
    {
        gameStateManger.extractionLocation = _interactableExtraction.InteractionName;
        entity.entityMotor.SetState(new EntityStateExtraction(entity.entityMotor));
    }
}
