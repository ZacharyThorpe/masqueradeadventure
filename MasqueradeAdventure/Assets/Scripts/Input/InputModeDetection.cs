using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputModeDetection : MonoBehaviour
{
    public static Action<bool> OnControlChanged = default;

    public static bool currentlyKeyboard = false;

    private void Awake()
    {
        InputSystem.onActionChange += InputActionChangedCallback;
    }

    private void InputActionChangedCallback(object obj, InputActionChange change)
    {
        if(change == InputActionChange.ActionPerformed)
        {
            InputAction receivedInputAction = (InputAction)obj;
            InputDevice lastDevice = receivedInputAction.activeControl.device;

            bool isKeyBoardAndMouse = lastDevice.name.Equals("Keyboard") || lastDevice.name.Equals("Mouse");

            if(currentlyKeyboard != isKeyBoardAndMouse)
            {
                currentlyKeyboard = isKeyBoardAndMouse;
                OnControlChanged?.Invoke(isKeyBoardAndMouse);
            }

        }
    }
}
