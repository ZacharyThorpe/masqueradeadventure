﻿using UnityEngine;
using UnityEngine.InputSystem;

public class InputHandlerPlayer : InputHandler
{
    PlayerAction m_Controls = default;

    [SerializeField] private Transform camerasTransform = default;

    void Awake()
    {
        m_Controls = new PlayerAction();
        m_Controls.Player.Targeting.started += (InputAction.CallbackContext _inputCallback) => LTriggerOn?.Invoke();
        m_Controls.Player.Targeting.canceled += (InputAction.CallbackContext _inputCallback) => LTriggerOff?.Invoke();

        m_Controls.Player.Crawling.started += (InputAction.CallbackContext _inputCallback) => RTriggerOn?.Invoke();
        m_Controls.Player.Crawling.canceled += (InputAction.CallbackContext _inputCallback) => RTriggerOff?.Invoke();

        m_Controls.Player.A_Action.started += (InputAction.CallbackContext _inputCallback) => AButtonOn?.Invoke();
        m_Controls.Player.A_Action.canceled += (InputAction.CallbackContext _inputCallback) => AButtonOff?.Invoke();

        m_Controls.Player.B_Action.started += (InputAction.CallbackContext _inputCallback) => BButtonOn?.Invoke();
        m_Controls.Player.B_Action.canceled += (InputAction.CallbackContext _inputCallback) => BButtonOff?.Invoke();

        m_Controls.Player.X_Action.started += (InputAction.CallbackContext _inputCallback) => XButtonOn?.Invoke();
        m_Controls.Player.X_Action.canceled += (InputAction.CallbackContext _inputCallback) => XButtonOff?.Invoke();
    }

    public void OnEnable()
    {
        m_Controls.Enable();
    }

    public void OnDisable()
    {
        m_Controls.Disable();
    }

    public Vector3 GetCameraDirection()
    {
        if (camerasTransform != null)
        {
            Vector3 forward = camerasTransform.TransformDirection(Vector3.forward);
            Vector3 side = camerasTransform.TransformDirection(Vector3.right);
            forward = forward.normalized;
            side = side.normalized;

            forward *= MoveInput.y;
            side *= MoveInput.x;

            return side + forward;
        }
        else
        {
            return transform.forward;
        }
    }

    void Update()
    {
        CameraInput = m_Controls.Camera.Move.ReadValue<Vector2>();
        MoveInput = m_Controls.Player.Move.ReadValue<Vector2>();
        Forward?.Invoke(GetCameraDirection());

        if(Time.timeScale <= 0)
        {
            m_Controls.Disable();
        }
        else
        {
            m_Controls.Enable();
        }
    }
}
