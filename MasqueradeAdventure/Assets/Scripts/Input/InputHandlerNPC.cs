﻿using UnityEngine.AI;
using UnityEngine.Splines;

public class InputHandlerNPC : InputHandler
{
    public Entity entity;
    public NavMeshAgent _navMeshAgent;
    public SplineContainer SplineContainer;

    private AIState _currentState;

    public string currentStateName = string.Empty;

    private bool hasTarget;

    private void OnEnable()
    {
        entity.lockOn.gainTarget += () => _currentState.FoundTarget();
        entity.lockOn.lostTarget += () => _currentState.LostTarget();
    }

    private void OnDestroy()
    {
        entity.lockOn.gainTarget -= () => _currentState.FoundTarget();
        entity.lockOn.lostTarget -= () => _currentState.LostTarget();
    }

    void Start()
    {
        var state = new AIStateRoam(this, _navMeshAgent);
        state.leasePole = transform.position;
        SetState(state);
    }

    public void SetState(AIState aiState)
    {
        StopAllCoroutines();

        _currentState = aiState;

        StartCoroutine(_currentState.BeginState());
    }

    public void SetSplineContainer(SplineContainer splineContainer)
    {
        SplineContainer = splineContainer;
    }

    private void Update()
    {
        if (entity.isActive == false) return;

        if(hasTarget == false)
        {
            if (entity.seeker.currentInteracble != null && entity.seeker.currentInteracble.Equals(null) == false && entity.seeker.IsInteractableInLOS())
            {
                hasTarget = true;
                LTriggerOn?.Invoke();
            }
        }
        else
        {
            if(!entity.lockOn.CheckIfLockOnInteractionIsValid())
            {
                hasTarget = false;
                LTriggerOff?.Invoke();
            }
        }
    }
}
