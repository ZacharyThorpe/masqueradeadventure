using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class UIInputManager : MonoBehaviour
{
    public static UIInputManager Instance;

    private UIInputAction m_Controls = default;

    private List<ButtonControlAction> ListOfActiveButtonControls = new List<ButtonControlAction>();

    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(this);
            return;
        }
        else Instance = this;

        m_Controls = new UIInputAction();

        m_Controls.UIAction.SendToContainer.started += (InputAction.CallbackContext _inputCallback) => InvokeAction(ButtonAction.SendToContainer);
        m_Controls.UIAction.SendToInventory.started += (InputAction.CallbackContext _inputCallback) => InvokeAction(ButtonAction.SendToInventory);
        m_Controls.UIAction.DropItem.started += (InputAction.CallbackContext _inputCallback) => InvokeAction(ButtonAction.DropItem);
        m_Controls.UIAction.EquipItem.started += (InputAction.CallbackContext _inputCallback) => InvokeAction(ButtonAction.EquipItem);
        m_Controls.UIAction.SelectItem.started += (InputAction.CallbackContext _inputCallback) => InvokeAction(ButtonAction.SelectItem);
        m_Controls.UIAction.CloseMenu.started += (InputAction.CallbackContext _inputCallback) => InvokeAction(ButtonAction.CloseMenu);
    }

    private void OnEnable()
    {
        m_Controls.Enable();
    }

    private void OnDisable()
    {
        m_Controls.Disable();
    }

    private void InvokeAction(ButtonAction actionType)
    {
        var foundButton = ListOfActiveButtonControls.Find(button => button.IsSelected() == true && button.pickedAction == actionType && button.isActiveAndEnabled);

        if(foundButton != null) 
        {
            foundButton.InvokeAction();
        }
    }

    public void AddToList(ButtonControlAction buttonControl)
    {
        ListOfActiveButtonControls.Add(buttonControl);
    }

    public void RemoveFromList(ButtonControlAction buttonControl) 
    {
        ListOfActiveButtonControls.Remove(buttonControl);
    }
}
