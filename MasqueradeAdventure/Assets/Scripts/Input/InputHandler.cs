﻿using System;
using UnityEngine;

public abstract class InputHandler : MonoBehaviour
{
    public Vector2 MoveInput;
    public Vector2 CameraInput;
    public bool LeftTrigger;

    public Action LTriggerOn;
    public Action LTriggerOff;

    public Action RTriggerOn;
    public Action RTriggerOff;

    public Action AButtonOn;
    public Action AButtonOff;

    public Action BButtonOn;
    public Action BButtonOff;

    public Action XButtonOn;
    public Action XButtonOff;

    public Action<Vector3> Forward;
}
