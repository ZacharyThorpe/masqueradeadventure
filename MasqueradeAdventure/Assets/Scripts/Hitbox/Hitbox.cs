﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class Hitbox : MonoBehaviour
{
    public string GUID = default;
    public IHitOwner owner;

    public HitInfo hitInfo = new HitInfo();

    private List<string> hurtBoxesHit = new List<string>();

    private void Start()
    {
        GUID = Guid.NewGuid().ToString();
    }

    public void ResetHitbox()
    {
        hurtBoxesHit = new List<string>();
    }

    private void OnTriggerEnter(Collider otherCollider)
    {
        var hurtbox = otherCollider.gameObject.GetComponent<HurtBox>();

        if (hurtbox != null && !hurtBoxesHit.Contains(hurtbox.GUID))
        {
            if (otherCollider.gameObject.GetInstanceID() != owner.GetID())
            {
                if(hurtbox.hitboxType == HitboxType.Shield)
                {
                    hurtbox.HitInfo.direction = (owner.GetTransform().position - hurtbox.owner.GetTransform().position).normalized * hurtbox.HitInfo.hitKnockbackPower;
                    owner.TakeHit(hurtbox.HitInfo, true);
                    hurtbox.shield.DamageShield();
                    hurtBoxesHit.Add(hurtbox.GUID);
                }
                else
                {
                    hitInfo.direction = (hurtbox.owner.GetTransform().position - owner.GetTransform().position).normalized * hitInfo.hitKnockbackPower;
                    hurtbox.owner.TakeHit(hitInfo, false);
                    hurtBoxesHit.Add(hurtbox.GUID);
                }
            }
        }
    }
}
