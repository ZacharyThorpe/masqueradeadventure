using UnityEngine;

public interface IHitOwner
{
    public Transform GetTransform();
    public void TakeHit(HitInfo hitInfo, bool isShield);
    public int GetID();
}
