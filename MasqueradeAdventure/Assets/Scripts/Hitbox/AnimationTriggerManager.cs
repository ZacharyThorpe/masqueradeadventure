using System.Collections.Generic;
using UnityEngine;

public class AnimationTriggerManager : MonoBehaviour
{
    [SerializeField] private EntityVisuals m_entityVisuals;
    [SerializeField] private EntityMotor m_entityMotor;
    [SerializeField] private EntityAnimatorManager m_entityAnimator;

    [SerializeField] private AudioClip m_footStep;
    [SerializeField] private AudioClip m_roll;
    [SerializeField] private AudioClip m_drawn;
    [SerializeField] private AudioClip m_extract;

    [SerializeField] private List<AudioClip> m_attackSounds = new List<AudioClip>();
    [SerializeField] private List<AudioClip> m_hurtSounds = new List<AudioClip>();

    [SerializeField] private AudioSource m_audioSourceSFX;

    [SerializeField] private float pitchRange = 0.5f;

    public void TriggerHitBoxOn()
    {
        if (!m_entityMotor.isAttacking) m_entityVisuals.ToggleWeaponHurtBox(false);
        m_entityVisuals.ToggleWeaponHurtBox(true);
    }

    public void TriggerHitBoxOff()
    {
        m_entityVisuals.ToggleWeaponHurtBox(false);
    }

    public void TriggerArmChange()
    {
        m_audioSourceSFX.pitch = Random.Range(1 - pitchRange, 1 + pitchRange);
        m_audioSourceSFX.PlayOneShot(m_drawn);

        if (m_entityMotor.isArmed)
        {
            m_entityVisuals.AssignWeaponToDrawn();
        }
        else
        {
            m_entityVisuals.AssignWeaponToIdle();
        }
    }

    public void TriggerFootStep()
    {
        if(m_entityAnimator.YAxis > 0.1f || m_entityAnimator.YAxis < -0.1f || m_entityAnimator.StrafeAxis > 0.1f || m_entityAnimator.StrafeAxis < -0.1f)
        {
            m_audioSourceSFX.pitch = Random.Range(1 - pitchRange, 1 + pitchRange);
            m_audioSourceSFX.PlayOneShot(m_footStep);
        }
    }

    public void TriggerRoll()
    {
        m_audioSourceSFX.pitch = Random.Range(1 - pitchRange, 1 + pitchRange);
        m_audioSourceSFX.PlayOneShot(m_roll);
    }

    public void TriggerAttackSound()
    {
        var pickedSound = m_attackSounds[Random.Range(0, m_attackSounds.Count)];
        m_audioSourceSFX.pitch = Random.Range(1 - pitchRange, 1 + pitchRange);
        m_audioSourceSFX.PlayOneShot(pickedSound);
    }

    public void TriggerHit()
    {
        var pickedSound = m_hurtSounds[Random.Range(0, m_hurtSounds.Count)];
        m_audioSourceSFX.pitch = Random.Range(1 - pitchRange, 1 + pitchRange);
        m_audioSourceSFX.PlayOneShot(pickedSound);
    }

    public void Extraction()
    {
        m_audioSourceSFX.pitch = 1;
        m_audioSourceSFX.PlayOneShot(m_extract);
    }
}
