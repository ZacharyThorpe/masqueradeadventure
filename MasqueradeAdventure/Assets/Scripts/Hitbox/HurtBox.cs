using UnityEngine;
using System;

public class HurtBox : MonoBehaviour
{
    public string GUID = default;
    [SerializeField] private GameObject ownerGO;
    public IHitOwner owner;

    public HitboxType hitboxType;

    public HitInfo HitInfo = new HitInfo();

    public ShieldGameObject shield;

    private void Start()
    {
        GUID = Guid.NewGuid().ToString();
        if(ownerGO != null )
        {
            owner = ownerGO.gameObject.GetComponent<IHitOwner>();
        }
    }
}
