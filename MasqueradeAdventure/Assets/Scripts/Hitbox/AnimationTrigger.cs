using UnityEngine;

public class AnimationTrigger : MonoBehaviour
{
    [SerializeField] private AudioSource m_AudioSource;

    public void PlayAudioClip()
    {
        m_AudioSource.pitch = Random.Range(0.9f, 1.1f);
        m_AudioSource.Play();
    }
}
