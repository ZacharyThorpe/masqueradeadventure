﻿public enum HitboxType
{
    Inert,
    Shield,
    Damageable,
}
