﻿using UnityEngine;

public class HitInfo
{
    public Vector3 direction;
    public float hitReoveryTime = 0.25f;
    public float hitKnockbackTime = 0.15f;
    public float hitKnockbackPower = 0.15f;
    public int damage = 1;
}
