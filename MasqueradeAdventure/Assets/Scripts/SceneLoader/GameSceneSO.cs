using UnityEngine;
using UnityEngine.AddressableAssets;

[CreateAssetMenu(fileName = "GameScene", menuName = "Scene Data/GameScene")]
public class GameSceneSO : ScriptableObject
{
	public string gameSceneName;
	public AssetReference sceneReference; //Used at runtime to load the scene from the right AssetBundle
}
