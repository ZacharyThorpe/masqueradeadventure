using System.Collections;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private LoadingScreen loadingScreen = default;

    [Header("Listening to")]
    [SerializeField] private LoadEventChannelSO _loadScene = default;

    [Header("Broadcasting on")]
    [SerializeField] private VoidEventChannelSO _onSceneReady = default;

    private AsyncOperationHandle<SceneInstance> _loadingOperationHandle;

    //Parameters coming from scene loading requests
    private GameSceneSO _sceneToLoad;
    private GameSceneSO _currentlyLoadedScene;

    private bool _isLoading = false;

    private void OnEnable()
    {
        _loadScene.OnLoadingRequested += LoadLevel;
    }

    private void OnDisable()
    {
        _loadScene.OnLoadingRequested -= LoadLevel;
    }

    /// <summary>
    /// Load requested scene
    /// </summary>
    private void LoadLevel(GameSceneSO levelToLoad)
    {
        //Prevent a double-loading
        if (_isLoading)
            return;

        _sceneToLoad = levelToLoad;
        _isLoading = true;

        loadingScreen.TogglePanel(true);

        StartCoroutine(DelayUnload());
    }

    private IEnumerator DelayUnload()
    {
        yield return new WaitForSeconds(0.5f);
        UnloadPreviousScene();
    }

    /// <summary>
    /// Function takes care of removing previously loaded scenes.
    /// </summary>
    private void UnloadPreviousScene()
    {
        if (_currentlyLoadedScene != null) //would be null if the game was started in Initialisation
        {
            if (_currentlyLoadedScene.sceneReference.OperationHandle.IsValid())
            {
                //Unload the scene through its AssetReference, i.e. through the Addressable system
                _currentlyLoadedScene.sceneReference.UnLoadScene().Completed += LoadNewScene;
            }
        }
        else
        {
            LoadNewScene();
        }
    }

    /// <summary>
    /// Kicks off the asynchronous loading of a scene.
    /// </summary>
    private void LoadNewScene(AsyncOperationHandle<SceneInstance> _obj)
    {
        _loadingOperationHandle = _sceneToLoad.sceneReference.LoadSceneAsync(LoadSceneMode.Additive, true, 0);

        _loadingOperationHandle.Completed += OnNewSceneLoaded;
    }
    private void LoadNewScene()
    {
        _loadingOperationHandle = _sceneToLoad.sceneReference.LoadSceneAsync(LoadSceneMode.Additive, true, 0);

        _loadingOperationHandle.Completed += OnNewSceneLoaded;
    }

    private void OnNewSceneLoaded(AsyncOperationHandle<SceneInstance> obj)
    {
        //Save loaded scenes (to be unloaded at next load request)
        _currentlyLoadedScene = _sceneToLoad;

        Scene s = obj.Result.Scene;
        SceneManager.SetActiveScene(s);

        StartCoroutine(DelayLoadingOff());
    }

    private IEnumerator DelayLoadingOff()
    {
        yield return new WaitForSeconds(0.5f);
        loadingScreen.TogglePanel(false);

        _isLoading = false;

        StartLevelGamePlay();
    }

    private void StartLevelGamePlay()
    {
        if (_sceneToLoad != null)
            _onSceneReady.RaiseEvent();
    }
}
