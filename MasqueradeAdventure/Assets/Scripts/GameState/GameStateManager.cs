using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameStateManager : MonoBehaviour
{
    [SerializeField] private LoadEventChannelSO sceneLoader;
    [SerializeField] private GameSceneSO mainMenu;

    [SerializeField] private PlayerSpawner playerSpawner = default;
    [SerializeField] private InGameHudManager inGameHudManager = default;

    [SerializeField] private ConfirmationChannelSO confirmChannel = default;

    public float currentGameTime = 0;
    public string extractionLocation = "";

    private void Awake()
    {
        playerSpawner.Death += LoadFailedExtraction;
        GlobalEventsManager.OnExtracted += LoadSuccessScreen;
    }

    private void OnDestroy()
    {
        GlobalEventsManager.OnExtracted -= LoadSuccessScreen;
    }

    private void Start()
    {
        StartCoroutine(IncrementGameTime());
    }

    private IEnumerator IncrementGameTime()
    {
        while(true)
        {
            currentGameTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    private void LoadFailedExtraction(Entity _deadEntity)
    {
        StartCoroutine(DelayFailed());
    }

    private IEnumerator DelayFailed()
    {
        yield return new WaitForSeconds(3);
        string message = "Extraction Failed";
        SetPostGameScreen(message, false);
    }

    public void LoadSuccessScreen(Entity entity)
    {
        string message = "Extracted";
        SetPostGameScreen(message, true);
    }

    private void SetPostGameScreen(string message, bool isSucessful)
    {
        Time.timeScale = 0;
        playerSpawner.Death -= LoadFailedExtraction;
        var newPostGameResults = new PostGameResults()
        {
            message = message,
            isSuccessful = isSucessful,
            duration = currentGameTime,
            extractionLocation = extractionLocation
        };
        inGameHudManager.OpenPostScreenMenu(newPostGameResults);
    }

    public void LoadMainMenu()
    {
        confirmChannel?.RaiseEvent("You sure you like to leave this map?", ConfirmToLoadMainMenu);
    }

    public void ConfirmToLoadMainMenu()
    {
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.None;
        sceneLoader.RaiseEvent(mainMenu);
    }
}
