﻿using UnityEngine;
using UnityEngine.Splines;

public class SpawnLocation : MonoBehaviour
{
    [SerializeField] private Spawner manager;
    [SerializeField] private Entity currentEntity;
    [SerializeField] private SplineContainer patrolSpline = default;

    public bool CanSpawn()
    {
        if (currentEntity == null) return true;
        return currentEntity.isActive == false;
    }

    public Entity Spawn(EntityData _entityToSpawn)
    {
        if (currentEntity != null) Destroy(currentEntity.gameObject);

        Vector3 spawnPosition = transform.position;

        if (patrolSpline != null)
        {
            spawnPosition = patrolSpline.EvaluatePosition(0);
            if (Physics.Raycast(spawnPosition + Vector3.up, -Vector3.up, out RaycastHit hit))
            {
                spawnPosition = hit.point + Vector3.up;
            }
        }

        var gameObject = Instantiate(_entityToSpawn.prefab, spawnPosition, transform.rotation, manager.entityPool.transform);
        currentEntity = gameObject.GetComponent<Entity>();
        currentEntity.Death += OnDeath;
        if(patrolSpline != null && currentEntity.GetComponent<InputHandlerNPC>() != null) 
        {
            currentEntity.GetComponent<InputHandlerNPC>().SetSplineContainer(patrolSpline);
        }
        return currentEntity;
    }

    private void OnDeath(Entity entity)
    {
        currentEntity = null;
    }

    public void SetManager(Spawner spawnerManager)
    {
        manager = spawnerManager;
    }
}
