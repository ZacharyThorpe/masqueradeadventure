﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public abstract class Spawner : MonoBehaviour
{
    [SerializeField] protected GameStateManager gameStateManager = default;

    public Transform entityPool;

    [SerializeField] private VisualEffect particleDeath;
    [SerializeField] private AudioSource deathSound;

    [SerializeField] protected List<SpawnLocation> entitySpawners = new List<SpawnLocation>();
    public List<Entity> activeEntities = new List<Entity>();

    [SerializeField] protected List<EntityData> entitiesToSpawn = default;

    public Action<Entity> NewSpawn;
    public Action<Entity> HealTaken;
    public Action<Entity> MaskEquipmentUpdate;
    public Action<Entity> ShieldStatsUpdated;
    public Action<Entity> Death;
    public Action<Entity> DropLoot;

    [SerializeField] protected Queue<SpawnLocation> currentActiveSpawns = new Queue<SpawnLocation>();

    private void Awake()
    {
        Instantiate();
    }

    private void Start()
    {
        StartCoroutine(SpawnLoop());
    }

    protected void Instantiate()
    {
        for (int i = 0; i < entitySpawners.Count; i++)
        {
            if (entitySpawners[i] == null) continue;
            entitySpawners[i].SetManager(this);
        }
    }

    protected virtual IEnumerator SpawnLoop()
    {
        yield return new WaitForEndOfFrame();
    }

    protected SpawnLocation FindValidSpawner()
    {
        List<SpawnLocation> randomizedSpawners = new List<SpawnLocation>(entitySpawners);
        randomizedSpawners.Shuffle();
        return randomizedSpawners[0];
    }

    protected void EntityDropLoot(Entity entity)
    {
        entity.DropLoot -= EntityDropLoot;
        DropLoot?.Invoke(entity);
    }

    protected void EntityDeath(Entity entity)
    {
        var clone = Instantiate(particleDeath, transform);
        clone.transform.position = entity.transform.position + Vector3.down;

        var sound = Instantiate(deathSound, transform);
        sound.transform.position = entity.transform.position;
        sound.Play();
        Destroy(sound, 1);

        StartCoroutine(WaitOnDestroyParticle(clone));

        entity.Death -= EntityDeath;
        Death?.Invoke(entity);
        activeEntities.Remove(entity);

        Destroy(entity.gameObject);
    }

    private IEnumerator WaitOnDestroyParticle(VisualEffect deathVFX)
    {
        yield return new WaitForSeconds(0.25f);
        while (deathVFX.aliveParticleCount > 0)
        {
            yield return new WaitForSeconds(0.25f);
        }
        Destroy(deathVFX.gameObject);
    }

    protected void BroadcastHealTaken(Entity _entity)
    {
        HealTaken?.Invoke(_entity);
    }

    protected void BoradcastMaskEquipment(Entity _entity)
    {
        MaskEquipmentUpdate?.Invoke(_entity);
    }
}
