﻿using System;
using System.Collections;
using UnityEngine;

public class PlayerSpawner : Spawner
{
    [SerializeField] private bool playerSpawned = false;

    public Entity Player;
    public Action<Entity, float> InteractionTimerUpdated = default;

    protected override IEnumerator SpawnLoop()
    {
        while (playerSpawned != true)
        {
            if (activeEntities.Count < 1)
            {
                SpawnLocation newSpawner = FindValidSpawner();
                Player = newSpawner.Spawn(entitiesToSpawn[0]);

                Player.Instantiate();
                Player.equipment.Instantiate();
                Player.Death += EntityDeath;
                Player.HealTaken += BroadcastHealTaken;
                Player.MaskEquipmentChanged += MaskEquipmentUpdate;
                Player.InteractingTimer += UpdateInteractionUI;
                Player.isActive = true;

                activeEntities.Add(Player);
                NewSpawn?.Invoke(Player);
                playerSpawned = true;
            }
            yield return new WaitForEndOfFrame();
        }
    }

    private void UpdateInteractionUI(Entity _entity, float _timer)
    {
        InteractionTimerUpdated?.Invoke(_entity, _timer);
    }
}
