﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntitySpawner : Spawner
{
    [SerializeField] private MapData mapData = default;

    [SerializeField] private float spawnDelay = 1;

    int currentSpawnWave = -1;

    protected override IEnumerator SpawnLoop()
    {
        currentSpawnWave = -1;
        while (true)
        {
            UpdateSpawnQueue();

            if (currentActiveSpawns.Count <= 0) yield return new WaitForEndOfFrame();
            else
            {
                SpawnLocation spawnLocation = currentActiveSpawns.Dequeue();
                EntityData entityData = entitiesToSpawn[Mathf.Clamp(currentSpawnWave, 0, entitiesToSpawn.Count - 1)];

                if (entityData != null && spawnLocation.CanSpawn())
                {
                    Entity newEntity = spawnLocation.Spawn(entityData);
                    newEntity.equipment.Instantiate();
                    newEntity.Instantiate();
                    newEntity.SetEntityData(entityData);

                    newEntity.Death += EntityDeath;
                    newEntity.DropLoot += EntityDropLoot;
                    newEntity.HealTaken += BroadcastHealTaken;

                    activeEntities.Add(newEntity);
                    NewSpawn?.Invoke(newEntity);
                    newEntity.isActive = false;
                }
                yield return new WaitForSeconds(spawnDelay);
            }
        }
    }

    private void UpdateSpawnQueue()
    {
        if (currentSpawnWave < 0 && gameStateManager.currentGameTime > mapData.secondsUntilFirstWave)
        {
            RefillQueue();
            currentSpawnWave++;
        }
        else if (currentSpawnWave < 1 && gameStateManager.currentGameTime > mapData.secondsUntilSecondWave)
        {
            RefillQueue();
            currentSpawnWave++;
        }
        else if (currentSpawnWave < 2 && gameStateManager.currentGameTime > mapData.secondsUntilThirdWave)
        {
            RefillQueue();
            currentSpawnWave++;
        }
    }

    private void RefillQueue()
    {
        List<SpawnLocation> spawnLocations = new List<SpawnLocation>(entitySpawners);
        spawnLocations.Shuffle();

        for (int i = 0; i < spawnLocations.Count; i++)
        {
            currentActiveSpawns.Enqueue(spawnLocations[i]);
        }
    }
}
