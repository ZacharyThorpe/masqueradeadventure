using System;

public static class GlobalEventsManager
{
    public static Action<Entity, float> OnShieldValueUpdated;
    public static Action<Entity, Weapon> OnWeaponUpdated;
    public static Action<Entity, Shield> OnShieldUpdated;
    public static Action<Entity> OnExtracted;
    public static Action<Item> OnItemDropped;

    public static Action<string> OnPopupError;

    public static Action<InterableLoot> OnLootDropped;
    public static Action<InterableLoot> OnGroundLootAttempt;
    public static Action<InterableLoot> OnGroundLootSuccess;

    public static Action<Entity, int, bool> OnDamageTaken;
}
