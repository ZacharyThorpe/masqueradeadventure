using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class SaveLoadManager : MonoBehaviour
{
    [SerializeField] private InventorySO storeBuyBackInventory;
    [SerializeField] private InventorySO storeInventory;
    [SerializeField] private InventorySO stashInventory;
    [SerializeField] private EntityInventorySO playerInventory;

    [SerializeField] private SettingsSO settings;

    private void OnEnable()
    {
        storeBuyBackInventory.OnSaveRequest += SaveToDisk;
        storeInventory.OnSaveRequest += SaveToDisk;
        stashInventory.OnSaveRequest += SaveToDisk;
        playerInventory.OnSavePlayerInventoryRequest += SaveToDisk;
        settings.OnSaveRequest += SaveToDisk;
    }

    private void OnDisable()
    {
        storeBuyBackInventory.OnSaveRequest -= SaveToDisk;
        storeInventory.OnSaveRequest -= SaveToDisk;
        stashInventory.OnSaveRequest -= SaveToDisk;
        playerInventory.OnSavePlayerInventoryRequest -= SaveToDisk;
        settings.OnSaveRequest -= SaveToDisk;
    }

    private void Start()
    {
        storeBuyBackInventory.InstaniateSaveRequest();
        storeInventory.InstaniateSaveRequest();
        stashInventory.InstaniateSaveRequest();
        playerInventory.InstaniateSaveRequest();

        LoadFromDisk(storeBuyBackInventory);
        LoadFromDisk(storeInventory);
        LoadFromDisk(stashInventory);
        LoadFromDisk(playerInventory);
        LoadFromDisk(settings);
    }

    private void SaveToDisk(InventorySO _inventory)
    {
        List<InventoryItemJSON> inventoryItemsJSON = new List<InventoryItemJSON>();

        for (int i = 0; i < _inventory.inventory.items.Count; i++)
        {
            InventoryItemJSON itemInventoryJSON = new InventoryItemJSON()
            {
                amount = _inventory.inventory.items[i].amount,
            };

            if (_inventory.inventory.items[i].item.itemData.GetType().IsSubclassOf(typeof(ItemForgableData)))
            {
                ItemForgableData itemForgable = _inventory.inventory.items[i].item.itemData as ItemForgableData;
                itemInventoryJSON.item = itemForgable;
            }
            else
            {
                itemInventoryJSON.item = _inventory.inventory.items[i].item.itemData;
            }
            inventoryItemsJSON.Add(itemInventoryJSON);
        }

        EntityInventoryJSON newJSON = new EntityInventoryJSON()
        {
            items = inventoryItemsJSON,
        };

        JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All, Formatting = Formatting.Indented };

        string jsonString = JsonConvert.SerializeObject(newJSON, settings);

        var fullPath = Path.Combine(Application.persistentDataPath, _inventory.GUID);

        try
        {
            File.WriteAllText(fullPath, jsonString);
        }
        catch (Exception e)
        {
            Debug.LogError($"Failed to write to {fullPath} with exception {e}");
        }
    }

    private void SaveToDisk(EntityInventorySO _entityInventory)
    {
        List<InventoryItemJSON> inventoryItemsJSON = new List<InventoryItemJSON>();

        for (int i = 0; i < _entityInventory.inventory.items.Count; i++)
        {
            InventoryItemJSON itemInventoryJSON = new InventoryItemJSON()
            {
                amount = _entityInventory.inventory.items[i].amount,
            };

            if (_entityInventory.inventory.items[i].item.itemData.GetType().IsSubclassOf(typeof(ItemForgableData)))
            {
                ItemForgableData itemForgable = _entityInventory.inventory.items[i].item.itemData as ItemForgableData;
                itemInventoryJSON.item = itemForgable;
            }
            else
            {
                itemInventoryJSON.item = _entityInventory.inventory.items[i].item.itemData;
            }
            inventoryItemsJSON.Add(itemInventoryJSON);
        }

        EntityInventoryJSON newJSON = new EntityInventoryJSON()
        {
            items = inventoryItemsJSON,
        };

        if (_entityInventory.mainHand != null) newJSON.mainHand = _entityInventory.mainHand.itemData as ItemForgableData;
        if (_entityInventory.shield != null) newJSON.shield = _entityInventory.shield.itemData as ItemForgableData;
        if (_entityInventory.mask != null) newJSON.mask = _entityInventory.mask.itemData as ItemForgableData;
        if (_entityInventory.bag != null) newJSON.bag = _entityInventory.bag.itemData;

        JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All, Formatting = Formatting.Indented };

        string jsonString = JsonConvert.SerializeObject(newJSON, settings);

        var fullPath = Path.Combine(Application.persistentDataPath, _entityInventory.GUID);

        try
        {
            File.WriteAllText(fullPath, jsonString);
        }
        catch (Exception e)
        {
            Debug.LogError($"Failed to write to {fullPath} with exception {e}");
        }
    }

    private void SaveToDisk(SettingsSO _settings)
    {
        AudioJSON audioJSON = new AudioJSON()
        {
            masterVolume = _settings.masterVolume,
            musicVolume = _settings.musicVolume,
            SFXVolume = _settings.SFXVolume,
        };

        JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All, Formatting = Formatting.Indented };

        string jsonString = JsonConvert.SerializeObject(audioJSON, settings);

        var fullPath = Path.Combine(Application.persistentDataPath, "SETTINGS");

        try
        {
            File.WriteAllText(fullPath, jsonString);
        }
        catch (Exception e)
        {
            Debug.LogError($"Failed to write to {fullPath} with exception {e}");
        }
    }



    private void LoadFromDisk(InventorySO _inventory)
    {
        var fullPath = Path.Combine(Application.persistentDataPath, _inventory.GUID);

        if (!File.Exists(fullPath))
        {
            return;
        }

        try
        {
            string result = File.ReadAllText(fullPath);

            JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All, Formatting = Formatting.Indented };

            EntityInventoryJSON json = JsonConvert.DeserializeObject<EntityInventoryJSON>(result, settings);

            _inventory.inventory.items = new List<InventoryItem>();

            for (int i = 0; i < json.items.Count; i++)
            {
                if(json.items[i].item.GetType() == typeof(ItemForgableData))
                {
                    ItemForgableData itemForgable = json.items[i].item as ItemForgableData;

                    InventoryItem inventoryItem = new InventoryItem()
                    {
                        amount = json.items[i].amount,
                    };

                    switch (itemForgable.itemType)
                    {
                        case ItemType.Weapon:
                            Weapon weapon = new Weapon()
                            {
                                itemData = itemForgable,
                            };
                            inventoryItem.item = weapon;
                            break;
                        case ItemType.Shield:
                            Shield shield = new Shield()
                            {
                                itemData = itemForgable,
                            };
                            inventoryItem.item = shield;
                            break;
                        case ItemType.Mask:
                            Mask mask = new Mask()
                            {
                                itemData = itemForgable,
                            };
                            inventoryItem.item = mask;
                            break;
                        case ItemType.TradeGood:
                        case ItemType.ItemAffix:
                        case ItemType.Bag:
                        case ItemType.None:
                        default:
                            break;
                    }
                    _inventory.inventory.items.Add(inventoryItem);
                }
                else
                {
                    InventoryItem inventoryItem = new InventoryItem()
                    {
                        amount = json.items[i].amount,
                    };

                    switch (json.items[i].item.itemType)
                    {
                        case ItemType.TradeGood:
                            TradeGood tradegood = new TradeGood()
                            {
                                itemData = json.items[i].item,
                            };
                            inventoryItem.item = tradegood;
                            break;
                        case ItemType.ItemAffix:
                            ItemAffixItem itemAffix = new ItemAffixItem()
                            {
                                itemData = json.items[i].item,
                            };
                            inventoryItem.item = itemAffix;
                            break;
                        case ItemType.Bag:
                            Bag bag = new Bag()
                            {
                                itemData = json.items[i].item,
                            };
                            inventoryItem.item = bag;
                            break;
                        case ItemType.Consumable:
                            Consumable consumable = new Consumable()
                            {
                                itemData = json.items[i].item,
                            };
                            inventoryItem.item = consumable;
                            break;
                        case ItemType.Mask:
                        case ItemType.Weapon:
                        case ItemType.Shield:
                        case ItemType.None:
                        default:
                            break;
                    }
                    _inventory.inventory.items.Add(inventoryItem);
                }
            }

        }
        catch (Exception e)
        {
            Debug.LogError($"Failed to read from {fullPath} with exception {e}");
        }
    }

    private void LoadFromDisk(EntityInventorySO _inventory)
    {
        var fullPath = Path.Combine(Application.persistentDataPath, _inventory.GUID);

        if (!File.Exists(fullPath))
        {
            return;
        }

        try
        {
            string result = File.ReadAllText(fullPath);

            JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All, Formatting = Formatting.Indented };

            EntityInventoryJSON json = JsonConvert.DeserializeObject<EntityInventoryJSON>(result, settings);

            _inventory.inventory.items = new List<InventoryItem>();

            for (int i = 0; i < json.items.Count; i++)
            {
                if (json.items[i].item.GetType() == typeof(ItemForgableData))
                {
                    ItemForgableData itemForgable = json.items[i].item as ItemForgableData;

                    InventoryItem inventoryItem = new InventoryItem()
                    {
                        amount = json.items[i].amount,
                    };

                    switch (itemForgable.itemType)
                    {
                        case ItemType.Weapon:
                            Weapon weapon = new Weapon()
                            {
                                itemData = itemForgable,
                            };
                            inventoryItem.item = weapon;
                            break;
                        case ItemType.Shield:
                            Shield shield = new Shield()
                            {
                                itemData = itemForgable,
                            };
                            inventoryItem.item = shield;
                            break;
                        case ItemType.Mask:
                            Mask mask = new Mask()
                            {
                                itemData = itemForgable,
                            };
                            inventoryItem.item = mask;
                            break;
                        case ItemType.TradeGood:
                        case ItemType.ItemAffix:
                        case ItemType.Bag:
                        case ItemType.None:
                        default:
                            break;
                    }
                    _inventory.inventory.items.Add(inventoryItem);
                }
                else
                {
                    InventoryItem inventoryItem = new InventoryItem()
                    {
                        amount = json.items[i].amount,
                    };

                    switch (json.items[i].item.itemType)
                    {
                        case ItemType.TradeGood:
                            TradeGood tradegood = new TradeGood()
                            {
                                itemData = json.items[i].item,
                            };
                            inventoryItem.item = tradegood;
                            break;
                        case ItemType.ItemAffix:
                            ItemAffixItem itemAffix = new ItemAffixItem()
                            {
                                itemData = json.items[i].item,
                            };
                            inventoryItem.item = itemAffix;
                            break;
                        case ItemType.Bag:
                            Bag bag = new Bag()
                            {
                                itemData = json.items[i].item,
                            };
                            inventoryItem.item = bag;
                            break;
                        case ItemType.Consumable:
                            Consumable consumable = new Consumable()
                            {
                                itemData = json.items[i].item,
                            };
                            inventoryItem.item = consumable;
                            break;
                        case ItemType.Mask:
                        case ItemType.Weapon:
                        case ItemType.Shield:
                        case ItemType.None:
                        default:
                            break;
                    }
                    _inventory.inventory.items.Add(inventoryItem);
                }
            }
            if(json.mainHand != null && json.mainHand.itemType == ItemType.Weapon)
            {
                Weapon weapon = new Weapon()
                {
                    itemData = json.mainHand,
                };
                _inventory.mainHand = weapon;
            }
            if (json.shield != null && json.shield.itemType == ItemType.Shield)
            {
                Shield shield = new Shield()
                {
                    itemData = json.shield,
                };
                _inventory.shield = shield;
            }
            if (json.mask != null && json.mask.itemType == ItemType.Mask)
            {
                Mask mask = new Mask()
                {
                    itemData = json.mask,
                };
                _inventory.mask = mask;
            }
            if (json.bag != null && json.bag.itemType == ItemType.Bag)
            {
                Bag bag = new Bag()
                {
                    itemData = json.bag,
                };
                _inventory.bag = bag;
            }
            if(json.consumeSlot01 != null && json.consumeSlot01.item.itemType == ItemType.Consumable)
            {
                InventoryItem inventoryConsumable = new InventoryItem()
                {
                    amount = json.consumeSlot01.amount,
                    item = new Consumable()
                    {
                        itemData = json.consumeSlot01.item,
                    },
                };
                _inventory.consumable = inventoryConsumable;
            }
        }
        catch (Exception e)
        {
            Debug.LogError($"Failed to read from {fullPath} with exception {e}");
        }
    }

    private void LoadFromDisk(SettingsSO _settings)
    {
        var fullPath = Path.Combine(Application.persistentDataPath, "settings");

        if (!File.Exists(fullPath))
        {
            return;
        }

        try
        {
            string result = File.ReadAllText(fullPath);

            JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All, Formatting = Formatting.Indented };

            AudioJSON audioJson = JsonConvert.DeserializeObject<AudioJSON>(result, settings);

            _settings.masterVolume = audioJson.masterVolume;
            _settings.musicVolume = audioJson.musicVolume;
            _settings.SFXVolume = audioJson.SFXVolume;

            _settings.RefreshAll();
        }
        catch (Exception e)
        {
            Debug.LogError($"Failed to read from {fullPath} with exception {e}");
        }
    }
}
