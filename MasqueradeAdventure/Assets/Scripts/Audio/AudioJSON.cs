public class AudioJSON
{
    public float masterVolume = 1f;
    public float musicVolume = 1f;
    public float SFXVolume = 1f;

    public AudioJSON()
    {
        masterVolume = 1f;
        musicVolume = 1f;
        SFXVolume = 1f;
    }
}
