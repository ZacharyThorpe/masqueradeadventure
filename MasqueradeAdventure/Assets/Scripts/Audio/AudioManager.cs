using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioSource soundEffect;

    [SerializeField] private AudioClip openMenu;
    [SerializeField] private AudioClip closeMenu;
    [SerializeField] private AudioClip equipSound;
    [SerializeField] private AudioClip buttonSelect;
    [SerializeField] private AudioClip buttonHover;
    [SerializeField] private AudioClip itemDrop;
    [SerializeField] private AudioClip itemPickup;

    private Action soundEffectAction;

    private void Awake()
    {
        if (Instance != null) Destroy(this);
        Instance = this;
    }

    private void Start()
    {
        StartCoroutine(AudioEffectCheck());
    }

    public void ToggleMusic(bool _toggleMusic)
    {
        if(_toggleMusic == true)
        {
            audioSource.Play();
        }
        audioSource.DOFade(_toggleMusic ? 1 : 0, 1f);
    }

    private IEnumerator AudioEffectCheck()
    {
        while(true)
        {
            yield return new WaitForEndOfFrame();
            if (soundEffectAction != null)
            {
                soundEffectAction?.Invoke();
                soundEffectAction = null;
            }
        }
    }

    public void OpenMenu()
    {
        soundEffectAction = () => soundEffect.PlayOneShot(openMenu);
    }

    public void CloseMenu()
    {
        soundEffectAction = () => soundEffect.PlayOneShot(closeMenu);
    }

    public void EquipItem()
    {
        soundEffectAction = () => soundEffect.PlayOneShot(equipSound);
    }

    public void ButtonSelect()
    {
        if(soundEffectAction == null)
        {
            soundEffectAction = () => soundEffect.PlayOneShot(buttonSelect);
        }
    }

    public void ButtonHover()
    {
        if (soundEffectAction == null)
        {
            soundEffectAction = () => soundEffect.PlayOneShot(buttonHover);
        }
    }

    public void DropItem()
    {
        soundEffectAction = () => soundEffect.PlayOneShot(itemDrop);
    }

    public void LootItem()
    {
        soundEffectAction = () => soundEffect.PlayOneShot(itemPickup);
    }
}
