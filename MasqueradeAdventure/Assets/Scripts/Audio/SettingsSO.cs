using System;
using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(fileName = "AudioSettings", menuName = "Data/Audio Settings", order = 1)]
public class SettingsSO : ScriptableObject
{
    public float masterVolume = 1f;
    public float musicVolume = 1f;
    public float SFXVolume = 1f;

    public float lowDPForSound = -20f;

    public float sfxMax = 10;

    [SerializeField] private AudioMixer audioMixer = default;

    public Action<SettingsSO> OnSaveRequest = default;

    public void SetMaster(float newValue)
    {
        masterVolume = newValue;
        RefreshAll();
    }

    public void SetMusic(float newValue)
    {
        musicVolume = newValue;
        RefreshAll();
    }

    public void SetSFX(float newValue)
    {
        SFXVolume = newValue;
        RefreshAll();
    }

    public void RefreshAll()
    {
        audioMixer.SetFloat("masterVolume", RemapVolumeToDP(masterVolume));
        audioMixer.SetFloat("musicVolume", RemapVolumeToDP(musicVolume));
        audioMixer.SetFloat("sfxVolume", RemapVolumeToDP(SFXVolume, sfxMax));
    }

    public void SaveSettings()
    {
        OnSaveRequest?.Invoke(this);
    }

    private float RemapVolumeToDP(float value)
    {
        value = Remap.RemapFloat(value, 0, lowDPForSound, 1, 0);
        if (value <= lowDPForSound)
        {
            value = -80;
        }
        return value;
    }

    private float RemapVolumeToDP(float value, float max)
    {
        value = Remap.RemapFloat(value, 0, lowDPForSound, 1, sfxMax);
        if (value <= lowDPForSound)
        {
            value = -80;
        }
        return value;
    }
}
