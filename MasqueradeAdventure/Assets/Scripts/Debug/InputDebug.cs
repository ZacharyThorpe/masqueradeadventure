﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputDebug : MonoBehaviour
{
    [SerializeField] private InputHandler handler = default;

    [SerializeField] private Transform origin = default;

    [SerializeField] private Transform directionOrigin = default;
    [SerializeField] private GameObject playerForward = default;


    [SerializeField] private Transform lastDirectionOrigin = default;
    [SerializeField] private GameObject lastDirection = default;
    [SerializeField] private EntityMotor entityMotor = default;

    private void Start()
    {
        if(handler != null)
        {
            handler.Forward += SetPlayerForward;
        }
    }

    private void SetPlayerForward(Vector3 value)
    {
        if(value != Vector3.zero)
        {
            playerForward.transform.forward = value;
        }
    }

    private void Update()
    {
        if(origin != null)
        {
            directionOrigin.position = origin.transform.position;
            lastDirectionOrigin.position = origin.transform.position;
        }

        if(entityMotor != null)
        {
            if(entityMotor.lastDirection != Vector3.zero)
            {
                lastDirection.transform.forward = entityMotor.lastDirection;
            }
        }
    }
}
