using UnityEngine;

[CreateAssetMenu(menuName = "Map/Map Settings")]
public class MapData : ScriptableObject
{
    public float secondsUntilExtractionAllowed = 60;
    public float timerUntilDark = 120;

    public float secondsUntilFirstWave = 1;
    public float secondsUntilSecondWave = 60;
    public float secondsUntilThirdWave = 120;
}
