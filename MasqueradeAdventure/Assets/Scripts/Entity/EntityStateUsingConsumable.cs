using System.Collections;
using UnityEngine;

public class EntityStateUsingConsumable : EntityStates
{
    public float interactionTimeInSeconds = 1;
    public InventoryItem currentConsumable = default;

    public EntityStateUsingConsumable(EntityMotor entityMotor) : base(entityMotor)
    {

    }

    public override void Move()
    {
        Reset();
        _entityMotor.SetArmedState();
    }

    public override IEnumerator BeginState()
    {
        _entityAnimator.ResetVariables();

        _entityMotor.isInteracting = true;
        _entityAnimator.UseConsume();

        float timer = 0;

        while (timer < interactionTimeInSeconds)
        {
            timer += Time.deltaTime;
            _entity.UpdateInteractionTimer(timer / interactionTimeInSeconds);
            yield return new WaitForEndOfFrame();
        }

        var data = DatabaseManager.Instance.FindItem(currentConsumable.item.itemData.dataGUID) as ConsumableSO;

        switch (data.consumable.consumableType)
        {
            case ConsumableType.Healing:
                var healingConsumable = data.consumable as ConsumableHeal;
                _entity.TakeHeal(healingConsumable.heal);
                break;
            case ConsumableType.None:
            default:
                break;
        }

        _entity.equipment.ConsumeItem(-1);
        _entityAnimator.FinishConsume();
        _entity.UpdateInteractionTimer(0);
        _entityMotor.isInteracting = false;
        _entityMotor.SetArmedState();
    }

    public override void Reset()
    {
        base.Reset();
        _entityAnimator.DoneLooting();
        _entityMotor.isInteracting = false;
        _entity.UpdateInteractionTimer(0);
    }
}
