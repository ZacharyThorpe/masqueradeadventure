using NHance.Assets.Scripts.Enums;
using NHance.Assets.Scripts;
using UnityEngine;
using NHance.Assets.Scripts.Items;
using UnityEngine.VFX;
using DG.Tweening;
using System.Collections;
using System.Linq;

public class AvatarEntityVisuals : EntityVisuals
{
    [SerializeField] private NHAvatar avatar;
    [SerializeField] private Transform scalePivot;
    [SerializeField] private Transform yTranslate;
    [SerializeField] private Transform vfxPivot;

    [SerializeField] private VisualEffect extractionVFXs;
    [SerializeField] private VisualEffect burstExtractionVFXs;

    [SerializeField] private Light extractionLight;

    [SerializeField] private int rate = 5;
    [SerializeField] private float durationUntilCollapse = 2f;
    [SerializeField] private float collapseTime = 1f;
    [SerializeField] private float maxBrightness = 1000;
    [SerializeField] private float heightOffset = 1.5f;

    private WeaponGameObject currentIdleWeapon;
    private Weapon weapon;

    public override WeaponGameObject UpdateWeapon(Weapon _weapon)
    {
        weapon = _weapon;

        if (currentIdleWeapon != null)
        {
            Destroy(currentIdleWeapon.gameObject);
            currentIdleWeapon = null;
        }

        if(avatar.Cache[ItemTypeEnum.WeaponR] != null)
        {
            avatar.ClearItems(ItemTypeEnum.WeaponR);
            avatar.Clean().Compile();
        }

        if (_weapon == null || _weapon.IsValid() == false || _weapon.EquipmentPrefab == null || _weapon.EquipmentPrefab.GetComponent<NHItem>() == null)
        {
            UpdateLayersAndWeaponVisuals();
            return null;
        }

        avatar.SetItem(_weapon.EquipmentPrefab.GetComponent<NHItem>())
            .Clean()
            .Compile();

        var attachedBone = avatar.SocketMap[BoneType.BackR];

        if (attachedBone != null)
        {
            var newIdleWeapon = Instantiate(_weapon.EquipmentPrefab, attachedBone);
            currentIdleWeapon = newIdleWeapon.GetComponent<WeaponGameObject>();
            UpdateTransformsLayer(currentIdleWeapon.transform);
        }

        UpdateLayersAndWeaponVisuals();

        return currentWeapon;
    }

    public override ShieldGameObject UpdateShield(Shield shield)
    {
        if (currentShield != null)
        {
            Destroy(currentShield.gameObject);
            currentShield = null;
        }

        if (avatar.Cache[ItemTypeEnum.Shield] != null)
        {
            avatar.ClearItems(ItemTypeEnum.Shield);
            avatar.Clean().Compile();
        }

        if (shield == null || shield.IsValid() == false || shield.EquipmentPrefab == null)
        {
            UpdateLayersAndWeaponVisuals();
            return null;
        }

        ShieldSO data = DatabaseManager.Instance.FindItem(shield.itemData.dataGUID) as ShieldSO;

        if (data.runePrefab == null) return null;

        var nhItem = data.runePrefab.GetComponent<NHItem>();

        avatar.SetItem(nhItem)
            .Clean()
            .Compile();

        var clone = Instantiate(shield.EquipmentPrefab, transform);

        currentShield = clone.GetComponent<ShieldGameObject>();
        currentShield.name = data.name;
        UpdateTransformsLayer(currentShield.transform);

        currentShield.transform.localRotation = Quaternion.identity;
        currentShield.transform.localPosition = Vector3.zero;

        UpdateLayersAndWeaponVisuals();

        return currentShield.GetComponent<ShieldGameObject>();
    }

    public override void UpdateMask(Mask mask)
    {
        if (avatar.Cache[ItemTypeEnum.HeadAdditional] != null)
        {
            avatar.ClearItems(ItemTypeEnum.HeadAdditional);
            avatar.Clean().Compile();
        }

        if (mask == null || mask.IsValid() == false)
        {
            UpdateLayersAndWeaponVisuals();
            return;
        }

        var nhItem = mask.EquipmentPrefab.GetComponent<NHItem>();

        avatar.SetItem(nhItem)
            .Clean()
            .Compile();

        UpdateLayersAndWeaponVisuals();
    }

    public override void AssignWeaponToDrawn()
    {
        if(currentIdleWeapon) currentIdleWeapon.gameObject.SetActive(false);
        if(currentWeapon) currentWeapon.gameObject.SetActive(true);
    }

    public override void AssignWeaponToIdle()
    {
        if (currentIdleWeapon) currentIdleWeapon.gameObject.SetActive(true);
        if (currentWeapon) currentWeapon.gameObject.SetActive(false);
    }

    public override void UpdateBag(Bag _bag)
    {
        if (currentBag != null)
        {
            Destroy(currentBag);
            currentBag = null;
        }

        if (_bag == null) return;
        if (_bag.IsValid() == false) return;

        var clone = Instantiate(_bag.EquipmentPrefab, backpackLocation);
        currentBag = clone.gameObject;

        BagSO data = DatabaseManager.Instance.FindItem(_bag.itemData.dataGUID) as BagSO;
        currentBag.name = data.name;

        currentBag.gameObject.layer = gameObject.layer;
        UpdateTransformsLayer(currentBag.transform);
    }

    private void UpdateLayersAndWeaponVisuals()
    {
        UpdateAvatarItemsLayer();
        UpdateEquippedSword();
        UpdateWeaponToggle();
    }

    private void UpdateAvatarItemsLayer()
    {
        for (int i = 0; i < avatar.Items._list.Count; i++)
        {
            if (avatar.Items._list[i].Item != null)
            {
                avatar.Items._list[i].Item.gameObject.layer = gameObject.layer;
            }
        }
    }
    private void UpdateTransformsLayer(Transform transform)
    {
        var children = transform.GetComponentsInChildren<Transform>(true).Select(t => t.gameObject).ToList();
        children.ForEach(c => c.layer = gameObject.layer);
    }

    private void UpdateEquippedSword()
    {
        if (avatar.Cache[ItemTypeEnum.WeaponR] != null)
        {
            currentWeapon = avatar.Cache[ItemTypeEnum.WeaponR].GetComponent< WeaponGameObject>();
            currentWeapon.SetOwner(entity);
            currentWeapon.SetStats(weapon);
        }
        else
        {
            currentWeapon = null;
        }
    }

    public override void StartExtractionEffect()
    {
        SetAllMaterialEmissive(avatar.transform);
        yTranslate.DOLocalMoveY(heightOffset, durationUntilCollapse).SetEase(Ease.OutQuad);
        vfxPivot.DOLocalMoveY(heightOffset, durationUntilCollapse).SetEase(Ease.OutQuad);
        StartCoroutine(TriggerOffMesh());
    }

    private void SetAllMaterialEmissive(Transform transform)
    {
        foreach (Transform child in transform)
        {
            if(child.childCount > 0)
            {
                SetAllMaterialEmissive(child);
            }
            var foundSkinnedMeshMaterial = child.GetComponent<SkinnedMeshRenderer>();
            if(foundSkinnedMeshMaterial != null)
            {
                for (int i = 0; i < foundSkinnedMeshMaterial.materials.Length; i++)
                {
                    foundSkinnedMeshMaterial.materials[i].DOVector(new Vector4(0, 1, 0.5f, maxBrightness), "_EmissionColor", durationUntilCollapse).SetEase(Ease.OutQuad);
                }
            }
        }
        extractionVFXs.SetInt("Rate", rate);
    }

    private IEnumerator TriggerOffMesh()
    {
        extractionLight.enabled = true;
        extractionLight.DOIntensity(1, durationUntilCollapse).From(0);

        yield return new WaitForSeconds(durationUntilCollapse);

        extractionLight.DOIntensity(0, 0.25f).OnComplete(() => extractionLight.enabled = false);

        burstExtractionVFXs.SendEvent("Burst");

        float attractForce = 0;
        DOTween.To(() => attractForce, x => attractForce = x, 100, collapseTime)
            .OnUpdate(() => {
                extractionVFXs.SetFloat("AttractForce", attractForce);
            });
        scalePivot.DOScale(0, collapseTime);
        extractionVFXs.SetInt("Rate", 0);

        yield return new WaitForSeconds(collapseTime);

        extractionVFXs.SetFloat("AttractForce", 0);
    }
}
