﻿using System.Collections;
using UnityEngine;

public class EntityStateSpawn : EntityStates
{
    public EntityStateSpawn(EntityMotor entityMotor) : base(entityMotor)
    {

    }

    public override IEnumerator BeginState()
    {
        _entityAnimator.Spawn();
        yield return new WaitForSeconds(2);
        _entityMotor.SetArmedState();
    }
}
