﻿using System.Collections;
using UnityEngine;

public class EntityStateJump : EntityStates
{
    public EntityStateJump(EntityMotor entityMotor) : base(entityMotor)
    {

    }
    public override IEnumerator BeginState()
    {
        _entityMotor.ResetPrimaryAndSecondaryText();
        _entityAnimator.SetSpeed(1);

        _entityAnimator.ResetVariables();

        _entityMotor.controller.velocity = RoundToNearestAngle() * _entity.entityData.rollModifer;
        _entityMotor.gravity.y += _entity.entityData.hopForce;
        _entityAnimator.SetStrafeJump(Vector3.Dot(_entityMotor.meshRoot.right.normalized, _entityMotor.currentPlayerDirection.normalized));
        _entityAnimator.Jumped();

        yield return new WaitForSeconds(0.25f);

        while(!_entityMotor.isGrounded)
        {
            _entityMotor.controller.velocity = Vector3.Lerp(_entityMotor.controller.velocity, Vector3.zero, Time.deltaTime * 2);
            yield return new WaitForEndOfFrame();
        }

        _entityMotor.controller.velocity = Vector3.zero;

        yield return new WaitForSeconds(_entity.entityData.jumpRecovery);

        _entityMotor.SetArmedState();
    }

    Vector3 RoundToNearestAngle()
    {
        float singedAngle = Vector3.SignedAngle(_entityMotor.currentPlayerDirection.normalized, _entityMotor.meshRoot.forward.normalized, Vector3.up);
        if (Mathf.Abs(singedAngle) > 135f)
            return -_entityMotor.meshRoot.forward.normalized;
        else if (singedAngle < 135f && singedAngle > 45f)
            return -_entityMotor.meshRoot.right.normalized;
        else if (singedAngle > -135f && singedAngle < -45)
            return _entityMotor.meshRoot.right.normalized;
        else
            return _entityMotor.meshRoot.forward.normalized;
    }
}
