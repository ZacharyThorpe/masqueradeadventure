using System.Collections.Generic;
using UnityEngine;

public abstract class EntityVisuals : MonoBehaviour
{
    [SerializeField] protected Transform backpackLocation;

    public Entity entity;
    public WeaponGameObject currentWeapon;
    public ShieldGameObject currentShield;
    protected GameObject currentMask;
    protected GameObject currentBag;
    protected GameObject shieldRune;

    protected bool isCurrentlyArmed = false;

    public virtual void AssignMaterial(List<Material> newMaterials){ }
    public virtual void StartExtractionEffect() { }

    public virtual WeaponGameObject UpdateWeapon(Weapon _weapon) { return null; }
    public virtual ShieldGameObject UpdateShield(Shield shield) { return null; }
    public virtual void UpdateMask(Mask mask) { }

    public virtual void AssignWeaponToIdle() { }

    public virtual void AssignWeaponToDrawn() { }

    public void ToggleWeaponHurtBox(bool _isOn)
    {
        if(currentWeapon != null)
        {
            currentWeapon.ToggleHurtBox(_isOn);
        }
    }

    public void ToggleShieldHitBox(bool _isOn)
    {
        if (currentShield != null)
        {
            currentShield.ToggleShieldHitBox(_isOn);
        }
    }

    public void UpdateWeaponToggle()
    {
        if (isCurrentlyArmed)
        {
            AssignWeaponToDrawn();
        }
        else
        {
            AssignWeaponToIdle();
        }
    }

    public virtual void UpdateBag(Bag _bag)
    {
        if (currentBag != null)
        {
            Destroy(currentBag);
            currentBag = null;
        }

        if (_bag == null) return;
        if(_bag.IsValid() == false) return;

        var clone = Instantiate(_bag.EquipmentPrefab, backpackLocation);
        currentBag = clone.gameObject;

        BagSO data = DatabaseManager.Instance.FindItem(_bag.itemData.dataGUID) as BagSO;
        currentBag.name = data.name;
    }
}
