using System.Collections.Generic;
using UnityEngine;

public class NonAvatarVisuals : EntityVisuals
{
    [SerializeField] protected SkinnedMeshRenderer meshRenderer = default;

    [SerializeField] protected Transform shieldRuneLocation = default;
    [SerializeField] protected Transform weaponIdle = default;
    [SerializeField] protected Transform weaponHeld = default;
    [SerializeField] protected Transform maskLocation = default;

    public override void AssignMaterial(List<Material> newMaterials)
    {
        meshRenderer.SetMaterials(newMaterials);
    }

    public override WeaponGameObject UpdateWeapon(Weapon _weapon)
    {
        if (currentWeapon != null)
        {
            Destroy(currentWeapon.gameObject);
            currentWeapon = null;
        }

        if (_weapon == null || _weapon.IsValid() == false) return null;

        var clone = Instantiate(_weapon.EquipmentPrefab, weaponHeld);
        clone.name = _weapon.itemData.itemType.ToString();
        currentWeapon = clone.GetComponent<WeaponGameObject>();

        return currentWeapon;
    }

    public override ShieldGameObject UpdateShield(Shield shield)
    {
        if (currentShield != null)
        {
            Destroy(currentShield.gameObject);
            shield = null;
        }

        if (shieldRune != null)
        {
            Destroy(shieldRune.transform.gameObject);
            shieldRune = null;
        }

        if (shield == null || shield.IsValid() == false) return null;

        var clone = Instantiate(shield.EquipmentPrefab, transform);

        ShieldSO data = DatabaseManager.Instance.FindItem(shield.itemData.dataGUID) as ShieldSO;
        clone.name = data.name;

        currentShield = clone.GetComponent<ShieldGameObject>();

        shieldRune = Instantiate(data.runePrefab, shieldRuneLocation);
        shieldRune.name = data.name;

        return currentShield;
    }

    public override void UpdateMask(Mask mask)
    {
        if (currentMask != null)
        {
            Destroy(currentMask);
            currentMask = null;
        }

        if (mask == null) return;
        if (mask.prefabMonster == null) return;

        var clone = Instantiate(mask.prefabMonster, maskLocation);
        currentMask = clone.gameObject;

        MaskSO data = DatabaseManager.Instance.FindItem(mask.itemData.dataGUID) as MaskSO;
        currentMask.name = data.name;
    }

    public override void AssignWeaponToIdle()
    {
        if (currentWeapon != null)
        {
            currentWeapon.transform.parent = weaponIdle.transform;
            currentWeapon.transform.localRotation = Quaternion.identity;
            currentWeapon.transform.localPosition = Vector3.zero;
            isCurrentlyArmed = false;
        }
    }

    public override void AssignWeaponToDrawn()
    {
        if (currentWeapon != null)
        {
            currentWeapon.transform.parent = weaponHeld.transform;
            currentWeapon.transform.localRotation = Quaternion.identity;
            currentWeapon.transform.localPosition = Vector3.zero;
            isCurrentlyArmed = true;
        }
    }
}
