﻿using System.Collections;
using UnityEngine;

public class EntityStateRolling : EntityStates
{
    public EntityStateRolling(EntityMotor entityMotor) : base(entityMotor)
    {

    }

    public override IEnumerator BeginState()
    {
        _entityMotor.ResetPrimaryAndSecondaryText();
        _entityAnimator.SetSpeed(1);

        _entityAnimator.ResetVariables();

        _entityAnimator.Roll();
        _entityMotor.isRolling = true;

        float time = _entity.entityData.rollTime;
        
        while(time >= 0)
        {
            time -= Time.deltaTime;

            _entityMotor.controller.velocity = _entityMotor.lastDirection * _entity.entityData.rollModifer;

            DetectingIfHitWall();

            yield return new WaitForEndOfFrame();
        }

        while(!_entityMotor.isGrounded)
        {
            _entityMotor.controller.velocity = Vector3.Lerp(_entityMotor.controller.velocity, Vector3.zero, Time.deltaTime * 2);
            yield return new WaitForEndOfFrame();
        }

        _entityMotor.controller.velocity = Vector3.zero;
        _entityMotor.isRolling = false;
        _entityMotor.SetArmedState();
    }

    public override void Reset()
    {
        base.Reset();
        _entityMotor.isRolling = false;
    }

    void DetectingIfHitWall()
    {
        RaycastHit hit;
        if (Physics.SphereCast(_entityMotor.transform.position, _entity.entityData.rollWallDetectionRadius, _entityMotor.lastDirection, out hit, 1, _entity.entityData.rollingStunLayer))
        {
            if (Vector3.Dot(hit.normal.normalized, _entityMotor.lastDirection.normalized) < -_entity.entityData.rollingStunTolerence)
            {
                _entityMotor.isRolling = false;
                _entityMotor.lastDirection = hit.normal.normalized;
                _entityMotor.SetState(new EntityStateWallStunned(_entityMotor));
            }
        }
    }
}
