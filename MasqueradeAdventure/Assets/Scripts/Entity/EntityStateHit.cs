﻿using System.Collections;
using UnityEngine;

public class EntityStateHit : EntityStates
{
    public HitInfo hitInfo;

    public EntityStateHit(EntityMotor entityMotor) : base(entityMotor)
    {

    }

    public override IEnumerator BeginState()
    {
        _entityMotor.ResetPrimaryAndSecondaryText();
        _entityAnimator.SetSpeed(1);

        _entityAnimator.ResetVariables();

        _entityAnimator.Hit();
        _entityMotor.lastDirection = hitInfo.direction;
        _entityMotor.isStunned = true;
        var time = 0f;

        while (time <= hitInfo.hitReoveryTime)
        {
            _entity.entityVisuals.ToggleWeaponHurtBox(false);
            time += Time.deltaTime;
            if (time <= hitInfo.hitKnockbackTime)
            {
                _entityMotor.controller.velocity = _entityMotor.lastDirection;
            }
            else
            {
                _entityMotor.controller.velocity = Vector3.zero;
            }
            yield return new WaitForEndOfFrame();
        }

        _entityMotor.lastDirection = _entityMotor.meshRoot.forward;
        _entityMotor.isStunned = false;

        _entityMotor.SetArmedState();
    }
}
