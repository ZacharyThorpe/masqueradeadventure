﻿using System.Collections;
using UnityEngine;

public class EntityStateWallStunned : EntityStates
{
    public EntityStateWallStunned(EntityMotor entityMotor) : base(entityMotor)
    {

    }

    public override IEnumerator BeginState()
    {
        _entityMotor.ResetPrimaryAndSecondaryText();
        _entityAnimator.SetSpeed(1);
        _entityAnimator.ResetVariables();
        _entityAnimator.Stun();

        _entityMotor.isStunned = true;
        var time = 0f;
        Vector3 boucePwr = _entityMotor.lastDirection * _entity.entityData.stunKnockbackPower;
        _entityMotor.lastDirection = boucePwr;

        while (time <= _entity.entityData.rollingWallStunTimer)
        {
            time += Time.deltaTime;
            if(time <= _entity.entityData.knockBackTimer)
            {
                _entityMotor.controller.velocity = _entityMotor.lastDirection;
            }
            else
            {
                _entityMotor.controller.velocity = Vector3.zero;
            }
            yield return new WaitForEndOfFrame();
        }

        _entityMotor.lastDirection = _entityMotor.meshRoot.forward;
        _entityMotor.isStunned = false;
        _entityMotor.SetArmedState();
    }

    public override void Reset()
    {
        base.Reset();
        _entityMotor.isStunned = false;
    }
}
