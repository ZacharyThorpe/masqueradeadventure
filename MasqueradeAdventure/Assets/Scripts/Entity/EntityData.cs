﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EntityData", menuName = "Data/Entity Data", order = 1)]
public class EntityData : ScriptableObject
{
    public string GUID;
    public string Name;
    public GameObject prefab;
    public WeaponSO defaultWeapon;
    public ShieldSO defaultShield;
    public List<Material> listOfMaterials = new List<Material>();

    public InteractionType interactsWith;
    public LayerMask rollingStunLayer = 8 >> 1;

    public int totalHealth = 3;

    public int maxDetectionRange = 20;
    public int hearDetectionRange = 2;

    public float movementSpeed = 1f;
    public float roamMovementSpeedModifier = 1f;
    public float strafeSpeedModifier = 0.25f;

    public float crawlModifer = 0.25f;
    public float rollModifer = 1.25f;
    public float rollTime = 1;
    public float gravity = 10f;
    public float rotationSpeed = 500f;
    public float rollingStunTolerence = 0.85f;

    public float hopForce = 10;
    public float hopModifier = 1.25f;

    public float rollingWallStunTimer = 1;
    public float stunKnockbackPower = 1;
    public float knockBackTimer = 0.99f;

    public float crawlingTransition = 0.5f;
    public float jumpRecovery = 0.15f;

    public float rollWallDetectionRadius = 0.25f;
    public float attackWallDetectionRadius = 0.25f;

    public float graceTime = 0.15f;

    public float idleTime = 4f;
    public float roamAroundTime = 1f;
    public float runTimeToWall = 0.15f;
    public float strafeTimer = 0.25f;

    public bool faceLockedTarget = true;
    public bool autoHops = false;

    public float leaseDistance = 10;

    public LootTableSO lootTableSO = default;

    public Item GetRandomLoot()
    {
        return lootTableSO.lootTable.GetRandomLoot();
    }
}
