using System;

public interface IEquipmentHandler
{
    public void SetEntity(Entity entity);
    public void UpdateWeapon(Weapon weapon);
    public void ToggleHurtBox(bool isOn);
    public void AssignWeaponToIdle();
    public IWeapon GetEquippedShield();
    public void AssignWeaponToDrawn();
    public ShieldGameObject AssignShield(Shield shield);
    public void AssignBackpack(Bag _bag);
    public void AssignMask(Mask mask);
    public bool HasAWeapon();
    public void GlowTheMeshes();
}
