﻿using System.Collections;
using UnityEngine;

public abstract class EntityStates 
{
    protected readonly EntityMotor _entityMotor;
    protected readonly Entity _entity;
    protected readonly EntityAnimatorManager _entityAnimator;
    protected readonly EntityEquipment _entityEquipment;
    protected readonly EntityVisuals _entityVisuals;
    public bool isLocked = false;

    public EntityStates(EntityMotor entityMotor)
    {
        _entityMotor = entityMotor;
        _entity = _entityMotor.entity;
        _entityAnimator = _entity.entityAnimator;
        _entityEquipment = _entity.equipment;
        _entityVisuals = _entity.entityVisuals;
    }

    public virtual IEnumerator BeginState()
    {
        yield return null;
    }
    public virtual void Reset() 
    {
        if(!_entityMotor.controller.isKinematic)
        {
            _entityMotor.controller.velocity = Vector3.zero;
        }
    }
    public virtual void Move() { }
    public virtual void Interact() { }
    public virtual void Idle() { }
    public virtual void Attack() { }
    public virtual void SecondaryAction() { }
    public virtual void Loot() { }
    public virtual void Hit(HitInfo hitInfo)
    {
        Reset();
        _entity.weaponHitboxAllowed = false;
        var state = new EntityStateHit(_entityMotor)
        {
            hitInfo = hitInfo
        };
        _entityMotor.SetState(state);
    }
    public virtual void Died() 
    {
        Reset();
        _entityMotor.SetState(new EntityStateDeath(_entityMotor));
    }
    public virtual void LeaveSecondary() { }
    public virtual bool LootClose()
    {
        return _entityMotor.lootChecker.currentLootable != null;
    }
    public virtual void WeaponUpdated(Weapon weapon) { }
}
