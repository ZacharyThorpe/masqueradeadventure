using System.Collections.Generic;
using UnityEngine;

public interface IAnimationHandler
{
    public float StrafeAxis { get; set; }
    public float YAxis { get; set; }
    public bool isGrounded { get; set; }
    public bool isCrawling { get; set; }

    public void ResetVariables();
    public void AssignMaterial(List<Material> newMaterials);
    public void Jumped();
    public void Roll();
    public void Attack();
    public void AttackLarge();
    public void AttackJump();
    public void Stun();
    public void Deflected();
    public void Hit();
    public void Death();
    public void Crawl();
    public void Stand();
    public void Loot();
    public void DoneLooting();
    public void SetSpeed(float _speed);
    public void Blocking();
    public void Idle();
    public void OneHander();
    public void Unarmed();
    public void UseConsume();
    public void FinishConsume();
    public void TriggerDifferentIdle();
    public void SetStrafeJump(float strafeJump);
    public void Spawn();
}
