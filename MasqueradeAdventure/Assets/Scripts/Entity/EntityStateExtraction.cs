using System.Collections;
using UnityEngine;

public class EntityStateExtraction : EntityStates
{
    public EntityStateExtraction(EntityMotor entityMotor) : base(entityMotor)
    {

    }

    public override IEnumerator BeginState()
    {
        _entityMotor.isExtracting = true;
        _entity.entityAnimator.Extract();
        _entity.entityVisuals.StartExtractionEffect();
        yield return new WaitForSeconds(9);
        GlobalEventsManager.OnExtracted?.Invoke(_entity);
    }
}
