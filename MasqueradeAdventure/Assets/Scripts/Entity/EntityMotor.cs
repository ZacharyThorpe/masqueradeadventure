﻿using System;
using UnityEngine;

public class EntityMotor : MonoBehaviour
{
    public Entity entity;

    public Rigidbody controller;
    public CapsuleCollider capCollider;
    public InputHandler inputHandler;
    public GroundLootChecker lootChecker;
    public Transform meshRoot;
    public Transform groundHitNormal;

    [SerializeField] private bool hasSpawnState = false;
    [SerializeField] private LayerMask groundMask = default;
    [SerializeField] private EntityRaycaster raycaster = default;

    [SerializeField] private UISurpriseIcon surpriseIcon = default;

    public Vector3 currentPlayerDirection;
    public Vector3 lastDirection;
    public Vector3 gravity = Vector3.zero;
    public Vector3 targetFacingDirection = Vector3.zero;

    public bool isGrounded = false;
    public bool isArmed = false;
    public bool isCrawling = false;
    public bool isStunned = false;
    public bool isRolling = false;
    public bool isAttacking = false;
    public bool isInteracting = false;
    public bool isExtracting = false;
    public bool isDieing = false;
    public bool isSurprised = false;
    public bool isfacingLocked = false;

    private EntityStates _currentState;

    void Start()
    {
        if(inputHandler != null)
        {
            inputHandler.AButtonOn += Interact;
            inputHandler.BButtonOn += Attack;

            inputHandler.XButtonOn += UseConsumable;

            inputHandler.RTriggerOn += SecondaryAction;
            inputHandler.RTriggerOff += ReleaseSecondaryAction;

            inputHandler.LTriggerOn += AttemptToLockOn;
            inputHandler.LTriggerOff += ReleaseTarget;

            entity.lockOn.lostTarget += ReleaseTarget;

            inputHandler.Forward += (value) => currentPlayerDirection = value;
        }

        lastDirection = transform.forward;

        if(hasSpawnState == true)
        {
            SetState(new EntityStateSpawn(this));
        }
        else
        {
            SetState(new EntityStateUnarmed(this));
        }
    }

    void OnDestroy()
    {
        if(inputHandler != null)
        {
            inputHandler.AButtonOn -= Interact;
            inputHandler.BButtonOn -= Attack;

            inputHandler.RTriggerOn -= SecondaryAction;
            inputHandler.RTriggerOff -= ReleaseSecondaryAction;

            inputHandler.LTriggerOn -= AttemptToLockOn;
            inputHandler.LTriggerOff -= ReleaseTarget;

            entity.lockOn.lostTarget -= ReleaseTarget;

            inputHandler.Forward -= (value) => currentPlayerDirection = value;
        }
    }

    void FixedUpdate()
    {
        if (entity.isActive == false) return;

        CheckIfGrounded();
        CheckIfBelowDeathLine();

        entity.entityAnimator.isGrounded = isGrounded;
        entity.entityAnimator.isCrawling = isCrawling;

        if (isDieing || entity.entityMotor.isExtracting == true) return;

        if (inputHandler != null && inputHandler.MoveInput.magnitude > 0.1f && !isStunned && !isExtracting)
        {
            _currentState.Move();
        }
        else
        {
            _currentState.Idle();
            entity.entityAnimator.ResetVariables();
        }

        ApplyGravity();

        if(controller.isKinematic != true)
        {
            controller.velocity = new Vector3(controller.velocity.x, gravity.y, controller.velocity.z);
        }

        if(surpriseIcon != null) surpriseIcon.ToggleSurprise(isSurprised);
    }

    private void CheckIfBelowDeathLine()
    {
        if(transform.position.y < -100 && isDieing != true)
        {
            Died();
        }
    }

    public void SetEntity(Entity _entity)
    {
        entity = _entity;
    }

    public void SetState(EntityStates entityState)
    {
        StopAllCoroutines();
        entity.isBlocking = false;

        if (_currentState != null)
        {
            entityState.isLocked = _currentState.isLocked;
        }

        _currentState = entityState;

        StartCoroutine(_currentState.BeginState());
    }

    public void SetArmedState()
    {
        if (isArmed)
        {
            SetState(new EntityStateArmed(this));
        }
        else
        {
            SetState(new EntityStateUnarmed(this));
        }
    }

    private void ReleaseSecondaryAction()
    {
        _currentState.LeaveSecondary();
    }

    private void SecondaryAction()
    {
        _currentState.SecondaryAction();
    }

    private void Interact()
    {
        if (lootChecker.currentLootable != null)
        {
            _currentState.Loot();
        }
        else
        {
            _currentState.Interact();
        }
    }
    private void Attack()
    {
        _currentState.Attack();
    }

    private void UseConsumable()
    {
        if(entity.equipment.EntityInventory.consumable != null && entity.equipment.EntityInventory.consumable.item != null && entity.equipment.EntityInventory.consumable.item.IsValid())
        {
            var newState = new EntityStateUsingConsumable(this)
            {
                currentConsumable = entity.equipment.EntityInventory.consumable,
            };
            SetState(newState);
        }
    }

    private void AttemptToLockOn()
    {
        _currentState.isLocked = true;
    }

    private void ReleaseTarget()
    {
        _currentState.isLocked = false;
    }

    public void GotHit(HitInfo _hitInfo)
    {
        _currentState.Hit(_hitInfo);
    }

    public void Died()
    {
        _currentState.Died();
    }

    public Transform GetGroundNormal()
    {
        var distanceToTheGround = GetComponent<Collider>().bounds.extents.y + 0.1f;
        if (Physics.SphereCast(transform.position, 0.5f, -Vector3.up, out RaycastHit hit, distanceToTheGround))
        {
            groundHitNormal.up = hit.normal;
            return groundHitNormal;
        }
        groundHitNormal.up = Vector3.up;
        return groundHitNormal;
    }

    void CheckIfGrounded()
    {
        var distanceToTheGround = Remap.RemapFloat(GetComponent<Collider>().bounds.extents.y, 0.5f, 0.1f, 1f, 1.2f) / 2f;
        isGrounded = Physics.SphereCast(transform.position, 0.45f, -Vector3.up, out RaycastHit hitNew, distanceToTheGround, groundMask);
    }

    void ApplyGravity()
    {
        gravity.y += entity.entityData.gravity * Time.deltaTime;

        float dotProduct = 1;

        if (isGrounded)
        {
            GetGroundNormal();
            dotProduct = Vector3.Dot(groundHitNormal.up.normalized, lastDirection);
        }

        if(dotProduct <= 0)
        {
            if (isGrounded && gravity.y < 0)
                gravity.y = 0;
            dotProduct = 0;
        }

        gravity.y = Mathf.Clamp(gravity.y, -20 * dotProduct, float.MaxValue);
    }

    public bool HopCheck(float timeSpentRunning)
    {
        if (entity.entityData.autoHops == false) return false;

        if(timeSpentRunning >= entity.entityData.runTimeToWall)
        {
            if(CheckIfAboveHopableHeight() && !raycaster.CheckIfWallTooHigh())
            {
                SetState(new EntityStateHopUp(this));
                return true;
            }
        }
        return false;
    }

    public bool DirectionIsNotFoward()
    {
        if (Vector3.Dot(currentPlayerDirection.normalized, meshRoot.forward.normalized) < 0.9f)
            return true;
        else
            return false;
    }

    public bool CheckIfAboveHopableHeight()
    {
        return raycaster.CheckIfCanHop();
    }

    public void SetPrimaryText(string text)
    {
        entity.PrimaryActionText?.Invoke(text);
    }

    public void SetSecondaryText(string _secondaryText)
    {
        entity.SecondaryActionText?.Invoke(_secondaryText);
    }

    public void ResetPrimaryAndSecondaryText()
    {
        entity.PrimaryActionText?.Invoke(" ");
        entity.SecondaryActionText?.Invoke(" ");
    }

    public void WeaponUpdated(Weapon weapon)
    {
        _currentState.WeaponUpdated(weapon);
    }
}
