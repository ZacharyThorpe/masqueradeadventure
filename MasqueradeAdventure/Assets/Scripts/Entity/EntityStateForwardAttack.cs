﻿using System.Collections;
using UnityEngine;

public class EntityStateForwardAttack : EntityStates
{
    public EntityStateForwardAttack(EntityMotor entityMotor) : base(entityMotor)
    {

    }

    public override IEnumerator BeginState()
    {
        _entityMotor.ResetPrimaryAndSecondaryText();
        _entityAnimator.SetSpeed(1);

        _entityAnimator.ResetVariables();

        _entityMotor.isAttacking = true;
        _entityMotor.lastDirection = _entityMotor.meshRoot.forward;
        _entityMotor.gravity.y += _entity.equipment.EntityInventory.mainHand.AttackJumpForce;

        float jumpDistanceForce = 1;

        float timeBetweenAttacks = 1 / _entity.equipment.EntityInventory.mainHand.AttackJumpAttackSpeed;

        if (_entity.lockOn.CheckIfLockOnInteractionIsValid())
        {
            var distance = Vector3.Distance(_entity.lockOn.GetSeekedPosition(), _entity.transform.position) - 1;
            distance = distance > 0 ? distance : 0;
            var lerpValue = distance / _entity.entityData.rollModifer;
            jumpDistanceForce = Mathf.Lerp(0, _entity.entityData.rollModifer, lerpValue);
        }
        else
        {
            jumpDistanceForce *= _entity.entityData.rollModifer;     
        }

        float time = 0;

        _entityAnimator.AttackJump();
        _entityEquipment.WeaponMultiplier();

        while (time <= timeBetweenAttacks || !_entityMotor.isGrounded)
        {
            time += Time.deltaTime;
            Vector3 direction = _entityMotor.lastDirection * jumpDistanceForce;
            _entityMotor.controller.velocity = new Vector3(direction.x, _entityMotor.controller.velocity.y, direction.z);
            yield return new WaitForEndOfFrame();
        }
        _entityMotor.controller.velocity = Vector3.zero;

        yield return new WaitForSeconds(_entity.equipment.EntityInventory.mainHand.AttackJumpRecovery);

        _entityMotor.SetArmedState();
    }

    public override void Reset()
    {
        base.Reset();
        _entityMotor.controller.velocity = Vector3.zero;
        _entity.entityVisuals.ToggleWeaponHurtBox(false);
    }
}