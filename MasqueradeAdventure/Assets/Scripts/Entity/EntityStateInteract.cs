﻿using System.Collections;
using UnityEngine;
using static UnityEngine.EventSystems.EventTrigger;

public class EntityStateInteract : EntityStates
{
    public float interactionTimeInSeconds = 1;

    public EntityStateInteract(EntityMotor entityMotor) : base(entityMotor)
    {

    }

    public override void Move()
    {
        Reset();
        _entityMotor.SetState(new EntityStateUnarmed(_entityMotor));
    }

    public override IEnumerator BeginState()
    {
        _entityAnimator.ResetVariables();

        _entityMotor.isInteracting = true;
        _entityAnimator.Loot();

        float timer = 0;

        while(timer < interactionTimeInSeconds)
        {
            timer += Time.deltaTime;
            _entity.UpdateInteractionTimer(timer / interactionTimeInSeconds);
            yield return new WaitForEndOfFrame();
        }

        _entity.UpdateInteractionTimer(0);

        _entity.seeker.currentInteracble.Action(_entity);
        if(_entity.entityMotor.isExtracting == false)
        {
            _entityAnimator.DoneLooting();
            _entityMotor.isInteracting = false;
            _entityMotor.SetState(new EntityStateUnarmed(_entityMotor));
        }
    }

    public override void Reset()
    {
        base.Reset();
        _entityAnimator.DoneLooting();
        _entityMotor.isInteracting = false;
        _entity.UpdateInteractionTimer(0);
    }

}
