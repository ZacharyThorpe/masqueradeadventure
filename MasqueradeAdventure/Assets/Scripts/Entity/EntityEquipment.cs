using UnityEngine;

public class EntityEquipment : MonoBehaviour
{
    public EntityInventorySO EntityInventory;

    [SerializeField] private Entity entity;
    [SerializeField] private EntityVisuals m_entityVisuals;

    public void Instantiate()
    {
        EntityInventory.OnWeaponEquipped += UpdateWeapon;
        EntityInventory.OnShieldEquipped += UpdateShield;
        EntityInventory.OnMaskEquipped += UpdateMask;
        EntityInventory.OnBagEquipped += UpdateBag;

        UpdateWeapon(EntityInventory.mainHand);
        UpdateShield(EntityInventory.shield);
        UpdateMask(EntityInventory.mask);
        UpdateBag(EntityInventory.bag);
    }

    private void OnDestroy()
    {
        EntityInventory.OnWeaponEquipped -= UpdateWeapon;
        EntityInventory.OnShieldEquipped -= UpdateShield;
        EntityInventory.OnMaskEquipped -= UpdateMask;
        EntityInventory.OnBagEquipped -= UpdateBag;
    }

    public ShieldGameObject GetCurrentShield()
    {
        return m_entityVisuals.currentShield;
    }

    public void ConsumeItem(int amount)
    {
        if (!EntityInventory.CheckIfConsumableSlot01IsValid()) return;
        EntityInventory.consumable.amount += amount;
        if (EntityInventory.consumable.amount <= 0)
        {
            EntityInventory.consumable = null;
        }
    }

    public void WeaponMultiplier()
    {
        if (m_entityVisuals.currentWeapon != null)
        {
            m_entityVisuals.currentWeapon.MultiplyAttack();
        }
    }

    public void WeaponMultiplierClear()
    {
        if (m_entityVisuals.currentWeapon != null)
        {
            m_entityVisuals.currentWeapon.ClearMultiplier();
        }
    }

    private void UpdateWeapon(Weapon weapon)
    {
        var currentWeapon = m_entityVisuals.UpdateWeapon(weapon);

        if (currentWeapon != null)
        {
            currentWeapon.SetOwner(entity);
            currentWeapon.SetStats(weapon);
        }
        else if(entity.entityMotor.isArmed)
        {
            entity.entityMotor.SetArmedState();
        }
        GlobalEventsManager.OnWeaponUpdated?.Invoke(entity, weapon);
    }

    private void UpdateShield(Shield shield)
    {
        var currentShield = m_entityVisuals.UpdateShield(shield);

        if (currentShield != null)
        {
            currentShield.SetOwner(entity);
            currentShield.SetStats(shield);
            currentShield.StartShieldRegain();
        }
    }

    private void UpdateMask(Mask mask)
    {
        m_entityVisuals.UpdateMask(mask);
    }

    private void UpdateBag(Bag bag)
    {
        m_entityVisuals.UpdateBag(bag);
    }
}
