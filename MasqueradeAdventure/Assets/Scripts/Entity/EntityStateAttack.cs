﻿using System.Collections;
using UnityEngine;

public class EntityStateAttack : EntityStates
{
    [SerializeField] private bool attackQued = false;
    [SerializeField] private int attackNum = 1;

    public EntityStateAttack(EntityMotor entityMotor) : base(entityMotor)
    {
        attackNum = 1;
    }

    public override IEnumerator BeginState()
    {
        _entityMotor.ResetPrimaryAndSecondaryText();
        _entityAnimator.SetSpeed(1);

        _entityMotor.isAttacking = true;

        var mainHand = _entity.equipment.EntityInventory.mainHand;

        float time = 0;
        float timeBetweenAttacks = 1 / mainHand.AttackPerSecond;
        float timeBetweenGreaterAttacks = 1 / mainHand.GreaterAttackSpeed;

        attackQued = false;

        Vector3 lungPower = _entityMotor.meshRoot.forward * _entity.equipment.EntityInventory.mainHand.AttackLung;
        UpdateMesh();

        _entityAnimator.SetSpeed(mainHand.AttackPerSecond);
        _entityAnimator.Attack();
        _entityEquipment.WeaponMultiplierClear();

        while (time <= timeBetweenAttacks)
        {
            time += Time.deltaTime;
            _entityMotor.lastDirection = lungPower * Mathf.Pow(1 - mainHand.AttackLungDecay, time * 2);
            _entityMotor.controller.velocity = _entityMotor.lastDirection;
            DetectingIfHitWall();

            yield return new WaitForEndOfFrame();

            if (time >= timeBetweenAttacks && attackQued && attackNum < 3)
            {
                attackNum++;
                attackQued = false;
                time = 0;
                UpdateMesh();
                _entityAnimator.SetSpeed(mainHand.AttackPerSecond);
                _entityAnimator.Attack();
            }
        }

        _entityMotor.controller.velocity = Vector3.zero;

        if (attackQued)
        {
            time = 0;
            attackQued = false;
            attackNum++;
            UpdateMesh();

            _entityAnimator.SetSpeed(mainHand.GreaterAttackSpeed);
            _entityAnimator.AttackLarge();
            _entityEquipment.WeaponMultiplier();

            while (time <= timeBetweenGreaterAttacks)
            {
                time += Time.deltaTime;
                _entityMotor.lastDirection = lungPower * Mathf.Pow(1 - mainHand.AttackLungDecay, time * 2);
                _entityMotor.controller.velocity = _entityMotor.lastDirection;
                DetectingIfHitWall();

                yield return new WaitForEndOfFrame();
            }

            _entityMotor.controller.velocity = Vector3.zero;
            attackNum = 0;
        }

        _entity.entityVisuals.ToggleWeaponHurtBox(false);
        _entityMotor.isAttacking = false;
        _entityMotor.lastDirection = _entityMotor.meshRoot.forward;
        _entityMotor.SetArmedState();
    }

    public override void Attack()
    {
        attackQued = true;
    }

    public override void Reset()
    {
        base.Reset();
        _entity.entityVisuals.ToggleWeaponHurtBox(false);
        _entityMotor.isAttacking = false;
    }

    public override void WeaponUpdated(Weapon weapon)
    {
        if (weapon == null)
        {
            _entityMotor.SetState(new EntityStateUnarmed(_entityMotor));
        }
    }

    void DetectingIfHitWall()
    {
        if (Physics.SphereCast(_entityMotor.transform.position, _entity.entityData.attackWallDetectionRadius, _entityMotor.lastDirection, out RaycastHit hit, 1, _entity.entityData.rollingStunLayer))
        {
            if (Vector3.Dot(hit.normal.normalized, _entityMotor.lastDirection.normalized) < -_entity.entityData.rollingStunTolerence)
            {
                _entityMotor.isRolling = false;
                _entityMotor.lastDirection = hit.normal.normalized;

                var currentWeapon = _entity.equipment.EntityInventory.mainHand;

                var state = new EntityStateAttackDeflected(_entityMotor)
                {
                    hitInfo = new HitInfo()
                    {
                        direction = hit.normal.normalized * currentWeapon.HitKnockbackBasePower,
                        hitReoveryTime = currentWeapon.HitReoveryTime,
                        hitKnockbackTime = currentWeapon.HitKnockbackTime,
                        damage = 0,
                    },
                };
                Reset();
                _entityMotor.SetState(state);
            }
        }
    }

    private void UpdateMesh()
    {
        if (isLocked)
        {
            if (_entity.lockOn.CheckIfLockOnInteractionIsValid())
            {
                Vector3 targetPos = _entity.lockOn.GetSeekedPosition();
                targetPos.y = _entityMotor.meshRoot.position.y;
                _entityMotor.meshRoot.rotation = Quaternion.LookRotation(targetPos - _entityMotor.meshRoot.position);
            }
        }
    }
}
