using UnityEngine;

public class EntityEquipmentMenu : MonoBehaviour
{
    public EntityInventorySO EntityInventorySO;

    public AvatarEntityVisuals visuals;

    private EntityInventory m_entityInventory;

    private void Awake()
    {
        if(EntityInventorySO != null)
        {
            EntityInventorySO.OnWeaponEquipped += UpdateWeapon;
            EntityInventorySO.OnShieldEquipped += UpdateShield;
            EntityInventorySO.OnMaskEquipped += UpdateMask;
            EntityInventorySO.OnBagEquipped += UpdateBag;

            visuals.UpdateWeapon(EntityInventorySO.mainHand);
            visuals.UpdateShield(EntityInventorySO.shield);
            visuals.UpdateMask(EntityInventorySO.mask);
            visuals.UpdateBag(EntityInventorySO.bag);
        }
    }

    private void OnDestroy()
    {
        if (EntityInventorySO != null)
        {
            EntityInventorySO.OnWeaponEquipped -= UpdateWeapon;
            EntityInventorySO.OnShieldEquipped -= UpdateShield;
            EntityInventorySO.OnMaskEquipped -= UpdateMask;
            EntityInventorySO.OnBagEquipped -= UpdateBag;
        }

        if (m_entityInventory != null)
        {
            m_entityInventory.OnWeaponEquipped -= UpdateWeapon;
            m_entityInventory.OnShieldEquipped -= UpdateShield;
            m_entityInventory.OnMaskEquipped -= UpdateMask;
            m_entityInventory.OnBagEquipped -= UpdateBag;
        }
    }

    public void SetEntityInventory(EntityInventory entityInventory)
    {
        m_entityInventory = entityInventory;
        if (entityInventory != null)
        {
            m_entityInventory.OnWeaponEquipped += UpdateWeapon;
            m_entityInventory.OnShieldEquipped += UpdateShield;
            m_entityInventory.OnMaskEquipped += UpdateMask;
            m_entityInventory.OnBagEquipped += UpdateBag;

            visuals.UpdateWeapon(m_entityInventory.mainHand);
            visuals.UpdateShield(m_entityInventory.shield);
            visuals.UpdateMask(m_entityInventory.mask);
            visuals.UpdateBag(m_entityInventory.bag);
        }
    }

    public void UpdateWeapon(Weapon weapon)
    {
        visuals.UpdateWeapon(weapon);
    }

    public void UpdateShield(Shield shield)
    {
        visuals.UpdateShield(shield);
    }

    public void UpdateMask(Mask mask)
    {
        visuals.UpdateMask(mask);
    }

    public void UpdateBag(Bag bag)
    {
        visuals.UpdateBag(bag);
    }
}
