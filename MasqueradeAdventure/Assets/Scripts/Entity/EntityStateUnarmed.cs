﻿using System.Collections;
using UnityEngine;

public class EntityStateUnarmed : EntityStates
{
    [SerializeField] private float speed = 0;

    [SerializeField] private float timeSpentRunning = 0;

    [SerializeField] private float TimeUntilRandomIdle = 3;

    private Vector3 lastPosition;

    public EntityStateUnarmed(EntityMotor entityMotor) : base(entityMotor)
    {

    }

    public override IEnumerator BeginState()
    {
        _entityMotor.ResetPrimaryAndSecondaryText();
        _entityAnimator.SetSpeed(1);
        _entityAnimator.Unarmed();
        _entityMotor.capCollider.height = 2;
        _entityMotor.isArmed = false;
        RandomizedTimeUntilNewIdle();
        yield break;
    }

    public override void Move()
    {
        if (_entityMotor.isGrounded)
        {
            CalculateDirectionAndSpeed();
            UpdateMesh();

            bool swappingStates = _entityMotor.HopCheck(timeSpentRunning);
            if (swappingStates) return;
        }
        else
        {
            speed = Mathf.Lerp(speed, 0, Time.deltaTime);
        }

        if (LootClose() == true)
        {
            _entityMotor.SetPrimaryText("Loot");
        }
        else
        {
            if (isLocked && Vector3.Dot(_entityMotor.currentPlayerDirection.normalized, _entityMotor.meshRoot.forward.normalized) > 0.9f)
            {
                _entityMotor.SetPrimaryText("Roll");
            }
            else
            {
                _entityMotor.SetPrimaryText("Jump");
            }
        }

        _entityMotor.SetSecondaryText(" ");

        _entityMotor.controller.velocity = _entityMotor.lastDirection * speed;

        UpdateRunningInPlaceTimer();
    }

    public override void Idle()
    {
        timeSpentRunning = 0;
        _entityMotor.controller.velocity = Vector3.zero;

        if(LootClose() == true)
        {
            _entityMotor.SetPrimaryText("Loot");
        }
        else
        {
            _entityMotor.SetPrimaryText(" ");
        }
        _entityMotor.SetSecondaryText("Crouch");

        TimeUntilRandomIdle -= Time.deltaTime;
        if (TimeUntilRandomIdle <= 0)
        {
            _entity.entityAnimator.TriggerDifferentIdle();
            RandomizedTimeUntilNewIdle();
        }
    }

    void CalculateDirectionAndSpeed()
    {
        var cameraDirection = _entityMotor.currentPlayerDirection;

        if (cameraDirection != Vector3.zero)
        {
            if (!isLocked)
            {
                float dotValue = Vector3.Dot(_entityMotor.lastDirection, cameraDirection);

                if (dotValue <= -0.75f)
                    _entityMotor.lastDirection = cameraDirection;
                else
                    _entityMotor.lastDirection = Vector3.RotateTowards(_entityMotor.lastDirection, cameraDirection, _entity.entityData.rotationSpeed * Mathf.Deg2Rad * Time.deltaTime, 0).normalized;
            }
            else
            {
                _entityMotor.lastDirection = cameraDirection;
            }
        }

        var cameraDirectionMagnitude = Mathf.Min(cameraDirection.magnitude, 1.0f);
        speed = GetSpeed(cameraDirectionMagnitude);
    }

    private float GetSpeed(float cameraDirectionMagnitude)
    {
        if(!_entityMotor.isGrounded)
        {
            return _entity.entityData.rollModifer;
        }
        else
        {
            var speed = _entity.GetMovementSpeed();
            speed *= _entityMotor.isCrawling ? 0.25f : 1;
            return cameraDirectionMagnitude * speed;
        }
    }

    private void RandomizedTimeUntilNewIdle()
    {
        TimeUntilRandomIdle = Random.Range(8, 14f);
    }

    public override void Interact()
    {
        if(_entityMotor.isGrounded)
        {
            if (InteractableIsNear())
            {
                EntityStateInteract interactionState = new EntityStateInteract(_entityMotor)
                {
                    interactionTimeInSeconds = _entity.seeker.currentInteracble.GetInteractionData().interactionInSeconds
                };
                _entityMotor.SetState(interactionState);
                return;
            }

            if (isLocked)
            {
                if (_entityMotor.DirectionIsNotFoward() && _entityMotor.inputHandler.MoveInput.magnitude > 0.1f)
                {
                    _entityMotor.SetState(new EntityStateJump(_entityMotor));
                }
                else
                {
                    _entityMotor.lastDirection = _entityMotor.meshRoot.forward;
                    _entityMotor.SetState(new EntityStateRolling(_entityMotor));
                }
            }
            else
            {
                if (_entityMotor.inputHandler.MoveInput.magnitude > 0.1f)
                {
                    _entityMotor.SetState(new EntityStateJump(_entityMotor));
                }
            }
        }
    }

    public override void Loot()
    {
        if (_entityMotor.lootChecker.currentLootable != null)
        {
            _entityMotor.lootChecker.currentLootable.LootItem();
        }
    }

    public override void Attack()
    {
        if (_entityEquipment.EntityInventory.mainHand == null || _entityEquipment.EntityInventory.mainHand.IsValid() == false) return;
        _entityMotor.SetState(new EntityStateArmed(_entityMotor));
        _entityAnimator.TriggerSheath();
    }

    public override void SecondaryAction()
    {
        _entityMotor.isCrawling = true;
    }

    public override void LeaveSecondary()
    {
        _entityMotor.isCrawling = false;
    }

    private void UpdateMesh()
    {
        if(isLocked && _entity.entityData.faceLockedTarget)
        {
            if (_entity.lockOn.CheckIfLockOnInteractionIsValid())
            {
                Vector3 targetPos = _entity.lockOn.GetSeekedPosition();
                targetPos.y = _entityMotor.meshRoot.position.y;
                _entityMotor.meshRoot.rotation = Quaternion.LookRotation(targetPos - _entityMotor.meshRoot.position);
            }

            _entityAnimator.StrafeAxis = Vector3.Dot(_entityMotor.meshRoot.right.normalized, _entityMotor.currentPlayerDirection.normalized);
            _entityAnimator.YAxis = Vector3.Dot(_entityMotor.meshRoot.forward.normalized, _entityMotor.currentPlayerDirection.normalized);
        }
        else
        {
            _entityMotor.meshRoot.rotation = Quaternion.LookRotation(_entityMotor.lastDirection);
            _entityAnimator.YAxis = _entityMotor.lastDirection.normalized.magnitude;
            _entityAnimator.StrafeAxis = 0;
        }
    }
    private void UpdateRunningInPlaceTimer()
    {
        if (Vector3.Distance(lastPosition, _entityMotor.transform.position) < 0.1f)
        {
            timeSpentRunning += Time.deltaTime;
        }
        else
        {
            timeSpentRunning = 0;
        }
        lastPosition = _entityMotor.transform.position;
    }
    private bool InteractableIsNear()
    {
        if (_entity.seeker.currentInteracble != null && _entityMotor.inputHandler.MoveInput.magnitude < 0.1f)
        {
            if (_entity.seeker.currentInteracble.GetInteractionData().canBeInteractedWith && !_entity.seeker.currentInteracble.HasBeenInteractedWith() && _entity.seeker.currentInteracble.IsActive())
            {
                return true;
            }
        }
        return false;
    }
    public override bool LootClose()
    {
        if (_entityMotor.lootChecker.currentLootable != null)
        {
            return true;
        }
        else if(InteractableIsNear())
        {
            return true;
        }
        return false;
    }
}
