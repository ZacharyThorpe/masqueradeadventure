﻿using System.Collections;
using UnityEngine;

public class EntityStateArmed : EntityStates
{
    [SerializeField] private float speed = 0;

    float timeSpentRunning = 0;
    Vector3 lastPosition;

    public EntityStateArmed(EntityMotor entityMotor) : base(entityMotor)
    {

    }

    public override IEnumerator BeginState()
    {
        _entityMotor.ResetPrimaryAndSecondaryText();
        _entityAnimator.SetSpeed(1);
        _entityAnimator.OneHander();
        _entityMotor.isArmed = true;
        yield break;
    }
    public override void Move()
    {
        if (_entityMotor.isGrounded)
        {
            CalulateDirectionAndSpeed();
            UpdateMesh();

            bool swappingStates = _entityMotor.HopCheck(timeSpentRunning);
            if (swappingStates) return;
        }
        else
        {
            speed = Mathf.Lerp(speed, 0, Time.deltaTime);
        }

        if (LootClose() == true)
        {
            _entityMotor.SetPrimaryText("Loot");
        }
        else
        {
            if (!isLocked && Vector3.Dot(_entityMotor.currentPlayerDirection.normalized, _entityMotor.meshRoot.forward.normalized) > 0.9f)
            {
                _entityMotor.SetPrimaryText("Roll");
            }
            else
            {
                _entityMotor.SetPrimaryText("Jump");
            }
        }
        if (_entityEquipment.EntityInventory.shield.IsValid() == true)
        {
            _entityMotor.SetSecondaryText("Defend");
        }
        else
        {
            _entityMotor.SetSecondaryText("");
        }

        _entityMotor.controller.velocity = _entityMotor.lastDirection * speed;

        UpdateRunningInPlaceTimer();
    }
    public override void Idle()
    {
        timeSpentRunning = 0;
        if (LootClose() == true)
        {
            _entityMotor.SetPrimaryText("Loot");
        }
        else
        {
            if(isLocked)
            {
                _entityMotor.SetPrimaryText("Jump");
            }
            else
            {
                _entityMotor.SetPrimaryText("Put Away");
            }
        }
        if (_entityEquipment.EntityInventory.shield.IsValid() == true)
        {
            _entityMotor.SetSecondaryText("Defend");
        }
        else
        {
            _entityMotor.SetSecondaryText("");
        }
        _entityMotor.controller.velocity = Vector3.zero;
    }

    public override void SecondaryAction()
    {
        if(_entityEquipment.EntityInventory.shield.IsValid() == true)
        {
            _entityMotor.SetState(new EntityStateBlock(_entityMotor));
        }
    }

    void CalulateDirectionAndSpeed()
    {
        var cameraDirection = _entityMotor.currentPlayerDirection;

        if (cameraDirection != Vector3.zero)
        {
            if (!isLocked)
            {
                float dotValue = Vector3.Dot(_entityMotor.lastDirection, cameraDirection);

                if (dotValue <= -0.75f)
                    _entityMotor.lastDirection = cameraDirection;
                else
                    _entityMotor.lastDirection = Vector3.RotateTowards(_entityMotor.lastDirection, cameraDirection, _entity.entityData.rotationSpeed * Mathf.Deg2Rad * Time.deltaTime, 0).normalized;
            }
            else
            {
                _entityMotor.lastDirection = cameraDirection;
            }
        }

        var cameraDirectionMagnitude = Mathf.Min(cameraDirection.magnitude, 1.0f);
        speed = GetSpeed(cameraDirectionMagnitude);
    }
    private float GetSpeed(float cameraDirectionMagnitude)
    {
        if (!_entityMotor.isGrounded)
        {
            return _entity.entityData.rollModifer;
        }
        else
        {
            var speed = _entity.GetMovementSpeed();
            speed *= _entityMotor.isCrawling ? 0.25f : 1;
            return cameraDirectionMagnitude * speed;
        }
    }

    public override void Interact()
    {
        if (_entityMotor.isGrounded)
        {
            if (_entityMotor.inputHandler.MoveInput.magnitude < 0.1f)
            {
                if (!isLocked)
                {
                    _entityMotor.SetState(new EntityStateUnarmed(_entityMotor));
                    _entityAnimator.TriggerSheath();
                    return;
                }
                else if (isLocked)
                {
                    _entityMotor.lastDirection = _entityMotor.meshRoot.forward;
                    _entityMotor.SetState(new EntityStateForwardAttack(_entityMotor));
                }

            }
            else
            {
                if (_entityMotor.DirectionIsNotFoward())
                {
                    _entityMotor.SetState(new EntityStateJump(_entityMotor));
                }
                else if (isLocked)
                {
                    _entityMotor.lastDirection = _entityMotor.meshRoot.forward;
                    _entityMotor.SetState(new EntityStateForwardAttack(_entityMotor));
                }
                else
                {
                    _entityMotor.lastDirection = _entityMotor.meshRoot.forward;
                    _entityMotor.SetState(new EntityStateRolling(_entityMotor));
                }
            }
        }
    }
    public override void Loot()
    {
        if (_entityMotor.lootChecker.currentLootable != null)
        {
            _entityMotor.lootChecker.currentLootable.LootItem();
        }
    }

    public override void Attack()
    {
        if (_entityEquipment.EntityInventory.mainHand == null || _entityEquipment.EntityInventory.mainHand.IsValid() == false)
        {
            _entityMotor.SetState(new EntityStateUnarmed(_entityMotor));
        }
        else
        {
            _entityMotor.SetState(new EntityStateAttack(_entityMotor));
        }
    }

    private void UpdateMesh()
    {
        if (isLocked && _entityMotor.isfacingLocked)
        {
            if (_entity.lockOn.CheckIfLockOnInteractionIsValid())
            {
                Vector3 targetPos = _entity.lockOn.GetSeekedPosition();
                targetPos.y = _entityMotor.meshRoot.position.y;
                _entityMotor.meshRoot.rotation = Quaternion.LookRotation(targetPos - _entityMotor.meshRoot.position);
            }

            _entityAnimator.StrafeAxis = Vector3.Dot(_entityMotor.meshRoot.right.normalized, _entityMotor.currentPlayerDirection.normalized);
            _entityAnimator.YAxis = Vector3.Dot(_entityMotor.meshRoot.forward.normalized, _entityMotor.currentPlayerDirection.normalized);
        }
        else
        {
            _entityMotor.meshRoot.rotation = Quaternion.LookRotation(_entityMotor.lastDirection);
            _entityAnimator.YAxis = _entityMotor.lastDirection.normalized.magnitude;
            _entityAnimator.StrafeAxis = 0;
        }
    }
    private void UpdateRunningInPlaceTimer()
    {
        if (Vector3.Distance(lastPosition, _entityMotor.transform.position) < 0.1f)
        {
            timeSpentRunning += Time.deltaTime;
        }
        else
        {
            timeSpentRunning = 0;
        }
        lastPosition = _entityMotor.transform.position;
    }
}
