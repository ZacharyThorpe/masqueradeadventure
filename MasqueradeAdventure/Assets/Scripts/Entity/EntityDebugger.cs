﻿using UnityEngine;

public class EntityDebugger : MonoBehaviour
{
    [SerializeField] private EntityMotor _entityMotor = default;
    [SerializeField] private MeshRenderer mRenderer = default;  

    Material instanceMaterial;

    void Start()
    {
        instanceMaterial = new Material(mRenderer.material);
        mRenderer.material = instanceMaterial;
    }

    void Update()
    {
        if (_entityMotor.isRolling)
            instanceMaterial.color = Color.blue;
        else if(_entityMotor.isStunned)
            instanceMaterial.color = Color.yellow;
        else if (_entityMotor.isAttacking)
            instanceMaterial.color = Color.red;
        else if (_entityMotor.isCrawling)
            instanceMaterial.color = Color.cyan;
        else if (_entityMotor.isArmed)
            instanceMaterial.color = Color.red * 0.25f;
        else if (_entityMotor.isInteracting)
            instanceMaterial.color = Color.green;
        else
            instanceMaterial.color = Color.white;
    }
}
