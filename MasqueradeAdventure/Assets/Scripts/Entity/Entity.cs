﻿using System;
using UnityEngine;
using System.Collections;

public class Entity : MonoBehaviour, IHitOwner
{
    public bool isActive;

    public bool isPlayerControlled;

    public EntityData entityData;

    public EntityMotor entityMotor;
    public EntityAnimatorManager entityAnimator;

    public EntityVisuals entityVisuals;
    public EntityEquipment equipment;

    public Seeker seeker;
    public LockOn lockOn;

    public EnemyHealthBar enemyHealthBar;

    public AnimationTriggerManager hitboxTrigger;

    public Hitbox selfHitBox;

    public bool weaponHitboxAllowed = false;

    [Header("Entity Stats")]
    public int currentHealth = 10;
    public bool vulnerable = true;
    public bool isBlocking = false;

    public Action<Entity> Death;
    public Action<Entity> DropLoot;
    public Action<Entity> HealTaken;
    public Action<Entity> MaskEquipmentChanged;

    public Action<Entity, float> InteractingTimer;

    public Action<string> SecondaryActionText;
    public Action<string> PrimaryActionText;
    public Action<string, Sprite> ConsumableActionButtonUpdate;

    public void Instantiate()
    {
        equipment.EntityInventory.OnMaskEquipped += SetMaxHealth;
        equipment.EntityInventory.OnConsumeEquipped += UpdateConsumable;
        equipment.EntityInventory.OnWeaponEquipped += WeaponUpdated;
    }

    private void OnDestroy()
    {
        equipment.EntityInventory.OnMaskEquipped -= SetMaxHealth;
        equipment.EntityInventory.OnConsumeEquipped -= UpdateConsumable;
        equipment.EntityInventory.OnWeaponEquipped -= WeaponUpdated;
    }

    private void WeaponUpdated(Weapon weapon)
    {
        entityMotor.WeaponUpdated(weapon);
    }

    private void FixedUpdate()
    {
        entityAnimator.OnFixedUpdate(isActive || entityMotor.isDieing);
    }

    public void SetEntityData(EntityData _entityData)
    {
        entityData = _entityData;

        entityVisuals.AssignMaterial(entityData.listOfMaterials);

        SetMaxHealth(null);
    }

    public int GetMaxHealth()
    {
        var mask = equipment.EntityInventory.mask;

        if (mask != null)
        {
            return entityData.totalHealth + mask.AdditionalHealth;
        }
        else
        {
            return entityData.totalHealth;
        }
    }

    public float GetMovementSpeed()
    {
        float movementSpeed = entityData.movementSpeed;
        var mask = equipment.EntityInventory.mask;

        if (mask.IsValid() == true)
        {
            movementSpeed *= 1 + (mask.AdditionalMovement / 100);
        }

        return movementSpeed;
    }

    public void ToggleShieldHitBox(bool _isOn)
    {
        entityVisuals.ToggleShieldHitBox(_isOn);
    }

    public void TakeDamage(HitInfo _hitInfo)
    {
        if (vulnerable && _hitInfo.damage > 0)
        {
            currentHealth -= _hitInfo.damage;
            SetHealthBar(currentHealth);

            GlobalEventsManager.OnDamageTaken?.Invoke(this, _hitInfo.damage, false);

            if (currentHealth <= 0)
            {
                vulnerable = false;
                entityMotor.controller.isKinematic = true;
                entityMotor.Died();
            }
            else
            {
                entityMotor.GotHit(_hitInfo);
                StartCoroutine(Recovery());
            }
        }
    }

    public void TakeHeal(int _healAmount)
    {
        currentHealth += _healAmount;
        SetHealthBar(currentHealth);
        HealTaken?.Invoke(this);
    }

    public void UpdateInteractionTimer(float _value)
    {
        InteractingTimer?.Invoke(this, _value);
    }

    public InteractionType GetInteractionType()
    {
        return entityData.interactsWith;
    }

    public void SetLastDirection(Vector3 direction)
    {
        entityMotor.lastDirection = direction; 
    }

    public void SetOwnerState(EntityStates SetState)
    {
        entityMotor.SetState(SetState);
    }

    private IEnumerator Recovery()
    {
        vulnerable = false;
        yield return new WaitForSeconds(entityData.graceTime);
        vulnerable = true;
    }

    private void SetMaxHealth(Mask mask)
    {
        currentHealth = GetMaxHealth();
        if(enemyHealthBar != null) enemyHealthBar.SetMaxHealth(currentHealth);
    }

    private void SetHealthBar(int amount)
    {
        if (enemyHealthBar != null) enemyHealthBar.SetHealth(amount);
    }

    private void UpdateConsumable(InventoryItem _consumable)
    {
        if (_consumable != null)
        {
            ConsumableSO consumable = DatabaseManager.Instance.FindItem(_consumable.item.itemData.dataGUID) as ConsumableSO;

            switch (consumable.consumable.consumableType)
            {
                case ConsumableType.Healing:
                    ConsumableActionButtonUpdate?.Invoke("Heal\nx" + _consumable.amount, consumable.icon);
                    break;
                case ConsumableType.None:
                default:
                    break;
            }
        }
        else
        {
            ConsumableActionButtonUpdate?.Invoke("", null);
        }
    }

    public Transform GetTransform()
    {
        return transform;
    }

    public void TakeHit(HitInfo _hitInfo, bool isShield)
    {
        if (isBlocking == true) return;

        if (isShield == true)
        {
            SetLastDirection(_hitInfo.direction);
            var state = new EntityStateAttackDeflected(entityMotor)
            {
                hitInfo = _hitInfo,
            };
            SetOwnerState(state);
        }
        else
        {
            entityMotor.GotHit(_hitInfo);
            TakeDamage(_hitInfo);
        }
    }

    public int GetID()
    {
        return GetInstanceID();
    }
}
