﻿using System.Collections;
using UnityEngine;

public class EntityStateDeath : EntityStates
{
    public EntityStateDeath(EntityMotor entityMotor) : base(entityMotor)
    {

    }

    public override IEnumerator BeginState()
    {
        _entityMotor.ResetPrimaryAndSecondaryText();
        _entityAnimator.SetSpeed(1);

        _entityMotor.isDieing = true;
        _entityAnimator.Death();
        _entityMotor.controller.isKinematic = true;
        _entityMotor.controller.detectCollisions = false;

        yield return new WaitForSeconds(1f);
        _entity.DropLoot?.Invoke(_entity);
        yield return new WaitForEndOfFrame();
        _entity.Death?.Invoke(_entity);
    }
}
