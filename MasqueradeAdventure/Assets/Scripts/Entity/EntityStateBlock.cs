﻿using System.Collections;
using UnityEngine;

public class EntityStateBlock : EntityStates
{
    public EntityStateBlock(EntityMotor entityMotor) : base(entityMotor)
    {

    }

    public override IEnumerator BeginState()
    {
        _entity.isBlocking = true;
        _entityMotor.ResetPrimaryAndSecondaryText();
        _entityAnimator.SetSpeed(1);

        _entity.SecondaryActionText("Defend");

        if (!_entityMotor.controller.isKinematic)
        {
            _entityMotor.controller.velocity = Vector3.zero;
        }

        if (_entity.equipment.GetCurrentShield().GetCurrentHealth() >= 1)
        {
            _entity.ToggleShieldHitBox(true);
            _entityAnimator.Blocking();
        }
        _entityMotor.controller.isKinematic = true;

        while(_entity.equipment.GetCurrentShield().GetCurrentHealth() >= 1)
        {
            yield return null;
        }
        LeaveBlocking();
    }

    public override void Hit(HitInfo hitInfo)
    {
        if(_entity.equipment.GetCurrentShield().GetCurrentHealth() < 1)
        {
            LeaveBlocking();
        }
    }

    public override void LeaveSecondary()
    {
        LeaveBlocking();
    }

    private void LeaveBlocking()
    {
        _entityAnimator.Idle();
        _entity.ToggleShieldHitBox(false);
        _entityMotor.controller.isKinematic = false;
        _entityMotor.SetArmedState();
        _entity.isBlocking = false;
    }

    public override void Reset()
    {
        base.Reset();
        _entityMotor.controller.isKinematic = false;
        _entityMotor.isStunned = false;
        _entity.ToggleShieldHitBox(false);
    }
}
