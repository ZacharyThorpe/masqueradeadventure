﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class EntityAnimatorManager : MonoBehaviour
{
    [SerializeField] private Animator animator = default;

    [SerializeField] private float smoothing = 1;

    public float StrafeAxis { get; set; }
    public float YAxis { get; set; }
    public bool isGrounded { get; set; }
    public bool isCrawling { get; set; }
    private float targetStrafeAxis = 0;
    private float targetYAxis = 0;

    private HashSet<string> allParameters =new HashSet<string>();

    private void Awake()
    {
        if(animator != null)
        {
            for (int i = 0; i < animator.parameterCount; i++)
            {
                allParameters.Add(animator.parameters[i].name);
            }
        }
    }

    public void OnFixedUpdate(bool isActive)
    {
        animator.gameObject.SetActive(isActive);
        if (isActive == false) return;

        if (animator != null)
        {
            targetStrafeAxis = Mathf.Lerp(targetStrafeAxis, StrafeAxis, smoothing * Time.deltaTime);
            targetYAxis = Mathf.Lerp(targetYAxis, YAxis, smoothing * Time.deltaTime);

            SetFloat("StrafeAxis", targetStrafeAxis);
            SetFloat("YAxis", targetYAxis);

            SetBool("isGrounded", isGrounded);
            SetBool("IsCrawling", isCrawling);

            if (YAxis > 0.1f)
            {
                ResetTrigger("DoneLooting");
            }
        }
    }

    public void ResetVariables()
    {
        YAxis = 0;
        StrafeAxis = 0;
    }

    public void Jumped()
    {
        SetTrigger("Jump");
    }
    public void Roll()
    {
        SetTrigger("Roll");
    }
    public void Attack()
    {
        SetTrigger("Attack");
    }
    public void AttackLarge()
    {
        SetTrigger("AttackLarge");
    }
    public void AttackJump()
    {
        SetTrigger("AttackJump");
    }

    public void Stun()
    {
        SetTrigger("Stunned");
    }
    public void Deflected()
    {
        SetTrigger("Deflected");
    }

    public void Hit()
    {
        SetTrigger("Hit");
    }
    public void Death()
    {
        SetTrigger("Death");
    }
    public void Crawl()
    {
        SetTrigger("Crawl");
    }
    public void Stand()
    {
        SetTrigger("Stand");
    }

    public void Loot()
    {
        SetTrigger("Loot");
    }
    public void DoneLooting()
    {
        SetTrigger("DoneLooting");
    }

    public void SetSpeed(float _speed)
    {
        if (animator != null)
        {
            animator.speed = _speed;
        }
    }

    public void Blocking()
    {
        SetTrigger("Blocking");
    }
    public void Idle()
    {
        SetTrigger("Idle");
    }
    public void UseConsume()
    {
        SetTrigger("UseConsume");
    }
    public void FinishConsume()
    {
        SetTrigger("DoneConsuming");
    }
    public void OneHander()
    {
        SetBool("1HandArmed", true);
    }

    public void Unarmed()
    {
        SetBool("1HandArmed", false);
    }
    public void TriggerDifferentIdle()
    {
        SetTrigger("TriggerRandomIdle");
    }

    public void SetStrafeJump(float strafeJump)
    {
        SetFloat("StrafeJump", strafeJump);
    }

    public void Spawn()
    {
        SetTrigger("Spawn");
    }

    public void Extract()
    {
        SetTrigger("Extract");
    }

    public void TriggerSheath()
    {
        SetTrigger("SheathAction");
    }

    private void SetBool(string parameter,  bool value)
    {
        if(animator != null)
        {
            if(allParameters.Contains(parameter))
            {
                animator.SetBool(parameter, value);
            }
        }
    }

    private void SetTrigger(string parameter)
    {
        if (animator != null)
        {
            if (allParameters.Contains(parameter))
            {
                animator.SetTrigger(parameter);
            }
        }
    }

    private void ResetTrigger(string parameter)
    {
        if (animator != null)
        {
            if (allParameters.Contains(parameter))
            {
                animator.ResetTrigger(parameter);
            }
        }
    }

    private void SetFloat(string parameter, float value)
    {
        if (animator != null)
        {
            if (allParameters.Contains(parameter))
            {
                animator.SetFloat(parameter, value);
            }
        }
    }
}
