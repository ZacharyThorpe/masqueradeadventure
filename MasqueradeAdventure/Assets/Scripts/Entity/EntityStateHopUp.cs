﻿using System.Collections;
using UnityEngine;

public class EntityStateHopUp : EntityStates
{
    public EntityStateHopUp(EntityMotor entityMotor) : base(entityMotor)
    {

    }
    public override IEnumerator BeginState()
    {
        base.BeginState();

        _entityAnimator.ResetVariables();

        _entityMotor.gravity.y += _entity.entityData.hopForce;
        _entityAnimator.Jumped();

        yield return new WaitForSeconds(0.05f);

        while (!_entityMotor.isGrounded)
        {
            if(!_entityMotor.CheckIfAboveHopableHeight())
            {
                break;
            }
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(0.15f);
        _entityMotor.controller.velocity += _entityMotor.meshRoot.forward.normalized * _entity.entityData.hopModifier;

        while (!_entityMotor.isGrounded)
        {
            yield return new WaitForEndOfFrame();
        }

        _entityMotor.controller.velocity = Vector3.zero;

        yield return new WaitForSeconds(_entity.entityData.jumpRecovery);

        _entityMotor.SetArmedState();
    }
}
