using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public abstract class DataContext : MonoBehaviour
{
    public GameData gameData;

    public abstract void Load<T>();

    public abstract void Save<T>();

    public abstract void Delete();

    public T Get<T>() where T : GameData
    {
        return gameData as T;
    }
}
