using Newtonsoft.Json;
using System;
using System.IO;
using UnityEngine;

public class JsonDataContext : DataContext
{
    public string fileName;

    public override void Load<T>()
    {
        var fullPath = Path.Combine(Application.persistentDataPath, fileName);

        if (!File.Exists(fullPath))
        {
            gameData = default;
            return;
        }

        try
        {
            string result = File.ReadAllText(fullPath);

            JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All, Formatting = Formatting.Indented };

            var readData = JsonConvert.DeserializeObject<T>(result, settings);

            if (readData == null) gameData = default;

            if (typeof(T) == typeof(PlayerQuestData))
            {
                gameData = readData as PlayerQuestData;
            }

        }

        catch (Exception e)
        {
            Debug.LogError($"Failed to read from {fullPath} with exception {e}");
            gameData = default;
            return;
        }
    }

    public override void Save<T>()
    {
        JsonSerializerSettings settings = new() { TypeNameHandling = TypeNameHandling.All, Formatting = Formatting.Indented };

        string jsonString = JsonConvert.SerializeObject(Convert.ChangeType(gameData, typeof(T)), settings);

        var fullPath = Path.Combine(Application.persistentDataPath, fileName);

        if (File.Exists(fullPath))
        {
            File.Delete(fullPath);
        }

        try
        {
            File.WriteAllText(fullPath, jsonString);
        }

        catch (Exception e)
        {
            Debug.LogError($"Failed to write to {fullPath} with exception {e}");
        }
    }

    public override void Delete()
    {
        var fullPath = Path.Combine(Application.persistentDataPath, fileName);

        if (File.Exists(fullPath))
        {
            gameData = null;
            File.Delete(fullPath);
        }
    }
}
