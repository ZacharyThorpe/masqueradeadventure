using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class AIStateAttack : AIState
{
    public AIStateAttack(InputHandlerNPC inputHandler, NavMeshAgent navMeshAgent) : base(inputHandler, navMeshAgent)
    {
        _inputHandler.currentStateName = "Attack";
    }

    public override IEnumerator BeginState()
    {
        _inputHandler.entity.entityMotor.isfacingLocked = true;

        while (_inputHandler.entity.isActive == false) yield return new WaitForEndOfFrame();

        _inputHandler.MoveInput = Vector2.zero;

        while (Vector3.Distance(_inputHandler.entity.transform.position, _inputHandler.entity.lockOn.GetSeekedPosition()) >= _inputHandler.entity.equipment.EntityInventory.mainHand.AttackRange)
        {
            ChaseLockedTarget();
            yield return new WaitForEndOfFrame();
        }

        _inputHandler.BButtonOn?.Invoke();
        yield return new WaitForSeconds(1 / _inputHandler.entity.equipment.EntityInventory.mainHand.AttackPerSecond);

        if(_inputHandler.entity.lockOn.CheckIfLockOnInteractionIsValid()) _inputHandler.SetState(new AiStateAggro(_inputHandler, _navMeshAgent));
        else _inputHandler.SetState(new AiStateAggro(_inputHandler, _navMeshAgent));
    }

    public override void LostTarget()
    {
        _inputHandler.SetState(new AIStateRoam(_inputHandler, _navMeshAgent));
    }
    private Vector3 ChaseLockedTarget()
    {
        Vector3 targetPosition = _inputHandler.entity.lockOn.GetSeekedPosition();

        Vector3 relativeDirection = _navMeshAgent.transform.InverseTransformDirection(_navMeshAgent.desiredVelocity);
        _navMeshAgent.SetDestination(targetPosition);

        _inputHandler.Forward?.Invoke(relativeDirection.normalized);

        _navMeshAgent.transform.localPosition = Vector3.zero;
        _navMeshAgent.transform.localRotation = Quaternion.identity;

        _inputHandler.MoveInput = Vector2.right;

        return relativeDirection;
    }
}
