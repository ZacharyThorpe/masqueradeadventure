using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class AIStateSurprised : AIState
{
    public AIStateSurprised(InputHandlerNPC inputHandler, NavMeshAgent navMeshAgent) : base(inputHandler, navMeshAgent)
    {
        inputHandler.currentStateName = "Surpised";
    }
    public override IEnumerator BeginState()
    {
        while (_inputHandler.entity.isActive == false) yield return new WaitForEndOfFrame();

        _inputHandler.MoveInput = Vector2.zero;

        _inputHandler.entity.entityMotor.isSurprised = true;

        if (!_inputHandler.entity.entityMotor.isArmed)
        {
            _inputHandler.BButtonOn?.Invoke();
            yield return new WaitForSeconds(0.75f);
        }
        else
        {
            yield return new WaitForSeconds(0.5f);
        }

        _inputHandler.entity.entityMotor.isSurprised = false;

        _inputHandler.SetState(new AiStateAggro(_inputHandler, _navMeshAgent));
    }
    public override void LostTarget()
    {
        _inputHandler.entity.entityMotor.isSurprised = false;
        _inputHandler.SetState(new AIStateRoam(_inputHandler, _navMeshAgent));
    }
}
