﻿using System.Collections;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Splines;
using Random = UnityEngine.Random;

public class AIStateRoam : AIState
{
    public AIStateRoam(InputHandlerNPC inputHandler, NavMeshAgent navMeshAgent) : base(inputHandler, navMeshAgent)
    {
        inputHandler.currentStateName = "Roaming";
    }

    public override IEnumerator BeginState()
    {
        while (_inputHandler.entity.isActive == false) yield return new WaitForEndOfFrame();

        while (true)
        {
            while (_inputHandler.entity.isActive == false) yield return new WaitForEndOfFrame();

            if(_inputHandler.SplineContainer != null) 
            {
                float distance = SplineUtility.GetNearestPoint(_inputHandler.SplineContainer.Spline, _navMeshAgent.transform.position, out float3 nearestPosition, out float normalizedPosition);
                Vector3 direction = (Vector3)_inputHandler.SplineContainer.Spline.EvaluatePosition((normalizedPosition + 0.01f) % 1) - _navMeshAgent.transform.position;
                direction = new Vector3(direction.x, 0, direction.z);
                _inputHandler.Forward?.Invoke(direction.normalized * _inputHandler.entity.entityData.roamMovementSpeedModifier);
                _inputHandler.MoveInput = Vector2.right;
                Debug.DrawRay(_navMeshAgent.transform.position, direction, Color.red, 0.25f);
                yield return new WaitForEndOfFrame();
            }
            else
            {
                MoveRandomDirection();

                float stateTime = 0;
                while (stateTime < _inputHandler.entity.entityData.roamAroundTime)
                {
                    stateTime += Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }

                HoldPosition();
                stateTime = 0;
                while (stateTime < _inputHandler.entity.entityData.idleTime)
                {
                    stateTime += Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }
            }
        }
    }

    public override void FoundTarget()
    {
        _inputHandler.SetState(new AIStateSurprised(_inputHandler, _navMeshAgent));
    }

    void MoveRandomDirection()
    {
        Vector3 direction = Vector3.forward;

        if (Vector3.Distance(_navMeshAgent.transform.position, leasePole) > _inputHandler.entity.entityData.leaseDistance)
        {
            direction = (leasePole - _navMeshAgent.transform.position).normalized;
            direction = new Vector3(direction.x, 0, direction.z);
        }
        else
        {
            direction = DetectingIfHitWall(new Vector3(Random.Range(-1, 1f), 0, Random.Range(-1, 1f)));
        }


        _inputHandler.Forward?.Invoke(direction.normalized * _inputHandler.entity.entityData.roamMovementSpeedModifier);
        _inputHandler.MoveInput = Vector2.right;
    }

    void HoldPosition()
    {
        _inputHandler.MoveInput = Vector2.zero;
    }

    Vector3 DetectingIfHitWall(Vector3 direction)
    {
        Transform groundNormal = _inputHandler.entity.entityMotor.GetGroundNormal();
        Quaternion lookAtRotation = Quaternion.LookRotation(direction);
        groundNormal.localRotation = Quaternion.Euler(groundNormal.localRotation.eulerAngles.x, lookAtRotation.eulerAngles.y, groundNormal.localRotation.eulerAngles.z);

        if (Physics.SphereCast(_inputHandler.entity.entityMotor.transform.position, 0.1f, groundNormal.forward, out RaycastHit hit, _inputHandler.entity.GetMovementSpeed(), _inputHandler.entity.entityData.rollingStunLayer))
        {
            return -direction;
        }
        return direction;
    }

}
