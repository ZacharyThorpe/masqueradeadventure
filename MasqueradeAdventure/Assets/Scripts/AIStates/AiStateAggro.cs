﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class AiStateAggro : AIState
{
    public AiStateAggro(InputHandlerNPC _inputHandler, NavMeshAgent navMeshAgent) : base(_inputHandler, navMeshAgent)
    {
        _inputHandler.currentStateName = "Aggro";
    }

    public override IEnumerator BeginState()
    {
        while(true)
        {
            while(_inputHandler.entity.isActive == false) yield return new WaitForEndOfFrame();

            if(_inputHandler.entity.lockOn.CheckIfLockOnInteractionIsValid())
            {
                var direction = ChaseLockedTarget();

                if (Vector3.Distance(_inputHandler.entity.transform.position, _inputHandler.entity.lockOn.GetSeekedPosition()) <= _inputHandler.entity.equipment.EntityInventory.mainHand.AttackRange && IsFacingTarget(direction))
                {
                    _inputHandler.MoveInput = Vector2.zero;
                    if(Random.Range(0,1f) > 0.85f)
                    {
                        _inputHandler.SetState(new AIStateStrafe(_inputHandler, _navMeshAgent));
                    }
                    else
                    {
                        _inputHandler.SetState(new AIStateAttack(_inputHandler, _navMeshAgent));
                    }
                }
            }
            yield return new WaitForEndOfFrame();
        }
    }

    public override void LostTarget()
    {
        _inputHandler.SetState(new AIStateRoam(_inputHandler, _navMeshAgent));
    }

    private Vector3 ChaseLockedTarget()
    {        
        Vector3 targetPosition = _inputHandler.entity.lockOn.GetSeekedPosition();

        Vector3 relativeDirection = _navMeshAgent.transform.InverseTransformDirection(_navMeshAgent.desiredVelocity);
        _navMeshAgent.SetDestination(targetPosition);

        _inputHandler.Forward?.Invoke(relativeDirection.normalized);

        _navMeshAgent.transform.localPosition = Vector3.zero;
        _navMeshAgent.transform.localRotation = Quaternion.identity;

        _inputHandler.MoveInput = Vector2.right;

        return relativeDirection;
    }

    private bool IsFacingTarget(Vector3 direction)
    {
        float dotValue = Vector3.Dot(_inputHandler.entity.entityMotor.meshRoot.forward, direction);
        return dotValue > 0.95f;
    }
}
