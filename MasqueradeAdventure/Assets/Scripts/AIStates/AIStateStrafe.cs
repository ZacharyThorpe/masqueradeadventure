using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class AIStateStrafe : AIState
{
    private Vector2 strafeDirection = Vector2.up;
    private float variedStrafeTiming = 0;

    public AIStateStrafe(InputHandlerNPC inputHandler, NavMeshAgent navMeshAgent) : base(inputHandler, navMeshAgent)
    {
        _inputHandler.currentStateName = "Strafe";
    }

    public override IEnumerator BeginState()
    {
        variedStrafeTiming = 0;

        _inputHandler.entity.entityMotor.isfacingLocked = true;
        yield return new WaitForEndOfFrame();

        while (_inputHandler.entity.isActive == false) yield return new WaitForEndOfFrame();

        RandomizeStrafe();

        while(variedStrafeTiming < _inputHandler.entity.entityData.strafeTimer)
        {
            ChaseLockedTarget();
            variedStrafeTiming += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        if (_inputHandler.entity.lockOn.CheckIfLockOnInteractionIsValid())
        {
            if (Vector3.Distance(_inputHandler.entity.transform.position, _inputHandler.entity.lockOn.GetSeekedPosition()) <= _inputHandler.entity.equipment.EntityInventory.mainHand.AttackRange)
            {
                _inputHandler.SetState(new AIStateAttack(_inputHandler, _navMeshAgent));
            }
            else
            {
                _inputHandler.MoveInput = Vector2.zero;
                _inputHandler.entity.entityMotor.isfacingLocked = false;
                yield return new WaitForSeconds(0.15f);
                _inputHandler.SetState(new AiStateAggro(_inputHandler, _navMeshAgent));
            }
        }
        else
        {
            _inputHandler.MoveInput = Vector2.zero;
            _inputHandler.entity.entityMotor.isfacingLocked = false;
            yield return new WaitForSeconds(0.15f);
            _inputHandler.SetState(new AIStateRoam(_inputHandler, _navMeshAgent));
        }
    }
    public override void LostTarget()
    {
        _inputHandler.entity.entityMotor.isfacingLocked = false;
        _inputHandler.SetState(new AIStateRoam(_inputHandler, _navMeshAgent));
    }

    private Vector3 ChaseLockedTarget()
    {
        Vector3 targetPosition = _inputHandler.entity.lockOn.GetSeekedPosition();

        Vector3 relativeDirection = _navMeshAgent.transform.InverseTransformDirection(_navMeshAgent.desiredVelocity);
        _navMeshAgent.SetDestination(targetPosition);

        var cross = Vector3.Cross(relativeDirection, strafeDirection);
        _inputHandler.Forward?.Invoke(cross.normalized * _inputHandler.entity.entityData.strafeSpeedModifier);

        _navMeshAgent.transform.localPosition = Vector3.zero;
        _navMeshAgent.transform.localRotation = Quaternion.identity;

        _inputHandler.MoveInput = Vector2.right;

        return relativeDirection;
    }

    private void RandomizeStrafe()
    {
        strafeDirection = Random.Range(0, 1f) > 0.5f ? Vector2.up : Vector2.down;
        variedStrafeTiming += Random.Range(0, 0.25f);
    }
}
