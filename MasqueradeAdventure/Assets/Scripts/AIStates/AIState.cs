﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public abstract class AIState
{
    protected InputHandlerNPC _inputHandler;
    protected NavMeshAgent _navMeshAgent;

    public Vector3 leasePole = Vector3.zero;

    public AIState(InputHandlerNPC inputHandler, NavMeshAgent navMeshAgent)
    {
        _inputHandler = inputHandler;
        _navMeshAgent = navMeshAgent;
    }

    public virtual IEnumerator BeginState() { yield break; }
    public virtual void FoundTarget() { }
    public virtual void LostTarget() { }
}
