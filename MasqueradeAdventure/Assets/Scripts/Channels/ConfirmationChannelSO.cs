using System;
using UnityEngine;

[CreateAssetMenu(fileName = "Confirm Channel", menuName = "Data/Confirm Channel", order = 1)]
public class ConfirmationChannelSO : ScriptableObject
{
    public Action<string, Action> OpenConfirmationAction = default;

    public void RaiseEvent(string message, Action confirmCallback)
    {
        OpenConfirmationAction?.Invoke(message, confirmCallback);
    }
}
