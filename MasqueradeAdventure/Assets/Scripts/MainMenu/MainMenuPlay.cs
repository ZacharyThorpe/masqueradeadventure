using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class MainMenuPlay : MonoBehaviour
{
    [SerializeField] private GameObject defaultSelection = default;
    [SerializeField] private GameObject lastSelection = default;

    [SerializeField] private GameObject playPanel = default;
    [SerializeField] private CanvasGroup canvasGroup = default;
    [SerializeField] private LoadEventChannelSO sceneLoader = default;
    [SerializeField] private GameSceneSO sceneToLoad = default;

    void Start()
    {
        TogglePlayPanel(true);
        lastSelection = defaultSelection;
    }

    private void OnEnable()
    {
        InputModeDetection.OnControlChanged += UpdateKeyboardAndMouseSettings;
    }

    private void OnDisable()
    {
        InputModeDetection.OnControlChanged -= UpdateKeyboardAndMouseSettings;
    }

    public void SetLastSelected(GameObject selected)
    {
        if (InputModeDetection.currentlyKeyboard == true) return;

        lastSelection = selected;
    }

    public void TogglePlayPanel(bool _isActive)
    {
        playPanel.SetActive(_isActive);
        ToggleCanvasGroup(_isActive);
    }

    public void ToggleCanvasGroup(bool _isActive)
    {
        canvasGroup.interactable = _isActive;

        if (_isActive)
        {
            StartCoroutine(SetNewSelection());
        }
    }

    public void LoadNextScene()
    {
        AudioManager.Instance.ToggleMusic(false);
        sceneLoader.RaiseEvent(sceneToLoad);
    }

    private IEnumerator SetNewSelection()
    {
        yield return new WaitForEndOfFrame();

        if (InputModeDetection.currentlyKeyboard == false)
        {
            EventSystem.current.SetSelectedGameObject(lastSelection);
        }
    }

    private void UpdateKeyboardAndMouseSettings(bool _isMouseAndKeyboard)
    {
        if (playPanel.activeInHierarchy)
        {
            EventSystem.current.SetSelectedGameObject(null);
            if (!_isMouseAndKeyboard)
            {
                EventSystem.current.SetSelectedGameObject(defaultSelection);
            }

            Cursor.lockState = _isMouseAndKeyboard ? CursorLockMode.None : CursorLockMode.Locked;
        }
    }
}
