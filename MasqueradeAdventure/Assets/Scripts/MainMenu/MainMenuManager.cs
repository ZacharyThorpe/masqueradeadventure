using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] private GameObject defaultSelection = default;

    [SerializeField] private InventoryUI inventoryUI = default;
    [SerializeField] private MainMenuPlay playMenu = default;
    [SerializeField] private MainMenuQuest mainMenuQuest = default;
    [SerializeField] private MainMenuShop mainMenuShop = default;
    [SerializeField] private MainMenuForge mainMenuForge = default;

    [SerializeField] private InventorySO stash = default;

    private UIInputAction m_Controls = default;

    private void Awake()
    {
        m_Controls = new UIInputAction();

        m_Controls.UIAction.CloseMenu.started += (InputAction.CallbackContext _inputCallback) => LoadPlayMenu();
    }

    private void OnDestroy()
    {
        m_Controls.UIAction.CloseMenu.started -= (InputAction.CallbackContext _inputCallback) => LoadPlayMenu();
    }

    private void OnEnable()
    {
        m_Controls.Enable();
    }

    private void OnDisable()
    {
        m_Controls.Disable();
    }

    private void Start()
    {
        LoadPlayMenu();
        AudioManager.Instance.ToggleMusic(true);
    }

    public void LoadPlayMenu()
    {
        ResetAllPanels();
        playMenu.TogglePlayPanel(true);
    }

    public void LoadInventory()
    {
        ResetAllPanels();
        inventoryUI.OpenWithOtherInventory(ref stash.inventory, "Stash");
        inventoryUI.TogglePanel(true);
    }

    public void LoadQuests()
    {
        ResetAllPanels();
        mainMenuQuest.ToggleQuestPanel(true);
    }

    public void LoadShops()
    {
        ResetAllPanels();
        mainMenuShop.ToggleShopPanel(true);
    }

    public void LoadForge()
    {
        ResetAllPanels();
        mainMenuForge.ToggleForgePanel(true);
    }

    private void ResetAllPanels()
    {
        playMenu.TogglePlayPanel(false);
        inventoryUI.TogglePanel(false);
        mainMenuQuest.ToggleQuestPanel(false);
        mainMenuShop.ToggleShopPanel(false);
        mainMenuForge.ToggleForgePanel(false);
    }
}
