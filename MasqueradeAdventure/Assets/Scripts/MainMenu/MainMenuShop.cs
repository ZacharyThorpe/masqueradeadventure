using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.EventSystems;

public class MainMenuShop : MonoBehaviour
{
    [SerializeField] private GameObject defaultSelection = default;

    [SerializeField] private GameObject shopPanel = default;

    [SerializeField] private TradeGood itemCurrency = default;

    [SerializeField] private InventorySO stashSO;
    [SerializeField] private UIInventoryItem stashUI = default;
    [SerializeField] private List<UIInventoryItem> listOfStashItems = default;

    [SerializeField] private UIInventoryItem storeUI = default;
    [SerializeField] private List<UIInventoryItem> listOfStoreItems = default;

    private GameObject lastCategory = default;
    private int selectedIndex = -1;

    [SerializeField] private NPCMenuLister npcMenuList = default;

    private void OnEnable()
    {
        InputModeDetection.OnControlChanged += UpdateKeyboardAndMouseSettings;
        npcMenuList.NewSelection += RefreshShop;
    }

    private void OnDisable()
    {
        InputModeDetection.OnControlChanged -= UpdateKeyboardAndMouseSettings;
        npcMenuList.NewSelection -= RefreshShop;
    }

    public void ToggleShopPanel(bool _isActive)
    {
        shopPanel.SetActive(_isActive);
        InstaniateItemUI(ref stashSO, ref listOfStashItems, ref stashUI, SellItem, false);

        StartCoroutine(SetNewSelection());
    }

    private void RefreshShop(NPCSO npcSO)
    {
        InstaniateItemUI(ref stashSO, ref listOfStashItems, ref stashUI, SellItem, false);

        if (npcSO != null && npcSO.npcInventory != null)
        {
            InstaniateItemUI(ref npcSO.npcInventory, ref listOfStoreItems, ref storeUI, BuyItem, true);
        }

        StartCoroutine(SetNewSelection());
    }

    private IEnumerator SetNewSelection()
    {
        yield return new WaitForEndOfFrame();

        if (InputModeDetection.currentlyKeyboard == false)
        {
            if (lastCategory != null && selectedIndex > -1)
            {
                if (lastCategory.transform.childCount > 1)
                {
                    int selectedIndexInList = selectedIndex + 1;
                    if (lastCategory.transform.childCount <= selectedIndexInList)
                    {
                        selectedIndexInList = 1;
                    }

                    UIInventoryItem uiInventory = lastCategory.transform.GetChild(selectedIndexInList).GetComponent<UIInventoryItem>();
                    if (uiInventory != null)
                    {
                        EventSystem.current.SetSelectedGameObject(uiInventory.button.gameObject);
                    }
                    else
                    {
                        EventSystem.current.SetSelectedGameObject(defaultSelection);
                    }
                }
                else
                {
                    EventSystem.current.SetSelectedGameObject(defaultSelection);
                }
            }
            else
            {
                EventSystem.current.SetSelectedGameObject(defaultSelection);
            }

            lastCategory = null;
            selectedIndex = -1;
        }
        else
        {
            EventSystem.current.SetSelectedGameObject(null);
        }
    }

    private void InstaniateItemUI(ref InventorySO _inventoryData, ref List<UIInventoryItem> listOfItems, ref UIInventoryItem inventoryUI, Action<InventoryItem, GameObject> callback, bool _isShop)
    {
        inventoryUI.gameObject.SetActive(false);
        for (int i = 0; i < listOfItems.Count; i++)
        {
            listOfItems[i].OnEquipItem = null;
            Destroy(listOfItems[i].gameObject);
        }

        listOfItems = new List<UIInventoryItem>();

        for (int i = 0; i < _inventoryData.inventory.items.Count; i++)
        {
            var clone = Instantiate(inventoryUI, inventoryUI.transform.parent);
            clone.Instantiate(_inventoryData.inventory.items[i]);
            clone.gameObject.SetActive(true);
            clone.OnEquipItem += callback;
            if (_isShop)
            {
                clone.OnHoverItem += InspectShop;
            }
            else
            {
                clone.OnHoverItem += InspectItem;
            }
            listOfItems.Add(clone);
        }
    }

    private void InspectItem(InventoryItem _item, GameObject selectedElement)
    {
        if (_item == null) InspectionInventoryUI.Instance.InspectNPC(null, selectedElement);
        else InspectionInventoryUI.Instance.InspectItem(_item.item, selectedElement);
    }

    private void InspectShop(InventoryItem _item, GameObject selectedElement)
    {
        if (_item == null) InspectionInventoryUI.Instance.InspectNPC(null, selectedElement);
        else InspectionInventoryUI.Instance.InspectItem(_item.item, selectedElement, true);
    }

    private void SellItem(InventoryItem _itemToSell, GameObject selectedElement)
    {
        ItemDataSO itemSO = DatabaseManager.Instance.FindItem(_itemToSell.item.itemData.dataGUID);

        if (itemSO.sellAmount > 0)
        {
            lastCategory = stashUI.transform.parent.gameObject;
            selectedIndex = stashSO.RemoveItem(_itemToSell, 1);

            InventoryItem tradeCurrency = new(itemCurrency, itemSO.sellAmount);

            stashSO.AddAnItem(tradeCurrency);

            RefreshShop(npcMenuList.CurrentNPCSO);
        }
    }

    private void BuyItem(InventoryItem _itemToSell, GameObject selectedElement)
    {
        ItemDataSO itemSO = DatabaseManager.Instance.FindItem(_itemToSell.item.itemData.dataGUID);

        if (stashSO.CheckAmount(itemCurrency) > itemSO.buyAmount)
        {
            lastCategory = storeUI.transform.parent.gameObject;
            selectedIndex = npcMenuList.CurrentNPCSO.npcInventory.inventory.items.IndexOf(_itemToSell);

            InventoryItem costItem = new InventoryItem(itemCurrency, itemSO.buyAmount);

            stashSO.RemoveItem(costItem, itemSO.buyAmount);
            InventoryItem newItem = new InventoryItem(_itemToSell.item, 1);
            stashSO.AddAnItem(newItem.item);
            RefreshShop(npcMenuList.CurrentNPCSO);
        }
    }

    private void BuyBackItem(InventoryItem _itemToSell)
    {
        ItemDataSO itemSO = DatabaseManager.Instance.FindItem(_itemToSell.item.itemData.dataGUID);

        if (stashSO.CheckAmount(itemCurrency) > itemSO.sellAmount)
        {
            InventoryItem costItem = new InventoryItem(itemCurrency, itemSO.sellAmount);

            stashSO.RemoveItem(costItem, costItem.amount);

            stashSO.AddAnItem(_itemToSell.item);
            RefreshShop(npcMenuList.CurrentNPCSO);
        }
    }

    private void UpdateKeyboardAndMouseSettings(bool _isMouseAndKeyboard)
    {
        if (shopPanel.activeInHierarchy)
        {
            EventSystem.current.SetSelectedGameObject(null);
            if (!_isMouseAndKeyboard)
            {
                EventSystem.current.SetSelectedGameObject(defaultSelection);
            }

            Cursor.lockState = _isMouseAndKeyboard ? CursorLockMode.None : CursorLockMode.Locked;
        }
    }
}
