using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainMenuForge : MonoBehaviour
{
    [SerializeField] private GameObject defaultSelection;

    [SerializeField] private GameObject forgePanel = default;

    [SerializeField] private GameObject levelUpButtonGO = default;
    [SerializeField] private Button levelUpButton = default;
    [SerializeField] private TextMeshProUGUI costAmount = default;
    [SerializeField] private TextMeshProUGUI nameOfItem = default;

    [SerializeField] private InventorySO stash;
    [SerializeField] private EntityInventorySO playerEquipment;
    [SerializeField] private TradeGood itemCurrency = default;

    [SerializeField] private UIInventoryItem stashUI = default;
    [SerializeField] private List<UIInventoryItem> listOfStashItems = default;

    [SerializeField] private UIInventoryItem equipment = default;
    [SerializeField] private List<UIInventoryItem> listOfEquipmentItems = default;

    [SerializeField] private UIInventoryItem forgeSlot = default;

    [SerializeField] private UIInventoryItem runeUI = default;
    [SerializeField] private List<UIInventoryItem> listOfActiveRunes = default;

    [SerializeField] private InventoryItem currentForging = default;

    [SerializeField] private UIForgeStats forgeStatsPrefab = default;
    [SerializeField] private List<UIForgeStats> listForgeStats = default;

    [SerializeField] private UIForgeStats newForgeStatsPrefab = default;
    [SerializeField] private List<UIForgeStats> newListForgeStats = default;

    private GameObject lastCategory = default;
    private int selectedIndex = -1;

    [SerializeField] private NPCMenuLister npcMenuList = default;

    private void OnEnable()
    {
        InputModeDetection.OnControlChanged += UpdateKeyboardAndMouseSettings;
        npcMenuList.NewSelection += RefreshUI;
    }

    private void OnDisable()
    {
        InputModeDetection.OnControlChanged -= UpdateKeyboardAndMouseSettings;
        npcMenuList.NewSelection += RefreshUI;
    }

    private void Start()
    {
        currentForging = null;

        forgeSlot.ResetSlot();
        forgeSlot.OnSendToContainer += RemoveFromForge;
        forgeSlot.OnHoverItem += InspectItemForge;
    }

    public void ToggleForgePanel(bool _isActive)
    {
        forgePanel.SetActive(_isActive);
        nameOfItem.text = string.Empty;

        if(_isActive == true)
        {
            currentForging = null;
            RefreshUI(null);
        }
    }

    private void RefreshUI(NPCSO npcSO)
    {
        InstaniateList(ref stash, ref listOfStashItems, ref stashUI, CheckWhatForgeSlotToGo);
        InstaniateEquipment();
        forgeSlot.Instantiate(currentForging);

        forgeStatsPrefab.gameObject.SetActive(false);
        for (int i = 0; i < listForgeStats.Count; i++)
        {
            Destroy(listForgeStats[i].gameObject);
        }
        listForgeStats = new();

        newForgeStatsPrefab.gameObject.SetActive(false);
        for (int i = 0; i < newListForgeStats.Count; i++)
        {
            Destroy(newListForgeStats[i].gameObject);
        }
        newListForgeStats = new();

        if (currentForging != null && currentForging.item != null)
        {
            ItemDataSO itemSO = DatabaseManager.Instance.FindItem(currentForging.item.itemData.dataGUID);
            nameOfItem.text = itemSO.displayName;

            Dictionary<string, string> ListOfStats = new Dictionary<string, string>();
            Dictionary<string, string> ListOfLevelUpStats = new Dictionary<string, string>();

            switch (itemSO.itemType)
            {
                case ItemType.None:
                    break;
                case ItemType.Weapon:
                    var weapon = currentForging.item as Weapon;
                    ListOfStats = weapon.GetListOfCurrentLevelStats();
                    ListOfLevelUpStats = weapon.GetListOfLevelUpStats();
                    break;
                case ItemType.Shield:
                    var shield = currentForging.item as Shield;
                    ListOfStats = shield.GetListOfCurrentLevelStats();
                    ListOfLevelUpStats = shield.GetListOfLevelUpStats();
                    break;
                case ItemType.Mask:
                    var mask = currentForging.item as Mask;
                    ListOfStats = mask.GetListOfCurrentLevelStats();
                    ListOfLevelUpStats = mask.GetListOfLevelUpStats();
                    break;
                default:
                    break;
            }

            foreach (var item in ListOfStats)
            {
                var clone = Instantiate(forgeStatsPrefab, forgeStatsPrefab.transform.parent);
                clone.SetText(item.Key, item.Value);
                clone.gameObject.SetActive(true);
                listForgeStats.Add(clone);
            }

            foreach (var item in ListOfLevelUpStats)
            {
                var clone = Instantiate(newForgeStatsPrefab, newForgeStatsPrefab.transform.parent);
                clone.SetText(item.Key, item.Value);
                clone.gameObject.SetActive(true);
                newListForgeStats.Add(clone);
            }
        }
        else
        {
            nameOfItem.text = string.Empty;
        }

        InstaniateRunes();
        UpdateLevelUpButton();
        SetItemsAsActive();

        StartCoroutine(SetNewSelection());
    }

    private IEnumerator SetNewSelection()
    {
        yield return new WaitForEndOfFrame();

        if (InputModeDetection.currentlyKeyboard == false)
        {
            if (lastCategory != null && selectedIndex > -1)
            {
                if (lastCategory.transform.childCount > 1)
                {
                    int selectedIndexInList = selectedIndex + 1;
                    if (lastCategory.transform.childCount <= selectedIndexInList)
                    {
                        selectedIndexInList = 1;
                    }

                    UIInventoryItem uiInventory = lastCategory.transform.GetChild(selectedIndexInList).GetComponent<UIInventoryItem>();

                    if(uiInventory.button.interactable == false)
                    {
                        uiInventory = GetFirstInteractableChild(lastCategory.transform);
                    }

                    if (uiInventory != null)
                    {
                        EventSystem.current.SetSelectedGameObject(null);
                        EventSystem.current.SetSelectedGameObject(uiInventory.button.gameObject);
                    }
                    else
                    {
                        EventSystem.current.SetSelectedGameObject(null);
                        EventSystem.current.SetSelectedGameObject(defaultSelection);
                    }
                }
                else
                {
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(defaultSelection);
                }
            }
            else
            {
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(defaultSelection);
            }

            lastCategory = null;
            selectedIndex = -1;
        }
        else
        {
            EventSystem.current.SetSelectedGameObject(null);
        }
    }

    private UIInventoryItem GetFirstInteractableChild(Transform transform)
    {
        foreach (Transform child in transform) 
        {
            var UIInventoryItem = child.GetComponent<UIInventoryItem>();
            if (UIInventoryItem != null && UIInventoryItem.button.interactable)
            {
                return child.GetComponent<UIInventoryItem>();
            }
        }
        return null;
    }

    private void InstaniateList(ref InventorySO listOfInventoryItems, ref List<UIInventoryItem> listOfItems, ref UIInventoryItem inventoryUI, Action<InventoryItem, GameObject> callback)
    {
        inventoryUI.gameObject.SetActive(false);
        for (int i = 0; i < listOfItems.Count; i++)
        {
            listOfItems[i].OnEquipItem = null;
            Destroy(listOfItems[i].gameObject);
        }

        listOfItems = new();

        for (int i = 0; i < listOfInventoryItems.inventory.items.Count; i++)
        {
            var clone = Instantiate(inventoryUI, inventoryUI.transform.parent);
            clone.Instantiate(listOfInventoryItems.inventory.items[i]);
            clone.gameObject.SetActive(true);
            clone.OnEquipItem += callback;
            clone.OnHoverItem += InspectItem;
            listOfItems.Add(clone);
        }
    }

    private void InstaniateEquipment()
    {
        equipment.gameObject.SetActive(false);
        for (int i = 0; i < listOfEquipmentItems.Count; i++)
        {
            listOfEquipmentItems[i].OnSendToContainer = null;
            Destroy(listOfEquipmentItems[i].gameObject);
        }

        listOfEquipmentItems = new();

        if (playerEquipment != null)
        {
            if(playerEquipment.mainHand != null && playerEquipment.mainHand.IsValid())
            {
                var clone = Instantiate(equipment, equipment.transform.parent);
                SetToEquipmentSlot(playerEquipment.mainHand, clone);
                ItemForgableData forgableItem = playerEquipment.mainHand.itemData as ItemForgableData;
            }
            if (playerEquipment.shield != null && playerEquipment.shield.IsValid())
            {
                var clone = Instantiate(equipment, equipment.transform.parent);
                SetToEquipmentSlot(playerEquipment.shield, clone);
            }
            if (playerEquipment.mask != null && playerEquipment.mask.IsValid())
            {
                var clone = Instantiate(equipment, equipment.transform.parent);
                SetToEquipmentSlot(playerEquipment.mask, clone);
            }
        }
    }

    public void SetToEquipmentSlot(Item _item, UIInventoryItem _inventoryItem)
    {
        _inventoryItem.ResetSlot();

        if (_item == null || !_item.IsValid()) return;

        ItemDataSO itemSO = DatabaseManager.Instance.FindItem(_item.itemData.dataGUID);

        if (itemSO == null) return;

        InventoryItem newEquipment = new(_item, 1);

        _inventoryItem.Instantiate(newEquipment);
        _inventoryItem.gameObject.SetActive(true);
        _inventoryItem.OnEquipItem += CheckWhatForgeSlotToGo;
        _inventoryItem.OnHoverItem += InspectItem;

        listOfEquipmentItems.Add(_inventoryItem);
    }

    private void InstaniateRunes()
    {
        runeUI.gameObject.SetActive(false);
        for (int i = 0; i < listOfActiveRunes.Count; i++)
        {
            listOfActiveRunes[i].OnSendToContainer = null;
            Destroy(listOfActiveRunes[i].gameObject);
        }

        listOfActiveRunes = new();

        if (currentForging != null && currentForging.item != null)
        {
            List<ItemAffixSO> activeAffixes = new List<ItemAffixSO>();

            if (forgeSlot.item.item is ItemForgable)
            {
                ItemForgable forgable = forgeSlot.item.item as ItemForgable;

                ItemForgableData forgableItem = forgeSlot.item.item.itemData as ItemForgableData;

                for (int i = 0; i < forgableItem.listOfAffixesGUIDs.Count; i++)
                {
                    ItemAffixSO itemAffixSO = DatabaseManager.Instance.FindItem(forgableItem.listOfAffixesGUIDs[i]) as ItemAffixSO;
                    activeAffixes.Add(itemAffixSO);
                }
            }

            for (int i = 0; i < activeAffixes.Count; i++)
            {
                var clone = Instantiate(runeUI, runeUI.transform.parent);
                Item rune = new ItemAffixItem(activeAffixes[i]);
                InventoryItem inventoryItem = new(rune, 1);
                clone.Instantiate(inventoryItem);
                clone.gameObject.SetActive(true);
                clone.OnSendToContainer += RemoveFromForgable;
                clone.OnHoverItem += InspectItem;
                listOfActiveRunes.Add(clone);
            }
        }

        for (int i = 0; listOfActiveRunes.Count < 3; i++)
        {
            var clone = Instantiate(runeUI, runeUI.transform.parent);
            clone.Instantiate(null);
            clone.gameObject.SetActive(true);
            listOfActiveRunes.Add(clone);
        }
    }

    private void CheckWhatForgeSlotToGo(InventoryItem _inventoryItem, GameObject selectedElement)
    {
        lastCategory = stashUI.transform.parent.gameObject;
        selectedIndex = stash.inventory.items.IndexOf(_inventoryItem);

        ItemDataSO itemSO = DatabaseManager.Instance.FindItem(_inventoryItem.item.itemData.dataGUID);

        if (_inventoryItem.item.GetType().IsSubclassOf(typeof(ItemForgable)))
        {
            SendToForge(_inventoryItem);
        }
        else if (itemSO.itemType == ItemType.ItemAffix)
        {
            AddToForable(_inventoryItem);
        }
    }

    private void SendToForge(InventoryItem _inventoryItem)
    {
        currentForging = _inventoryItem;

        SetToFirstStashSlot();

        RefreshUI(npcMenuList.CurrentNPCSO);
    }

    private void RemoveFromForge(InventoryItem _inventoryItem)
    {
        currentForging = null;
        InspectionInventoryUI.Instance.ClearInspection();

        SetToFirstStashSlot();

        RefreshUI(npcMenuList.CurrentNPCSO);
    }

    private void RemoveFromForgable(InventoryItem _runeToRemove)
    {
        ItemDataSO itemSO = DatabaseManager.Instance.FindItem(_runeToRemove.item.itemData.dataGUID);

        foreach (var activeAffix in listOfActiveRunes)
        {
            ItemDataSO activeAffixSO = DatabaseManager.Instance.FindItem(activeAffix.item.item.itemData.dataGUID);

            if (activeAffixSO.GUID == itemSO.GUID)
            {
                lastCategory = runeUI.transform.parent.gameObject;

                ItemForgable currentForgable = currentForging.item as ItemForgable;
                ItemAffixSO affixData = itemSO as ItemAffixSO;
                currentForgable.RemoveAffix(affixData);

                ItemForgableData forgableItem = currentForgable.itemData as ItemForgableData;

                selectedIndex = forgableItem.listOfAffixesGUIDs.Count - 1;

                if (selectedIndex == -1)
                {
                    SetToFirstStashSlot();
                }

                stash.AddAnItem(_runeToRemove);

                InspectionInventoryUI.Instance.ClearInspection();
                RefreshUI(npcMenuList.CurrentNPCSO);
                break;
            }
        }
    }

    private void AddToForable(InventoryItem _runeToAdd)
    {
        ItemDataSO itemSO = DatabaseManager.Instance.FindItem(_runeToAdd.item.itemData.dataGUID);

        if (currentForging == null || currentForging.item == null) return;
        if (!currentForging.item.GetType().IsSubclassOf(typeof(ItemForgable))) return;
        if (itemSO.itemType != ItemType.ItemAffix) return;

        ItemForgable forgable = currentForging.item as ItemForgable;

        ItemDataSO forgableSO = DatabaseManager.Instance.FindItem(forgable.itemData.dataGUID);

        ItemForgableData forgableItem = forgable.itemData as ItemForgableData;

        if (forgableItem.listOfAffixesGUIDs.Count >= forgableSO.maxRunes) return;

        ItemAffixSO newAffix = itemSO as ItemAffixSO;

        if (!newAffix.allowedSlots.Contains(forgableSO.itemType)) return;

        forgable.AddAffix(newAffix);

        lastCategory = stashUI.transform.parent.gameObject;
        selectedIndex = stash.RemoveItem(_runeToAdd, 1);

        RefreshUI(npcMenuList.CurrentNPCSO);
    }

    public void UpgradeForable()
    {
        if (currentForging == null || currentForging.item == null) return;
        if (!currentForging.item.GetType().IsSubclassOf(typeof(ItemForgable))) return;

        ItemForgable forgable = currentForging.item as ItemForgable;
        ItemDataSO itemSO = DatabaseManager.Instance.FindItem(forgable.itemData.dataGUID);

        ItemForgableData forgableItem = forgable.itemData as ItemForgableData;

        int upgradeAmount = itemSO.costCurveToLevel.GetCostAmount(forgableItem.currentLevel, itemSO.maxLevel);

        if (forgableItem.currentLevel >= itemSO.maxLevel) return;

        if (stash.CheckAmount(itemCurrency) > upgradeAmount)
        {
            InventoryItem costItem = new(itemCurrency, upgradeAmount);

            stash.RemoveItem(costItem, upgradeAmount);

            forgable.LevelUp();

            playerEquipment.SaveInventory();
            stash.SaveInventory();

            RefreshUI(npcMenuList.CurrentNPCSO);
        }
    }

    private void InspectItem(InventoryItem _item, GameObject selectedElement)
    {
        if (_item == null) InspectionInventoryUI.Instance.InspectNPC(null, selectedElement);
        else InspectionInventoryUI.Instance.InspectItem(_item.item, selectedElement);
    }
    private void InspectItemForge(InventoryItem _item, GameObject selectedElement)
    {
        if (_item == null) InspectionInventoryUI.Instance.InspectNPC(null, selectedElement);
        else InspectionInventoryUI.Instance.InspectItem(_item.item, selectedElement, false, true);
    }

    private void SetToFirstStashSlot()
    {
        if (stash.inventory.items.Count > 0)
        {
            lastCategory = stashUI.transform.parent.gameObject;
            selectedIndex = 0;
        }
    }

    private void UpdateLevelUpButton()
    {
        levelUpButtonGO.SetActive(false);
        levelUpButton.interactable = false;

        if (currentForging == null || currentForging.item == null) return;

        if (currentForging.item.GetType().IsSubclassOf(typeof(ItemForgable)))
        {
            ItemForgable forgable = currentForging.item as ItemForgable;

            ItemDataSO itemSO = DatabaseManager.Instance.FindItem(forgable.itemData.dataGUID);

            ItemForgableData forgableItem = forgable.itemData as ItemForgableData;

            int upgradeAmount = itemSO.costCurveToLevel.GetCostAmount(forgableItem.currentLevel, itemSO.maxLevel);

            if (stash.CheckAmount(itemCurrency) > upgradeAmount)
            {
                levelUpButton.interactable = true;
            }

            if (forgableItem.currentLevel < itemSO.maxLevel)
            {
                costAmount.text = string.Format("{0}g", upgradeAmount);
                levelUpButtonGO.SetActive(true);
            }
        }
    }

    private void UpdateKeyboardAndMouseSettings(bool _isMouseAndKeyboard)
    {
        if (forgePanel.activeInHierarchy)
        {
            EventSystem.current.SetSelectedGameObject(null);
            if (!_isMouseAndKeyboard)
            {
                EventSystem.current.SetSelectedGameObject(defaultSelection);
            }

            Cursor.lockState = _isMouseAndKeyboard ? CursorLockMode.None : CursorLockMode.Locked;
        }
    }

    private void SetItemsAsActive()
    {
        if(currentForging != null && currentForging.item != null && currentForging.item.GetType().IsSubclassOf(typeof(ItemForgable)))
        {
            for (int i = 0; i < listOfStashItems.Count; i++)
            {
                if(listOfStashItems[i].item.item.itemData.itemType == ItemType.ItemAffix)
                { 
                    ItemForgable forgable = currentForging.item as ItemForgable;

                    ItemDataSO forgableSO = DatabaseManager.Instance.FindItem(forgable.itemData.dataGUID);

                    ItemAffixSO newAffix = DatabaseManager.Instance.FindItem(listOfStashItems[i].item.item.itemData.dataGUID) as ItemAffixSO;

                    if (newAffix.allowedSlots.Contains(forgableSO.itemType)) listOfStashItems[i].button.interactable = true;
                    else listOfStashItems[i].button.interactable = false;
                }
                else
                {
                    listOfStashItems[i].button.interactable = false;
                }
            }
        }
        else
        {
            for (int i = 0; i < listOfStashItems.Count; i++)
            {
                listOfStashItems[i].button.interactable = true;
            }
        }
    }
}
