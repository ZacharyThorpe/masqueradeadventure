﻿using System;

[Flags]
public enum NPCInteractionType
{
    None = 0,
    Shop = 1,
    Forge = 2,
    Quest = 4,
}
