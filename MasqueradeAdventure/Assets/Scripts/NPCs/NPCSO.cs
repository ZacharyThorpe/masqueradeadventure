using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NPC Data", menuName = "Data/NPC", order = 1)]
public class NPCSO : ScriptableObject
{
    public string npcName = "NPC Person";
    public string GUID = string.Empty;
    public NPCInteractionType interactionType;
    public Sprite npcSprite;
    public Sprite npcSplash;

    public string nameOfService = string.Empty;

    public InventorySO npcInventory;
}