public class ItemAffixItem : Item
{
    public ItemAffixItem()
    {

    }

    public ItemAffixItem(ItemDataSO _data)
    {
        if (_data != null)
        {
            itemData = new ItemData();
            GenerateNewGUID();
            itemData.dataGUID = _data.GUID;
            itemData.itemType = ItemType.ItemAffix;
        }
    }

    public override string GetDescription()
    {
        var data = DatabaseManager.Instance.FindItem(itemData.dataGUID) as ItemAffixSO;
        return data.affix.GetDescription() + "\n";
    }
}
