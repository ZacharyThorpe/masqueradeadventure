﻿using System;
using UnityEngine;

[Serializable]
public class RarityAssets
{
    public Sprite frameSprite;
    public Color rarityColor;
}
