using UnityEngine;

[SerializeField]
public abstract class ItemForgable : Item
{
    public virtual void RemoveAffix(ItemAffixSO _itemAffixData)
    {
        ItemForgableData forgableData = itemData as ItemForgableData;

        foreach (var item in forgableData.listOfAffixesGUIDs)
        {
            if (item == _itemAffixData.GUID)
            {
                forgableData.listOfAffixesGUIDs.RemoveAt(forgableData.listOfAffixesGUIDs.IndexOf(item));
                break;
            }
        }
    }

    public virtual void AddAffix(ItemAffixSO _itemAffixData)
    {
        ItemForgableData forgableData = itemData as ItemForgableData;
        forgableData.listOfAffixesGUIDs.Add(_itemAffixData.GUID);
    }

    public virtual void LevelUp()
    {
        ItemForgableData forgableData = itemData as ItemForgableData;
        forgableData.currentLevel++;
    }
}
