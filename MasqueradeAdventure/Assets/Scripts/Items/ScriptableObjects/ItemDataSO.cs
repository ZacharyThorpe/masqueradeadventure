using Sirenix.OdinInspector;
using UnityEngine;

public abstract class ItemDataSO : ScriptableObject
{
    public ItemType itemType;
    public string GUID = "ITEM";
    public string displayName = "Item";
    [PreviewField(50, ObjectFieldAlignment.Left)]
    public Sprite icon = default;

    public ItemDropSprite prefab = default;
    public Rarity rarity = Rarity.Common;

    public int maxRunes = 3;

    public bool stackable = false;

    public int maxLevel = 0;
    public int buyAmount = 1;
    public int sellAmount = 1;

    public ItemCostScaleSO costCurveToLevel = default;

}
