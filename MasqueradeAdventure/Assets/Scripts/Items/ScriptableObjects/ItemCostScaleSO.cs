using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Cost Curve", menuName = "Data/CostCurve", order = 1)]
public class ItemCostScaleSO : ScriptableObject
{
    [SerializeField] private AnimationCurve costCurve = default;

    public int GetCostAmount(int level, int maxLevel)
    {
        return (int)costCurve.Evaluate((float)level / maxLevel);
    }
}
