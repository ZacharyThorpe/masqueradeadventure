using UnityEngine;

[CreateAssetMenu(fileName = "Bag Data", menuName = "Data/Bag", order = 1)]
public class BagSO : ItemDataSO
{
    public ItemMesh equipmentPrefab = default;
    public GameObject prefabMonster = default;
    public int additionalBagSpace = 1;
}
