﻿using UnityEngine;

[CreateAssetMenu(fileName = "Shield Data", menuName = "Data/Shield", order = 1)]
public class ShieldSO : ItemDataSO
{
    public GameObject runePrefab;
    public ItemMesh equipmentPrefab = default;
    public GameObject prefabMonster = default;

    public int damage = 0;
    public float hitReoveryTime = 0.25f;
    public float hitKnockbackBasePower = 1f;
    public float hitKnockbackTime = 0.15f;

    public int shieldCharges = 3;
    public float shieldRecovery = 0.25f;

    public int shieldChargesScaling = 1;
    public float shieldRecoveryScaling = 1f;
}