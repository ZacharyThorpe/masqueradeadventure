using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Consumable Data", menuName = "Data/Consumable", order = 1)]
public class ConsumableSO : ItemDataSO
{
    [SerializeReference]
    public ItemConsumable consumable = default;
}
