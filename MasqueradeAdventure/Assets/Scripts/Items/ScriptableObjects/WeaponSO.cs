﻿using UnityEngine;

[CreateAssetMenu(fileName = "Weapon Data", menuName = "Data/Weapon", order = 1)]
public class WeaponSO : ItemDataSO
{
    public ItemMesh equipmentPrefab = default;

    public int damage = 1;
    public float damageMultiplier = 1.5f;

    public float attackSpeed = 1f;
    public float greaterAttackSpeed = 1f;
    public float attackLung = 1f;
    public float attackLungDecay = 0.99f;

    public float attackRange = 1f;

    public float attackJumpAttackSpeed = 1f;
    public float attackJumpForce = 1f;
    public float attackJumpRecovery = 1f;

    public float hitReoveryTime = 0.25f;
    public float hitKnockbackBasePower = 1f;
    public float hitKnockbackTime = 0.15f;

    public int damageScaling = 2;
}
