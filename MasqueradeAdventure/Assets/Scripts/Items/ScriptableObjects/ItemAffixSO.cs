using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapon Data", menuName = "Data/Item Affix", order = 1)]
public class ItemAffixSO : ItemDataSO
{
    [SerializeReference]
    public ItemAffix affix = default;

    public List<ItemType> allowedSlots = default;
}
