using UnityEngine;

[CreateAssetMenu(fileName = "Mask Data", menuName = "Data/Mask", order = 1)]
public class MaskSO : ItemDataSO
{
    public ItemMesh equipmentPrefab = default;
    public GameObject prefabMonster = default;

    public int additionalHealth = 3;
    public int healthGrowth = 1;

    public float additionalMovement = 1;
    public int movementGrowth = 1;
}
