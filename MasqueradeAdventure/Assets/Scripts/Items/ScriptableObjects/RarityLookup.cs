using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Rarity Lookup", menuName = "Data/Rarity", order = 1)]
public class RarityLookup : ScriptableObject
{
    public RarityAssetsByRarity rarityAssetsByRarity;
}
