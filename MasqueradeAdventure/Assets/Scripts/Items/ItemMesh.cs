using Sirenix.OdinInspector;
using UnityEngine;

public class ItemMesh : MonoBehaviour
{
    public Renderer meshRenderer;

    [SerializeField] private float m_outlineScale = 1.0f;
    [SerializeField] private float m_outlineWidth = 1.0f;

    [Button("Set Values")]
    public void SetValues()
    {
        m_outlineScale = meshRenderer.material.GetFloat("_OutlineScale");
        m_outlineWidth = meshRenderer.material.GetFloat("_OutlineWidth");
    }

    public void SetEmissive(Color color)
    {
        if (meshRenderer != null)
        {
            meshRenderer.material.SetColor("_OutlineColor", color);
            meshRenderer.material.SetFloat("_OutlineScale", m_outlineScale);
            meshRenderer.material.SetFloat("_OutlineWidth", m_outlineWidth);
        }
    }
}
