using System;

[Serializable]
public class TradeGood : Item
{
    public TradeGood()
    {

    }

    public TradeGood(ItemDataSO _data)
    {
        if (_data != null)
        {
            itemData = new ItemData();
            GenerateNewGUID();
            itemData.dataGUID = _data.GUID;
            itemData.itemType = ItemType.TradeGood;
        }
    }
}
