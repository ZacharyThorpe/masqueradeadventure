﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;

[Serializable]
public abstract class Item
{
    [SerializeReference]
    public ItemData itemData;

    [SerializeField]
    private ItemDataSO itemSO;

    [Button("Update GUID")]
    public void UpdateGUIData()
    {
        if(itemSO != null && itemData != null)
        {
            itemData.dataGUID = itemSO.GUID;
        }
    }
    [Button("Generate GUID")]
    public void GenerateGUID()
    {
        if (itemSO != null && itemData != null)
        {
            itemData.GUID = Guid.NewGuid().ToString();
        }
    }

    public Item()
    {
            
    }

    public Item(Item _itemToClone)
    {
        itemData = new ItemData()
        {
            itemType = _itemToClone.itemData.itemType,
            dataGUID = _itemToClone.itemData.dataGUID,
            GUID = Guid.NewGuid().ToString(),
        };
        itemSO = _itemToClone.itemSO;
    }

    public Item(ItemDataSO _data)
    {
        GenerateNewGUID();
        if (_data != null)
        {
            itemData.dataGUID = _data.GUID;
        }
    }

    public bool IsValid()
    {
        if (itemData == null) return false;
        if (string.IsNullOrEmpty(itemData.dataGUID)) return false;
        if (string.IsNullOrEmpty(itemData.GUID)) return false;

        return true;
    }

    public void GenerateNewGUID()
    {
        if(itemData != null)
        {
            itemData.GUID = Guid.NewGuid().ToString();
        }
    }

    public virtual string GetDescription()
    {
        return "Item Description";
    }

    public virtual string GetLevelUpDescription()
    {
        return "Item Description";
    }

    public virtual Dictionary<string,string> GetListOfLevelUpStats() 
    {
        return new Dictionary<string, string>() { };
    }

    public virtual Dictionary<string, string> GetListOfCurrentLevelStats()
    {
        return new Dictionary<string, string>() { };
    }
}
