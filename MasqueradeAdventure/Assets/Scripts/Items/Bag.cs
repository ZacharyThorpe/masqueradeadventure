using System;
using UnityEngine;

[Serializable]
public class Bag : Item
{
    public Bag()
    {

    }
    public Bag(ItemDataSO _data)
    {
        if (_data != null)
        {
            itemData = new ItemData();
            GenerateNewGUID();
            itemData.dataGUID = _data.GUID;
            itemData.itemType = ItemType.Bag;
        }
    }

    public int AdditionalBagSpace
    {
        get
        {
            BagSO bagData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as BagSO;
            return bagData.additionalBagSpace;
        }
    }

    public ItemMesh EquipmentPrefab
    {
        get
        {
            BagSO bagData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as BagSO;
            return bagData.equipmentPrefab;
        }
    }

    public override string GetDescription()
    {
        return string.Format("{0} Increase Bag Slots\n", AdditionalBagSpace);
    }
}
