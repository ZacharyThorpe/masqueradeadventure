﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Weapon : ItemForgable
{
    public Weapon()
    {

    }

    public Weapon(ItemDataSO _data)
    {
        if(_data != null)
        {
            itemData = new ItemForgableData();
            GenerateNewGUID();
            itemData.itemType = ItemType.Weapon;
            itemData.dataGUID = _data.GUID;
        }
    }

    public float AttackJumpAttackSpeed
    {
        get
        {
            WeaponSO weaponData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as WeaponSO;
            return weaponData.attackJumpAttackSpeed;
        }
    }
    public float AttackJumpForce
    {
        get
        {
            WeaponSO weaponData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as WeaponSO;
            return weaponData.attackJumpForce;
        }
    }
    public float AttackJumpRecovery
    {
        get
        {
            WeaponSO weaponData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as WeaponSO;
            return weaponData.attackJumpRecovery;
        }
    }
    public float AttackLung
    {
        get
        {
            WeaponSO weaponData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as WeaponSO;
            return weaponData.attackLung;
        }
    }
    public float AttackLungDecay
    {
        get
        {
            WeaponSO weaponData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as WeaponSO;
            return weaponData.attackLungDecay;
        }
    }
    public float AttackRange
    {
        get
        {
            WeaponSO weaponData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as WeaponSO;
            return weaponData.attackRange;
        }
    }
    public float AttackPerSecond
    {
        get
        {
            WeaponSO weaponData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as WeaponSO;

            float totalAttackSpeedPercent = 1;

            ItemForgableData forgableData = itemData as ItemForgableData;

            for (int i = 0; i < forgableData.listOfAffixesGUIDs.Count; i++)
            {
                ItemAffixSO affix = DatabaseManager.Instance.FindItem(forgableData.listOfAffixesGUIDs[i]) as ItemAffixSO;
                if (affix.affix.affixType == AffixType.AttackSpeed)
                {
                    AffixAttackSpeed attackSpeedAffix = affix.affix as AffixAttackSpeed;
                    totalAttackSpeedPercent += attackSpeedAffix.attackSpeed / 100;
                }
            }

            float totalAttackSpeed = weaponData.attackSpeed * totalAttackSpeedPercent;

            return Mathf.Round(totalAttackSpeed * 100f) / 100f;
        }
    }
    public int Damage
    {
        get
        {
            WeaponSO weaponData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as WeaponSO;

            int weaponDamage = weaponData.damage;

            ItemForgableData forgableData = itemData as ItemForgableData;

            weaponDamage += forgableData.currentLevel * weaponData.damageScaling;

            for (int i = 0; i < forgableData.listOfAffixesGUIDs.Count; i++)
            {
                ItemAffixSO affix = DatabaseManager.Instance.FindItem(forgableData.listOfAffixesGUIDs[i]) as ItemAffixSO;
                if (affix.affix.affixType == AffixType.Attack)
                {
                    AffixDamage affixDamage = affix.affix as AffixDamage;
                    weaponDamage += affixDamage.damageIncrease;
                }
            }

            return weaponDamage;
        }
    }

    public float DamageMultiplier
    {
        get
        {
            WeaponSO weaponData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as WeaponSO;
            return weaponData.damageMultiplier;
        }
    }

    public int DamageLevelUp
    {
        get
        {
            WeaponSO weaponData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as WeaponSO;

            int weaponDamage = weaponData.damage;

            ItemForgableData forgableData = itemData as ItemForgableData;

            weaponDamage += (forgableData.currentLevel + 1) * weaponData.damageScaling;

            for (int i = 0; i < forgableData.listOfAffixesGUIDs.Count; i++)
            {
                ItemAffixSO affix = DatabaseManager.Instance.FindItem(forgableData.listOfAffixesGUIDs[i]) as ItemAffixSO;
                if (affix.affix.affixType == AffixType.Attack)
                {
                    AffixDamage affixDamage = affix.affix as AffixDamage;
                    weaponDamage += affixDamage.damageIncrease;
                }
            }

            return weaponDamage;
        }
    }

    public float GreaterAttackSpeed
    {
        get
        {
            WeaponSO weaponData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as WeaponSO;
            return weaponData.greaterAttackSpeed;
        }
    }
    public float HitKnockbackBasePower
    {
        get
        {
            WeaponSO weaponData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as WeaponSO;
            return weaponData.hitKnockbackBasePower;
        }
    }
    public float HitKnockbackTime
    {
        get
        {
            WeaponSO weaponData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as WeaponSO;
            return weaponData.hitKnockbackTime;
        }
    }
    public float HitReoveryTime
    {
        get
        {
            WeaponSO weaponData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as WeaponSO;
            return weaponData.hitReoveryTime;
        }
    }

    public ItemMesh EquipmentPrefab
    {
        get
        {
            WeaponSO weaponData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as WeaponSO;
            return weaponData.equipmentPrefab;
        }
    }

    public override string GetDescription()
    {
        ItemForgableData forgableDataWeapon = itemData as ItemForgableData;

        string stats = string.Format("Level {0}\n", forgableDataWeapon.currentLevel);
        stats += "Damage: " + Damage + "\n";
        stats += string.Format("{0} attacks per second\n", AttackPerSecond);

        for (int i = 0; i < forgableDataWeapon.listOfAffixesGUIDs.Count; i++)
        {
            ItemAffixSO itemAffixSO = DatabaseManager.Instance.FindItem(forgableDataWeapon.listOfAffixesGUIDs[i]) as ItemAffixSO;
            stats += itemAffixSO.affix.GetDescription() + "\n";
        }
        return stats;
    }

    public override string GetLevelUpDescription()
    {
        ItemForgableData forgableDataWeapon = itemData as ItemForgableData;

        string stats = string.Format("Level {0} -> {1}\n", forgableDataWeapon.currentLevel, forgableDataWeapon.currentLevel + 1);
        stats += string.Format("Damage {0} -> {1}\n", Damage, DamageLevelUp);
        stats += string.Format("{0} attacks per second\n", AttackPerSecond);

        for (int i = 0; i < forgableDataWeapon.listOfAffixesGUIDs.Count; i++)
        {
            ItemAffixSO itemAffixSO = DatabaseManager.Instance.FindItem(forgableDataWeapon.listOfAffixesGUIDs[i]) as ItemAffixSO;
            stats += itemAffixSO.affix.GetDescription() + "\n";
        }
        return stats;
    }

    public override Dictionary<string, string> GetListOfLevelUpStats()
    {
        Dictionary<string,string> stats = new Dictionary<string,string>();
        ItemForgableData forgableDataWeapon = itemData as ItemForgableData;

        stats.Add("Level", string.Format("{0}", forgableDataWeapon.currentLevel + 1));
        stats.Add("Damage", string.Format("{0}", DamageLevelUp));
        stats.Add("Attacks per second", string.Format("{0}", AttackPerSecond));

        return stats;
    }

    public override Dictionary<string, string> GetListOfCurrentLevelStats()
    {
        Dictionary<string, string> stats = new Dictionary<string, string>();
        ItemForgableData forgableDataWeapon = itemData as ItemForgableData;

        stats.Add("Level", string.Format("{0}", forgableDataWeapon.currentLevel));
        stats.Add("Damage", string.Format("{0}", Damage));
        stats.Add("Attacks per second", string.Format("{0}", AttackPerSecond));

        return stats;
    }
}
