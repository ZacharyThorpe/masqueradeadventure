using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemData
{
    public ItemType itemType = ItemType.None;
    public string GUID = string.Empty;
    public string dataGUID = string.Empty;
}
