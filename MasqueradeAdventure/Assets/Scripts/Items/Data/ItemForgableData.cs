using System.Collections.Generic;

public class ItemForgableData : ItemData
{
    public List<string> listOfAffixesGUIDs = new List<string>();
    public int currentLevel = 1;
}
