using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Mask : ItemForgable
{
    public Mask()
    {

    }
    public Mask(ItemDataSO _data)
    {
        if (_data != null)
        {
            itemData = new ItemForgableData();
            GenerateNewGUID();
            itemData.dataGUID = _data.GUID;
            itemData.itemType = ItemType.Mask;
        }
    }

    public int AdditionalHealth
    {
        get
        {
            MaskSO maskData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as MaskSO;

            int maskHealth = maskData.additionalHealth;

            ItemForgableData forgableData = itemData as ItemForgableData;

            maskHealth += forgableData.currentLevel * maskData.healthGrowth;

            for (int i = 0; i < forgableData.listOfAffixesGUIDs.Count; i++)
            {
                ItemAffixSO affix = DatabaseManager.Instance.FindItem(forgableData.listOfAffixesGUIDs[i]) as ItemAffixSO;
                if (affix.affix.affixType == AffixType.Health)
                {
                    AffixHealth affixHealth = affix.affix as AffixHealth;
                    maskHealth += affixHealth.healthIncrease;
                }
            }

            return maskHealth;
        }
    }

    public int AdditionalHealthLevelUp
    {
        get
        {
            MaskSO maskData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as MaskSO;

            int maskHealth = maskData.additionalHealth;

            ItemForgableData forgableData = itemData as ItemForgableData;

            maskHealth += (forgableData.currentLevel + 1) * maskData.healthGrowth;

            for (int i = 0; i < forgableData.listOfAffixesGUIDs.Count; i++)
            {
                ItemAffixSO affix = DatabaseManager.Instance.FindItem(forgableData.listOfAffixesGUIDs[i]) as ItemAffixSO;
                if (affix.affix.affixType == AffixType.Health)
                {
                    AffixHealth affixHealth = affix.affix as AffixHealth;
                    maskHealth += affixHealth.healthIncrease;
                }
            }

            return maskHealth;
        }
    }

    public float AdditionalMovement
    {
        get
        {
            MaskSO maskData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as MaskSO;

            float maskMovement = maskData.additionalMovement;

            ItemForgableData forgableData = itemData as ItemForgableData;

            maskMovement += forgableData.currentLevel * maskData.movementGrowth;

            for (int i = 0; i < forgableData.listOfAffixesGUIDs.Count; i++)
            {
                ItemAffixSO affix = DatabaseManager.Instance.FindItem(forgableData.listOfAffixesGUIDs[i]) as ItemAffixSO;
                if (affix.affix.affixType == AffixType.Movement)
                {
                    AffixMovement affixHealth = affix.affix as AffixMovement;
                    maskMovement += affixHealth.movementSpeedIncrease;
                }
            }

            return maskMovement;
        }
    }
    public float AdditionalMovementLevelUp
    {
        get
        {
            MaskSO maskData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as MaskSO;

            float maskMovement = maskData.additionalMovement;

            ItemForgableData forgableData = itemData as ItemForgableData;

            maskMovement += (forgableData.currentLevel + 1) * maskData.movementGrowth;

            for (int i = 0; i < forgableData.listOfAffixesGUIDs.Count; i++)
            {
                ItemAffixSO affix = DatabaseManager.Instance.FindItem(forgableData.listOfAffixesGUIDs[i]) as ItemAffixSO;
                if (affix.affix.affixType == AffixType.Movement)
                {
                    AffixMovement affixHealth = affix.affix as AffixMovement;
                    maskMovement += affixHealth.movementSpeedIncrease;
                }
            }

            return maskMovement;
        }
    }

    public ItemMesh EquipmentPrefab
    {
        get
        {
            MaskSO maskData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as MaskSO;
            return maskData.equipmentPrefab;
        }
    }

    public GameObject prefabMonster
    {
        get
        {
            MaskSO maskData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as MaskSO;
            return maskData.prefabMonster;
        }
    }

    public override string GetDescription()
    {
        ItemForgableData forgableDataMask = itemData as ItemForgableData;

        string stats = string.Format("Level {0}\n", forgableDataMask.currentLevel);
        stats += string.Format("+{0} Health\n", AdditionalHealth);
        stats += string.Format("{0}% Movement Speed\n", AdditionalMovement);

        for (int i = 0; i < forgableDataMask.listOfAffixesGUIDs.Count; i++)
        {
            ItemAffixSO itemAffixSO = DatabaseManager.Instance.FindItem(forgableDataMask.listOfAffixesGUIDs[i]) as ItemAffixSO;
            stats += itemAffixSO.affix.GetDescription() + "\n";
        }
        return stats;
    }

    public override string GetLevelUpDescription()
    {
        ItemForgableData forgableDataMask = itemData as ItemForgableData;

        string stats = string.Format("Level {0} -> {1}\n", forgableDataMask.currentLevel, forgableDataMask.currentLevel + 1); 
        stats += string.Format("+{0} -> {1} Health\n", AdditionalHealth, AdditionalHealthLevelUp);
        stats += string.Format("{0}% -> {1}% Movement Speed\n", AdditionalMovement, AdditionalMovementLevelUp);

        for (int i = 0; i < forgableDataMask.listOfAffixesGUIDs.Count; i++)
        {
            ItemAffixSO itemAffixSO = DatabaseManager.Instance.FindItem(forgableDataMask.listOfAffixesGUIDs[i]) as ItemAffixSO;
            stats += itemAffixSO.affix.GetDescription() + "\n";
        }
        return stats;
    }

    public override Dictionary<string, string> GetListOfLevelUpStats()
    {
        Dictionary<string, string> stats = new Dictionary<string, string>();
        ItemForgableData forgableDataWeapon = itemData as ItemForgableData;

        stats.Add("Level", string.Format("{0} -> {1}", forgableDataWeapon.currentLevel, forgableDataWeapon.currentLevel + 1));
        stats.Add("Health", string.Format("+{0} -> +{1}", AdditionalHealth, AdditionalHealthLevelUp));
        stats.Add("Movement Speed", string.Format("{0}% -> {1}%", AdditionalMovement, AdditionalMovementLevelUp));

        return stats;
    }
}
