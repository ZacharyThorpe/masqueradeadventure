using UnityEngine;
using UnityEngine.VFX;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEditor.PackageManager;

public class ShieldGameObject : MonoBehaviour
{
    [SerializeField] private Entity entity;
    [SerializeField] private Shield currentShield = default;

    [SerializeField] private List<HurtBox> listOfHitBoxes = new List<HurtBox>();

    [SerializeField] private VisualEffect shieldEffect = default;
    [SerializeField] private MeshRenderer shieldRenderer = default;

    [SerializeField] private float shieldCooldown = 0;

    private float alpha;
    private Material material;

    private void OnEnable()
    {
        foreach (var box in listOfHitBoxes)
        {
            box.gameObject.SetActive(false);
        }
    }

    private void Start()
    {
        material = shieldRenderer.material;

        shieldEffect.SetInt("SpawnRate", 0); 
        shieldEffect.SetFloat("Toggle", alpha);
        material.SetFloat("_Alpha", 0);
    }

    public void DamageShield()
    {
        shieldCooldown -= 1;
        GlobalEventsManager.OnShieldValueUpdated(entity, shieldCooldown);
        GlobalEventsManager.OnDamageTaken?.Invoke(entity, 1, true);
    }

    public float GetCurrentHealth()
    {
        return shieldCooldown;
    }

    private IEnumerator GainShield()
    {
        while(true)
        {
            shieldCooldown += Time.deltaTime * currentShield.shieldRecovery;
            shieldCooldown = Mathf.Clamp(shieldCooldown, 0, currentShield.shieldCharges);
            GlobalEventsManager.OnShieldValueUpdated(entity, shieldCooldown);
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
    public void SetOwner(Entity _owner)
    {
        entity = _owner;
        foreach (var box in listOfHitBoxes)
        {
            box.owner = _owner;
        }
    }

    public void SetStats(Item _item)
    {
        ItemDataSO itemSO = DatabaseManager.Instance.FindItem(_item.itemData.dataGUID);

        if (itemSO.itemType != ItemType.Shield) return;

        currentShield = _item as Shield;

        HitInfo hitInfo = new HitInfo()
        {
            damage = currentShield.damage,
            hitKnockbackPower = currentShield.hitKnockbackBasePower,
            hitReoveryTime = currentShield.hitReoveryTime,

        };

        foreach (var box in listOfHitBoxes)
        {
            box.HitInfo = hitInfo;
        }
    }

    public void StartShieldRegain()
    {
        StartCoroutine(GainShield());
    }

    public void ToggleShieldHitBox(bool _isEnabled)
    {
        if(shieldEffect != null && material != null)
        {
            shieldEffect.SetInt("SpawnRate", _isEnabled ? 50 : 0);
            DOTween.To(() => alpha, x => alpha = x, _isEnabled ? 0.15f : 0, 0.25f).OnUpdate(() => { shieldEffect.SetFloat("Toggle", alpha); });
            material.DOFloat(_isEnabled ? 0.25f : 0, "_Alpha", 0.25f);
        }

        foreach (var box in listOfHitBoxes)
        {
            box.gameObject.SetActive(_isEnabled);
        }
    }
}
