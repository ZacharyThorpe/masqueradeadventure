using System.Collections.Generic;
using UnityEngine;
using NHance.Assets.Scripts.Items;

public class WeaponGameObject : MonoBehaviour
{
    [SerializeField] private NHItem nhItem;
    [SerializeField] private List<Hitbox> listOfHurtBoxes = new List<Hitbox>();

    private Weapon currentWeapon;

    private void OnEnable()
    {
        foreach (var box in listOfHurtBoxes)
        {
            box.gameObject.SetActive(false);
        }
    }

    public void SetOwner(Entity _owner)
    {
        foreach (var box in listOfHurtBoxes)
        {
            box.owner = _owner;
        }
    }

    public void SetStats(Item _item)
    {
        ItemDataSO itemSO = DatabaseManager.Instance.FindItem(_item.itemData.dataGUID);

        if (itemSO.itemType != ItemType.Weapon) return;

        currentWeapon = _item as Weapon;

        HitInfo hitInfo = new HitInfo()
        {
            damage = currentWeapon.Damage,
            hitKnockbackPower = currentWeapon.HitKnockbackBasePower,
            hitKnockbackTime = currentWeapon.HitKnockbackTime,
            hitReoveryTime = currentWeapon.HitReoveryTime,
        };

        foreach (var box in listOfHurtBoxes)
        {
            box.hitInfo = hitInfo;
        }
    }

    public void MultiplyAttack()
    {
        HitInfo hitInfo = new HitInfo()
        {
            damage = (int)(currentWeapon.Damage * currentWeapon.DamageMultiplier),
            hitKnockbackPower = currentWeapon.HitKnockbackBasePower * currentWeapon.DamageMultiplier,
            hitKnockbackTime = currentWeapon.HitKnockbackTime * currentWeapon.DamageMultiplier,
            hitReoveryTime = currentWeapon.HitReoveryTime * currentWeapon.DamageMultiplier,
        };

        foreach (var box in listOfHurtBoxes)
        {
            box.hitInfo = hitInfo;
        }
    }

    public void ClearMultiplier()
    {
        HitInfo hitInfo = new HitInfo()
        {
            damage = currentWeapon.Damage,
            hitKnockbackPower = currentWeapon.HitKnockbackBasePower,
            hitKnockbackTime = currentWeapon.HitKnockbackTime,
            hitReoveryTime = currentWeapon.HitReoveryTime,
        };

        foreach (var box in listOfHurtBoxes)
        {
            box.hitInfo = hitInfo;
        }
    }

    public void ToggleHurtBox(bool _isEnabled)
    {
        foreach (var box in listOfHurtBoxes)
        {
            box.ResetHitbox();
            box.gameObject.SetActive(_isEnabled);
        }
    }
}
