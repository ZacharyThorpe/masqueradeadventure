public enum Rarity
{
    None,
    Common,
    Uncommon,
    Rare,
    Epic,
    Legendary,
}
