public enum ItemType
{
    None,
    TradeGood,
    Weapon,
    Shield,
    ItemAffix,
    Mask,
    Bag,
    Consumable,
}