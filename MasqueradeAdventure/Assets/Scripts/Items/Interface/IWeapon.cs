﻿using UnityEngine;
using NHance.Assets.Scripts.Items;

public interface IWeapon
{
    NHItem GetNHItem(); 
    void SetOwner(Entity _owner);
    void SetStats(Item _weapon);
    void ToggleHurtBox(bool _isEnabled);
    void DamageShield();
    float GetShieldHealth();
    float GetShieldCharges();
    Transform GetTransform();
    ItemMesh GetItemMesh();
    bool IsValid();
}
