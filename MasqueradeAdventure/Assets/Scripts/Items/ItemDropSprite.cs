using Coffee.UIEffects;
using UnityEngine;
using UnityEngine.UI;

public class ItemDropSprite : MonoBehaviour
{
    [SerializeField] private Image imageBackground;
    [SerializeField] private Image image;
    [SerializeField] private UIEffect uiEffect;
    [SerializeField] private UIEffect uiEffectBackground;

    private void Update()
    {
        transform.LookAt(Camera.main.transform.position);
    }

    public void SetColor(Color color)
    {
        uiEffect.transitionColor = color;
        uiEffectBackground.color = color;
    }

    public void SetIcon(Sprite sprite)
    {
        image.sprite = sprite;
        imageBackground.sprite = sprite;
    }
}
