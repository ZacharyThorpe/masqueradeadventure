﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Shield : ItemForgable
{
    public Shield()
    {

    }
    public Shield(ItemDataSO _data)
    {
        if (_data != null)
        {
            itemData = new ItemForgableData();
            GenerateNewGUID();
            itemData.dataGUID = _data.GUID;
            itemData.itemType = ItemType.Shield;
        }
    }

    public int damage
    {
        get
        {
            ShieldSO shieldData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as ShieldSO;

            int shieldDamage = shieldData.damage;

            ItemForgableData forgableData = itemData as ItemForgableData;

            for (int i = 0; i < forgableData.listOfAffixesGUIDs.Count; i++)
            {
                ItemAffixSO affix = DatabaseManager.Instance.FindItem(forgableData.listOfAffixesGUIDs[i]) as ItemAffixSO;
                if(affix.affix.affixType == AffixType.Attack)
                {
                    AffixDamage affixDamage = affix.affix as AffixDamage;
                    shieldDamage += affixDamage.damageIncrease;
                }
            }

            return shieldDamage;
        }
    }
    public float hitKnockbackBasePower
    {
        get
        {
            ShieldSO shieldData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as ShieldSO;
            return shieldData.hitKnockbackBasePower;
        }
    }
    public float hitKnockbackTime
    {
        get
        {
            ShieldSO shieldData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as ShieldSO;
            return shieldData.hitKnockbackTime;
        }
    }
    public float hitReoveryTime
    {
        get
        {
            ShieldSO shieldData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as ShieldSO;
            return shieldData.hitReoveryTime;
        }
    }

    public ItemMesh EquipmentPrefab
    {
        get
        {
            ShieldSO shieldData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as ShieldSO;
            return shieldData.equipmentPrefab;
        }
    }

    public float shieldRecovery
    {
        get
        {
            ShieldSO shieldData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as ShieldSO;

            float recoveryTime = shieldData.shieldRecovery;

            ItemForgableData forgableData = itemData as ItemForgableData;

            recoveryTime /= forgableData.currentLevel * shieldData.shieldRecoveryScaling;

            return recoveryTime;
        }
    }
    public float shieldRecoveryLevelUp
    {
        get
        {
            ShieldSO shieldData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as ShieldSO;

            float recoveryTime = shieldData.shieldRecovery;

            ItemForgableData forgableData = itemData as ItemForgableData;

            recoveryTime /= (forgableData.currentLevel + 1) * shieldData.shieldRecoveryScaling;

            return recoveryTime;
        }
    }

    public int shieldCharges
    {
        get
        {
            ShieldSO shieldData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as ShieldSO;

            int charges = shieldData.shieldCharges;

            ItemForgableData forgableData = itemData as ItemForgableData;

            charges += forgableData.currentLevel * shieldData.shieldChargesScaling;

            return charges;
        }
    }
    public int shieldChargesLevelUp
    {
        get
        {
            ShieldSO shieldData = DatabaseManager.Instance.FindItem(itemData.dataGUID) as ShieldSO;

            int charges = shieldData.shieldCharges;

            ItemForgableData forgableData = itemData as ItemForgableData;

            charges += (forgableData.currentLevel + 1) * shieldData.shieldChargesScaling;

            return charges;
        }
    }

    public override string GetDescription()
    {
        ItemForgableData forgableDataShield = itemData as ItemForgableData;

        string stats = string.Format("Level {0}\n", forgableDataShield.currentLevel);
        stats += string.Format("{0} Knockback Power\n", hitKnockbackBasePower);

        if (damage > 0)
        {
            stats += string.Format("{0} damage\n", damage);
        }

        stats += string.Format("{0} charges\n", shieldCharges);
        stats += string.Format("{0}% regen\n", (int)(shieldRecovery * 100));

        for (int i = 0; i < forgableDataShield.listOfAffixesGUIDs.Count; i++)
        {
            ItemAffixSO itemAffixSO = DatabaseManager.Instance.FindItem(forgableDataShield.listOfAffixesGUIDs[i]) as ItemAffixSO;
            stats += itemAffixSO.affix.GetDescription() + "\n";
        }
        return stats;
    }

    public override string GetLevelUpDescription()
    {
        ItemForgableData forgableDataShield = itemData as ItemForgableData;

        string stats = string.Format("Level {0} -> {1}\n", forgableDataShield.currentLevel, forgableDataShield.currentLevel + 1);
        stats += string.Format("{0} Knockback Power\n", hitKnockbackBasePower);

        if (damage > 0)
        {
            stats += string.Format("{0} damage\n", damage);
        }

        stats += string.Format("{0} -> {1} charges\n", shieldCharges, shieldChargesLevelUp);
        stats += string.Format("{0}% -> {1}% regen\n", (int)(shieldRecovery * 100), (int)(shieldRecoveryLevelUp * 100));

        for (int i = 0; i < forgableDataShield.listOfAffixesGUIDs.Count; i++)
        {
            ItemAffixSO itemAffixSO = DatabaseManager.Instance.FindItem(forgableDataShield.listOfAffixesGUIDs[i]) as ItemAffixSO;
            stats += itemAffixSO.affix.GetDescription() + "\n";
        }
        return stats;
    }

    public override Dictionary<string, string> GetListOfLevelUpStats()
    {
        Dictionary<string, string> stats = new Dictionary<string, string>();
        ItemForgableData forgableDataWeapon = itemData as ItemForgableData;

        stats.Add("Level", string.Format("{0} -> {1}", forgableDataWeapon.currentLevel, forgableDataWeapon.currentLevel + 1));
        stats.Add("Knockback Power", hitKnockbackBasePower.ToString());
        if (damage > 0)
        {
            stats.Add("Damage", damage.ToString());
        }
        stats.Add("Charges", string.Format("{0} -> {1}", shieldCharges, shieldChargesLevelUp));
        stats.Add("Regen", string.Format("{0}% -> {1}%", (int)(shieldRecovery * 100), (int)(shieldRecoveryLevelUp * 100)));

        return stats;
    }
}
