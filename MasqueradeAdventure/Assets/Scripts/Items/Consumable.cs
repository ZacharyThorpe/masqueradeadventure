using System;

[Serializable]
public class Consumable : Item
{
    public Consumable()
    {

    }
    public Consumable(ItemDataSO _data)
    {
        if (_data != null)
        {
            itemData = new ItemData();
            GenerateNewGUID();
            itemData.dataGUID = _data.GUID;
            itemData.itemType = ItemType.Consumable;
        }
    }

    public override string GetDescription()
    {
        var data = DatabaseManager.Instance.FindItem(itemData.dataGUID) as ConsumableSO;
        return data.consumable.GetDescription() + "\n";
    }
}
