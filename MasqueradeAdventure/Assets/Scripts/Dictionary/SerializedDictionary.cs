using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

[Serializable]
public class StringSpriteDictionary : UnitySerializedDictionary<string, Sprite> { }
[Serializable]
public class PlatformTypeSpriteDictionary : UnitySerializedDictionary<IconPlatformType, Sprite> { }
[Serializable]
public class RarityAssetsByRarity : UnitySerializedDictionary<Rarity, RarityAssets> { }

public abstract class UnitySerializedDictionary<TKey, TValue> : Dictionary<TKey, TValue>, ISerializationCallbackReceiver
{
    [SerializeField] private List<KeyValueData> keyValueData = new List<KeyValueData>();

    public void OnAfterDeserialize()
    {
        this.Clear();
        foreach (var item in this.keyValueData)
        {
            this[item.key] = item.value;
        }
    }

    public void OnBeforeSerialize()
    {
        this.keyValueData.Clear();
        foreach (var kvp in this)
        {
            keyValueData.Add(new KeyValueData() { key = kvp.Key, value = kvp.Value });
        }
    }

    [Serializable]
    private struct KeyValueData
    {
        public TKey key;
        public TValue value;
    }
}
